<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ms_lokasi
 * @package App\Models
 * @version January 10, 2020, 12:57 am UTC
 *
 * @property integer id_lokasi
 * @property string nomor_lokasi
 * @property string nama_lokasi
 */
class ms_lokasi extends Model
{
    

    public $table = 'ms_lokasi';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'nomor_lokasi',
        'nama_lokasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nomor_lokasi' => 'string',
        'nama_lokasi' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nomor_lokasi' => 'required',
        'nama_lokasi' => 'required'
    ];

    
}
