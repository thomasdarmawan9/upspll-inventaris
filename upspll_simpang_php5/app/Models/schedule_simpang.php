<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class schedule_simpang
 * @package App\Models
 * @version January 16, 2020, 2:19 am UTC
 *
 * @property string nama_lokasi
 * @property integer nomor_plan
 * @property integer ct
 * @property time time
 * @property string days
 */
class schedule_simpang extends Model
{
    use SoftDeletes;

    public $table = 'schedule_simpang';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_lokasi',
        'nomor_plan',
        'ct',
        'time',
        'days'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_lokasi' => 'string',
        'nomor_plan' => 'integer',
        'ct' => 'integer',
        'days' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function detail_simpang(){
        return $this->hasOne('\App\Models\detail_simpang','id','nama_lokasi');
    }
}
