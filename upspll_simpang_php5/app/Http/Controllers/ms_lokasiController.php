<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createms_lokasiRequest;
use App\Http\Requests\Updatems_lokasiRequest;
use App\Repositories\ms_lokasiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\ms_lokasi;
use App;
use PDF;

class ms_lokasiController extends AppBaseController
{
    /** @var  ms_lokasiRepository */
    private $msLokasiRepository;

    public function __construct(ms_lokasiRepository $msLokasiRepo)
    {
        $this->msLokasiRepository = $msLokasiRepo;
    }

    /**
     * Display a listing of the ms_lokasi.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->msLokasiRepository->pushCriteria(new RequestCriteria($request));
        $msLokasis = $this->msLokasiRepository->all();

        return view('ms_lokasis.index')
            ->with('msLokasis', $msLokasis);
    }

    public function cetak_pdf()
    {
        // $pdf = App::make('dompdf.wrapper');
        // $pdf->loadHTML('<h1>Test</h1>');
        // return $pdf->stream();
        $msLokasis = Ms_lokasi::all();
        $pdf = PDF::loadview('ms_lokasis.ms_lokasi',['msLokasis'=>$msLokasis]);
        // dd($pdf);
    	return $pdf->download('laporan-mslokasi-pdf.pdf');
    }

    /**
     * Show the form for creating a new ms_lokasi.
     *
     * @return Response
     */
    public function create()
    {
        return view('ms_lokasis.create');
    }

    /**
     * Store a newly created ms_lokasi in storage.
     *
     * @param Createms_lokasiRequest $request
     *
     * @return Response
     */
    public function store(Createms_lokasiRequest $request)
    {
        $input = $request->all();

        $msLokasi = $this->msLokasiRepository->create($input);

        Flash::success('Ms Lokasi saved successfully.');

        return redirect(route('msLokasis.index'));
    }

    /**
     * Display the specified ms_lokasi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $msLokasi = $this->msLokasiRepository->findWithoutFail($id);

        if (empty($msLokasi)) {
            Flash::error('Ms Lokasi not found');

            return redirect(route('msLokasis.index'));
        }

        return view('ms_lokasis.show')->with('msLokasi', $msLokasi);
    }

    /**
     * Show the form for editing the specified ms_lokasi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $msLokasi = $this->msLokasiRepository->findWithoutFail($id);

        if (empty($msLokasi)) {
            Flash::error('Ms Lokasi not found');

            return redirect(route('msLokasis.index'));
        }

        return view('ms_lokasis.edit')->with('msLokasi', $msLokasi);
    }

    /**
     * Update the specified ms_lokasi in storage.
     *
     * @param  int              $id
     * @param Updatems_lokasiRequest $request
     *
     * @return Response
     */
    public function update($id, Updatems_lokasiRequest $request)
    {
        $msLokasi = $this->msLokasiRepository->findWithoutFail($id);

        if (empty($msLokasi)) {
            Flash::error('Ms Lokasi not found');

            return redirect(route('msLokasis.index'));
        }

        $msLokasi = $this->msLokasiRepository->update($request->all(), $id);

        Flash::success('Ms Lokasi updated successfully.');

        return redirect(route('msLokasis.index'));
    }

    /**
     * Remove the specified ms_lokasi from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $msLokasi = $this->msLokasiRepository->findWithoutFail($id);

        if (empty($msLokasi)) {
            Flash::error('Ms Lokasi not found');

            return redirect(route('msLokasis.index'));
        }

        $this->msLokasiRepository->delete($id);

        Flash::success('Ms Lokasi deleted successfully.');

        return redirect(route('msLokasis.index'));
    }
}
