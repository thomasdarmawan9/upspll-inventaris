<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createschedule_simpangRequest;
use App\Http\Requests\Updateschedule_simpangRequest;
use App\Repositories\schedule_simpangRepository;
use App\Repositories\detail_simpangRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class schedule_simpangController extends AppBaseController
{
    /** @var  schedule_simpangRepository */
    private $scheduleSimpangRepository;

    public function __construct(detail_simpangRepository $detailSimpangRepo, schedule_simpangRepository $scheduleSimpangRepo)
    {
        
        $this->detailSimpangRepository = $detailSimpangRepo;
        $this->scheduleSimpangRepository = $scheduleSimpangRepo;
    }

    /**
     * Display a listing of the schedule_simpang.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->scheduleSimpangRepository->pushCriteria(new RequestCriteria($request));
        $scheduleSimpangs = $this->scheduleSimpangRepository->all();

        return view('schedule_simpangs.index')
            ->with('scheduleSimpangs', $scheduleSimpangs);
    }

    /**
     * Show the form for creating a new schedule_simpang.
     *
     * @return Response
     */
    public function create()
    {
        $nama_lokasi = DB::table('detail_simpang')->where('nama_lokasi','=','id');

        return view('schedule_simpangs.create')->with('scheduleSimpang', $nama_lokasi);
    }

    public function addsimpang($id)
    {
        $nama_lokasi = DB::table('detail_simpang')->where('id','=',$id)->first();
        return view('schedule_simpangs.create')->with('scheduleSimpang', $nama_lokasi);
    }

    /**
     * Store a newly created schedule_simpang in storage.
     *
     * @param Createschedule_simpangRequest $request
     *
     * @return Response
     */
    public function store(Createschedule_simpangRequest $request)
    {
        $this->detailSimpangRepository->pushCriteria(new RequestCriteria($request));
        $detailSimpangs = $this->detailSimpangRepository->all();

        $input = $request->all();
        // dd($input);
        $this->scheduleSimpangRepository->create($input);

        if (isset($input['nama_lokasi_arr'])) {
            if(count($input['nama_lokasi_arr'])>0){

                foreach ($input['nama_lokasi_arr'] as $key => $value) {
               
                    $scheduleSimpangs  = $this->scheduleSimpangRepository->create([
                        'nama_lokasi' => $input['nama_lokasi_arr'][$key],
                        'nomor_plan' => $input['nomor_plan_arr'][$key],
                        'ct' => $input['ct_arr'][$key],
                        'time' => $input['time_arr'][$key],
                        'days' => $input['days_arr'][$key],
                    ]);
                }
        }
        }

        Flash::success('Schedule Simpang saved successfully.');

        return view('detail_simpangs.index')
            ->with('detailSimpangs', $detailSimpangs);
    }

    /**
     * Display the specified schedule_simpang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $scheduleSimpang = $this->scheduleSimpangRepository->findWithoutFail($id);

        if (empty($scheduleSimpang)) {
            Flash::error('Schedule Simpang not found');

            return redirect(route('scheduleSimpangs.index'));
        }

        return view('schedule_simpangs.show')->with('scheduleSimpang', $scheduleSimpang);
    }

    /**
     * Show the form for editing the specified schedule_simpang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $scheduleSimpang = $this->scheduleSimpangRepository->findWithoutFail($id);

        if (empty($scheduleSimpang)) {
            Flash::error('Schedule Simpang not found');

            return redirect(route('scheduleSimpangs.index'));
        }

        return view('schedule_simpangs.edit')->with('scheduleSimpang', $scheduleSimpang);
    }

    /**
     * Update the specified schedule_simpang in storage.
     *
     * @param  int              $id
     * @param Updateschedule_simpangRequest $request
     *
     * @return Response
     */
    public function update($id, Updateschedule_simpangRequest $request)
    {
        $scheduleSimpang = $this->scheduleSimpangRepository->findWithoutFail($id);

        if (empty($scheduleSimpang)) {
            Flash::error('Schedule Simpang not found');

            return redirect(route('scheduleSimpangs.index'));
        }

        $scheduleSimpang = $this->scheduleSimpangRepository->update($request->all(), $id);

        Flash::success('Schedule Simpang updated successfully.');

        return redirect(route('scheduleSimpangs.index'));
    }

    /**
     * Remove the specified schedule_simpang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $scheduleSimpang = $this->scheduleSimpangRepository->findWithoutFail($id);

        if (empty($scheduleSimpang)) {
            Flash::error('Schedule Simpang not found');

            return redirect(route('scheduleSimpangs.index'));
        }

        $this->scheduleSimpangRepository->delete($id);

        Flash::success('Schedule Simpang deleted successfully.');

        return redirect(route('scheduleSimpangs.index'));
    }
}
