@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Detail Simpang
    </h1>
</section>

<div class="content">
    <div class="box box-primary">
        <div class="box-body"> 
            <div class="row" style="padding-left: 20px">
            <a href="/detailSimpang/cetak_pdf/{{$detailSimpang->id}}" class="uk-button uk-button-primary uk-button-small" target="_blank">CETAK PDF</a>
       <hr>
                @include('detail_simpangs.show_fields')

            </div>
            <hr>
            <div class="row">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home"><b>Planning</b></a></li>
                    <li><a data-toggle="tab" href="#menu1"><b>Schedule</b></a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active" style="padding-top:10px;">
                        <h3 style="padding-left: 20px">PLANNING</h3>
                        <p>
                        <input type="hidden" class="column_names" value="{{ $detailSimpang->nama_lokasi }} " id="nama_lokasi">
                            <center>
                                @if(auth::user()->role=='Teknisi')
                                <div class="table-responsive" style="width:95%;padding-top:20px;">
                                    <table class="uk-table uk-table-hover uk-table-striped" id="tableps">
                                        <thead>
                                            <tr>
                                                <th>Nomor Plan</th>
                                                <th>CT</th>
                                                <th>Offset</th>
                                                <th>A</th>
                                                <th>B</th>
                                                <th>C</th>
                                                <th>D</th>
                                                <th>E</th>
                                                <th>F</th>
                                                <th>R-</th>
                                                <th>R+</th>
                                                <th>Y-</th>
                                                <th>Y+</th>
                                                <th>Z-</th>
                                                <th>Z+</th>
                                                <th>Q-</th>
                                                <th>Q+</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($plan as $planSimpang)
                                            <tr>
                                                <td>{{ $planSimpang->nomor_plan }}</td>
                                                <td>{{ $planSimpang->cycle_time }}</td>
                                                <td>{{ $planSimpang->offset }}</td>
                                                <td>{{ $planSimpang->a }}</td>
                                                <td>{{ $planSimpang->b }}</td>
                                                <td>{{ $planSimpang->c }}</td>
                                                <td>{{ $planSimpang->d }}</td>
                                                <td>{{ $planSimpang->e }}</td>
                                                <td>{{ $planSimpang->f }}</td>
                                                <td>{{ $planSimpang->r_minus }}</td>
                                                <td>{{ $planSimpang->r_plus }}</td>
                                                <td>{{ $planSimpang->y_minus }}</td>
                                                <td>{{ $planSimpang->y_plus }}</td>
                                                <td>{{ $planSimpang->z_minus }}</td>
                                                <td>{{ $planSimpang->z_plus }}</td>
                                                <td>{{ $planSimpang->q_minus }}</td>
                                                <td>{{ $planSimpang->q_plus }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endif
                                <!-- ajaxtable -->
                                @if(auth::user()->role=='Admin')
                                <div class="panel panel-default" style="width:97%">
                                    <div class="panel-body">
                                    <div class="uk-text-left"><small>*ubah isi pada tabel untuk melakukan edit</small></div>
                                        <div id="message"></div>
                                        <div class="table-responsive">
                                            <table class="uk-table uk-table-hover uk-table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nomor Plan</th>
                                                        <th>CT</th>
                                                        <th>Offset</th>
                                                        <th>a</th>
                                                        <th>b</th>
                                                        <th>c</th>
                                                        <th>d</th>
                                                        <th>e</th>
                                                        <th>f</th>
                                                        <th>R-</th>
                                                        <th>R+</th>
                                                        <th>Y-</th>
                                                        <th>Y+</th>
                                                        <th>Z-</th>
                                                        <th>Z+</th>
                                                        <th>Q-</th>
                                                        <th>Q+</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="plan">

                                                </tbody>
                                            </table>
                                            {{ csrf_field() }}
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </center>
                        </p>
                    </div>
                    <div id="menu1" class="tab-pane fade" style="padding-top:10px;">
                        <h3 style="padding-left: 20px">SCHEDULE</h3>
                        <p>
                            <center>
                            @if(auth::user()->role=='Teknisi')
                                <div class="table-responsive" style="width:95%;padding-top:20px;">
                                    <table class="uk-table uk-table-hover uk-table-striped" id="tabless">
                                        <thead>
                                            <tr>
                                                <th>Nomor Plan</th>
                                                <th>Ct</th>
                                                <th>Time</th>
                                                <th>Days</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($schedule as $scheduleSimpang)
                                            <tr>
                                                <td>{!! $scheduleSimpang->nomor_plan !!}</td>
                                                <td>{!! $scheduleSimpang->ct !!}</td>
                                                <td>{!! $scheduleSimpang->time !!}</td>
                                                <td>{!! $scheduleSimpang->days !!}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endif

                                @if(auth::user()->role=='Admin')
                                <div class="panel panel-default" style="width:97%">
                                    <div class="panel-body">
                                    <div class="uk-text-left"><small>*ubah isi pada tabel untuk melakukan edit</small></div>
                                        <div id="message"></div>
                                        <div class="table-responsive">
                                            <table class="uk-table uk-table-hover uk-table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nomor Plan</th>
                                                        <th>CT</th>
                                                        <th>Time</th>
                                                        <th>Days</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="schedule">

                                                </tbody>
                                            </table>
                                            {{ csrf_field() }}
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </center>
                        </p>

                    </div>
                </div>
                <div class="col-sm-12" style="padding-top:30px">
                    <a href="{{ route('detailSimpangs.index') }}"
                        class="uk-button uk-button-default uk-button-small">Back</a>
                </div>
            </div>

        </div>
    </div>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@if(auth::user()->role=='Admin')
<script>
    $(document).ready(function () {
      
        fetch_data();
      
        function fetch_data() {
            var datas = $('#nama_lokasi').val();
            $.ajax({
                url: "/detail_simpangController/fetch_data",
                dataType: "json",
                data:{
                datas
                },
                success: function (data, key) {
                    console.log(data);
                    var html = '';
                    for (var count = 0; count < data.length; count++) {                                           

                        html += '<tr>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="nomor_plan" data-id="' +
                            data[count].id + '">' + data[count].nomor_plan + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="cycle_time" data-id="' +
                            data[count].id + '">' + data[count].cycle_time + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="offset" data-id="' +
                            data[count].id + '">' + data[count].offset + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="a" data-id="' +
                            data[count].id + '">' + ifnull(data[count].a) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="b" data-id="' +
                            data[count].id + '">' + ifnull(data[count].b) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="c" data-id="' +
                            data[count].id + '">' + ifnull(data[count].c) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="d" data-id="' +
                            data[count].id + '">' + ifnull(data[count].d) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="e" data-id="' +
                            data[count].id + '">' + ifnull(data[count].e) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="f" data-id="' +
                            data[count].id + '">' + ifnull(data[count].f) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="r_minus" data-id="' +
                            data[count].id + '">' + ifnull(data[count].r_minus) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="r_plus" data-id="' +
                            data[count].id + '">' + ifnull(data[count].r_plus) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="y_minus" data-id="' +
                            data[count].id + '">' + ifnull(data[count].y_minus)  + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="y_plus" data-id="' +
                            data[count].id + '">' + ifnull(data[count].y_plus) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="z_minus" data-id="' +
                            data[count].id + '">' + ifnull(data[count].z_minus) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="z_plus" data-id="' +
                            data[count].id + '">' + ifnull(data[count].z_plus) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="q_minus" data-id="' +
                            data[count].id + '">' + ifnull(data[count].q_minus) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="q_plus" data-id="' +
                            data[count].id + '">' + ifnull(data[count].q_plus) + '</td>';
                        html +=
                            '<td><button type="button" class="uk-button uk-button-danger uk-button-small delete" id="' +
                            data[count].id + '">Delete</button></td></tr>';
                    }
                    $('#plan').html(html);
                    
                }
            });
        }

        function ifnull(data){
            if(data == null || data == 'null'){
                return  '';
            }else{
               return data;
            }
        }

        var _token = $('input[name="_token"]').val();
        $(document).on('blur', '.column_name', function () {
            var column_name = $(this).data("column_name");
            var column_value = $(this).text();
            var id = $(this).data("id");

            if (column_value != '') {
                $.ajax({
                    url: "{{ route('detail_simpangController.update_data') }}",
                    method: "POST",
                    data: {
                        column_name: column_name,
                        column_value: column_value,
                        id: id,
                        _token: _token
                    },
                    success: function (data) {
                        $('#message').html(data);
                    }
                })
            }
        });

        $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this records?")) {
                $.ajax({
                    url: "{{ route('detail_simpangController.delete_data') }}",
                    method: "POST",
                    data: {
                        id: id,
                        _token: _token
                    },
                    success: function (data) {
                        $('#message').html(data);
                        fetch_data();
                    }
                });
            }
        });


    });
</script>

<script>
    $(document).ready(function () {

        fetch_data2();

        function fetch_data2() {
            
            var datas = $('#nama_lokasi').val();
            $.ajax({
                url: "/detail_simpangController/fetch_data2",
                dataType: "json",
                data:{
                datas
                },
                success: function (data) {
                    var html = '';
             
                    for (var count = 0; count < data.length; count++) {
                        html += '<tr>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="nomor_plan" data-id="' +
                            data[count].id + '">' + ifnull(data[count].nomor_plan) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="ct" data-id="' +
                            data[count].id + '">' + ifnull(data[count].ct) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="time" data-id="' +
                            data[count].id + '">' + ifnull(data[count].time) + '</td>';
                        html +=
                            '<td contenteditable class="column_name" data-column_name="days" data-id="' +
                            data[count].id + '">' + ifnull(data[count].days) + '</td>';
                        html +=
                            '<td><button type="button" class="uk-button uk-button-danger uk-button-small delete" id="' +
                            data[count].id + '">Delete</button></td></tr>';
                    }
                    $('#schedule').html(html);
                }
            });
        }
        function ifnull(data){
            if(data == null || data == 'null'){
                return  '';
            }else{
               return data;
            }
        }
        var _token = $('input[name="_token"]').val();
        $(document).on('blur', '.column_name', function () {
            var column_name = $(this).data("column_name");
            var column_value = $(this).text();
            var id = $(this).data("id");

            if (column_value != '') {
                $.ajax({
                    url: "{{ route('detail_simpangController.update_data2') }}",
                    method: "POST",
                    data: {
                        column_name: column_name,
                        column_value: column_value,
                        id: id,
                        _token: _token
                    },
                    success: function (data) {
                        $('#message').html(data);
                    }
                })
            } else {
                $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
            }
        });

        $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            if (confirm("Are you sure you want to delete this records?")) {
                $.ajax({
                    url: "{{ route('detail_simpangController.delete_data2') }}",
                    method: "POST",
                    data: {
                        id: id,
                        _token: _token
                    },
                    success: function (data) {
                        $('#message').html(data);
                        fetch_data2();
                    }
                });
            }
        });


    });
</script>
@endif
@endsection