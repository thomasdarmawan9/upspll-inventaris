<!-- Id Lokasi Fk Field -->
<div class="form-group col-sm-4">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    {!! Form::select('nama_lokasi', $nama_lokasi, null, ['class' => 'form-control']) !!}
</div>

<!-- Lat Field -->
<div class="form-group col-sm-4">
    {!! Form::label('lat', 'Lat:') !!}
    {!! Form::number('lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Long Field -->
<div class="form-group col-sm-4">
    {!! Form::label('long', 'Long:') !!}
    {!! Form::number('long', null, ['class' => 'form-control']) !!}
</div>

<!-- Tiang Field -->

<style>
.setengah {
  content: "\00BD";
}
</style>
<div class="form-group has-feedback{{ $errors->has('tiang') ? ' has-error' : '' }} col-sm-6">
{!! Form::label('tiang', 'Tiang:') !!}
            <select class="form-control" name="tiang"  value="{{ old('tiang') }}" >
                <option value="">----pilih jenis tiang----</option>
                <option value="tiang lurus 4 meter bulat" {{ $detailSimpang->tiang == 'tiang lurus 4 meter bulat' ? 'selected' : '' }}>tiang lurus 4 meter bulat</option>
                <option value="tiang lurus 4 meter oktagonal" {{ $detailSimpang->tiang == 'tiang lurus 4 meter oktagonal' ? 'selected' : '' }}>tiang lurus 4 meter oktagonal</option>
                <option value="tiang lurus 6 meter bulat" {{ $detailSimpang->tiang == 'tiang lurus 6 meter bulat' ? 'selected' : '' }}>tiang lurus 6 meter bulat</option>
                <option value="tiang lurus 6 meter oktagonal" {{ $detailSimpang->tiang == 'tiang lurus 6 meter oktagonal' ? 'selected' : '' }}>tiang lurus 6 meter oktagonal</option>
                <option value="tiang lengkung bulat" {{ $detailSimpang->tiang == 'tiang lengkung bulat' ? 'selected' : '' }}>tiang lengkung bulat</option>
                <option value="tiang lengkung oktagonal" {{ $detailSimpang->tiang == 'tiang lengkung oktagonal' ? 'selected' : '' }}>tiang lengkung oktagonal</option>
                <option value="tiang F" {{ $detailSimpang->tiang == 'tiang F' ? 'selected' : '' }}>tiang F</option>
                <option value="tiang siku bentangan 7 1&frasl;2" {{ $detailSimpang->tiang == 'tiang siku bentangan 7 1&frasl;2' ? 'selected' : '' }}>tiang siku bentangan 7<span class="setengah">&#189;</span></option>
                <option value="tiang siku bentangan 4 1&frasl;2" {{ $detailSimpang->tiang == 'tiang siku bentangan 4 1&frasl;2' ? 'selected' : '' }}>tiang siku bentangan 4<span class="setengah">&#189;</span></option>
            </select>
            </div>
            
            <div class="form-group col-sm-6">
    {!! Form::label('jml_tiang', 'Jumlah Tiang:') !!}
    {!! Form::number('jml_tiang', null, ['class' => 'form-control']) !!}
</div>
<!-- Unit Lampu Field -->
<div class="form-group has-feedback{{ $errors->has('unit_lampu') ? ' has-error' : '' }} col-sm-6 ">
{!! Form::label('unit_lampu', 'Unit Lampu:') !!}
            <select class="form-control" name="unit_lampu"  value="{{ old('unit_lampu') }}" >
                <option value="">----pilih jenis unit lampu----</option>
                <option value="2 X 30 cm (MKH)" {{ $detailSimpang->unit_lampu == '2 X 30 cm (MKH)' ? 'selected' : '' }}>2 X 30 cm (MKH)</option>
                <option value="2 X 30 cm (K)" {{ $detailSimpang->unit_lampu == '2 X 30 cm (K)' ? 'selected' : '' }}>2 X 30 cm (K)</option>
                <option value="2 X 30 cm (MH)" {{ $detailSimpang->unit_lampu == '2 X 30 cm (MH)' ? 'selected' : '' }}>2 X 30 cm (MH)</option>
                <option value="3 X 20 cm (MKH)" {{ $detailSimpang->unit_lampu == '3 X 20 cm (MKH)' ? 'selected' : '' }}>3 X 20 cm (MKH)</option>
                <option value="3 X 20 cm (K)" {{ $detailSimpang->unit_lampu == '3 X 20 cm (K)' ? 'selected' : '' }}>3 X 20 cm (K)</option>
                <option value="3 X 20 cm (MH)" {{ $detailSimpang->unit_lampu == '3 X 20 cm (MH)' ? 'selected' : '' }}>3 X 20 cm (MH)</option>
            </select>
            </div>
            <div class="form-group col-sm-6">
    {!! Form::label('jml_lampu', 'Jumlah Lampu:') !!}
    {!! Form::number('jml_lampu', null, ['class' => 'form-control']) !!}
</div>
<!-- Thn Psng Unit Field -->
<style>
#thn_psng_unit{
    display: block;
    width: 100%;
    padding: 6px 12px;
    font-size: 14px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
}
</style>
<div class="form-group col-sm-6">
    {!! Form::label('thn_psng_unit', 'Tahun Pasang Unit:') !!}
    <br>
    {!! Form::selectYear('thn_psng_unit',1998, 2029, ['class' => 'form-control tahununit']) !!}
</div>

<div class="form-group col-sm-6">
</div>


<!-- Controller Field -->

<div class="form-group has-feedback{{ $errors->has('controller') ? ' has-error' : '' }} col-sm-6">
{!! Form::label('controller', 'Controller:') !!}
            <select class="form-control" name="controller"  value="{{ old('controller') }}" >
                <option value="">----pilih jenis controller----</option>
                <option value="Stats A (ATCS)" {{ $detailSimpang->controller == 'Stats A (ATCS)' ? 'selected' : '' }}>Stats A (ATCS)</option>
                <option value="Stats A (NON ATCS)" {{ $detailSimpang->controller == 'Stats A (NON ATCS)' ? 'selected' : '' }}>Stats A (NON ATCS)</option>
                <option value="Newtrap" {{ $detailSimpang->controller == 'Newtrap' ? 'selected' : '' }}>Newtrap</option>
                <option value="UMC AC" {{ $detailSimpang->controller == 'UMC AC' ? 'selected' : '' }}>UMC AC</option>
                <option value="UMC DC" {{ $detailSimpang->controller == 'UMC DC' ? 'selected' : '' }}>UMC DC</option>
                <option value="HMK" {{ $detailSimpang->controller == 'HMK' ? 'selected' : '' }}>HMK</option>
            </select>
            </div>

<!-- Thn Psng Controller Field -->
<style>
#thn_psng_controller{
    display: block;
    width: 100%;
    padding: 6px 12px;
    font-size: 14px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
}
</style>
<div class="form-group col-sm-6">
    {!! Form::label('thn_psng_controller', 'Tahun Pasang Controller:') !!}
    <br>
    {!! Form::selectYear('thn_psng_controller',1998, 2029, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
</div>


<!-- Controller Pc Field -->
<div class="form-group has-feedback{{ $errors->has('controller_pc') ? ' has-error' : '' }} col-sm-6">
{!! Form::label('controller_pc', 'Controller PC:') !!}
            <select class="form-control" name="controller_pc"  value="{{ old('controller_pc') }}" >
                <option value="">----pilih jenis controller PC----</option>
                <option value="Newtrap" {{ $detailSimpang->controller_pc == 'Newtrap' ? 'selected' : '' }}>Newtrap</option>
                <option value="UMC" {{ $detailSimpang->controller_pc == 'UMC' ? 'selected' : '' }}>UMC</option>
                <option value="HMK" {{ $detailSimpang->controller_pc == 'HMK' ? 'selected' : '' }}>HMK</option>
                <option value="Telepico" {{ $detailSimpang->controller_pc == 'Telepico' ? 'selected' : '' }}>Telepico</option>
            </select>
            </div>


    

<div class="form-group col-sm-6">
    {!! Form::label('alamat', 'Alamat:') !!}
    {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
</div>
<!-- Fase A Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_a', 'Fase A:') !!}
    {!! Form::file('fase_a', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase B Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_b', 'Fase B:') !!}
    {!! Form::file('fase_b', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase C Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_c', 'Fase C:') !!}
    {!! Form::file('fase_c', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase D Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_d', 'Fase D:') !!}
    {!! Form::file('fase_d', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase E Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_e', 'Fase E:') !!}
    {!! Form::file('fase_e', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_f', 'Fase F:') !!}
    {!! Form::file('fase_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'uk-button uk-button-primary uk-button-small']) !!}
    <a href="{{ route('detailSimpangs.index') }}" class="uk-button uk-button-default uk-button-small">Cancel</a>
</div>
