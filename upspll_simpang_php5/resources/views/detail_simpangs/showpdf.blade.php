<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
           table {
  border-collapse: collapse;
}
 th {
  background: #ccc;
}

th, td {
  border: 1px solid #ccc;
  padding: 8px;
}

tr:nth-child(even) {
  background: #efefef;
}

tr:hover {
  background: #d1d1d1;
}
    </style>
</head>

<body>

    <h2>Laporan Informasi Detail Simpang {{ $detailSimpang->nama_lokasi }}</h2>
    <table id="table" class="uk-table uk-table-hover uk-table-striped">

        <tr>
            <td> {!! Form::label('nama_lokasi', 'Nama lokasi:') !!}
                <p>{{ $detailSimpang->nama_lokasi }}</p>
            </td>
            <td> {!! Form::label('alamat', 'Alamat:') !!}
                <p>{{ $detailSimpang->alamat }}</p>
            </td>
            <td> {!! Form::label('lat', 'Lat:') !!}
                <p>{{ $detailSimpang->lat }}</p>
            </td>
        </tr>

        <tr>
            <td>{!! Form::label('long', 'Long:') !!}
                <p>{{ $detailSimpang->long }}</p>
            </td>
            <td> {!! Form::label('tiang', 'Tiang:') !!}
                <p>{{ $detailSimpang->tiang }}</p>
            </td>
            <td>
                {!! Form::label('jml_tiang', 'Jumlah Tiang:') !!}
                <p>{{ $detailSimpang->jml_tiang }}</p>
            </td>

        </tr>
        <tr>
            <td> {!! Form::label('unit_lampu', 'Unit Lampu:') !!}
                <p>{{ $detailSimpang->unit_lampu }}</p>
            </td>
            <td> {!! Form::label('jml_lampu', 'Jumlah Lampu:') !!}
                <p>{{ $detailSimpang->jml_lampu }}</p>
            </td>
            <td> {!! Form::label('tahun_psng_unit', 'Thn Pasang Unit:') !!}
                <p>{{ $detailSimpang->thn_psng_unit }}</p>
            </td>
        </tr>
        <tr>
            <td> {!! Form::label('controller', 'Controller:') !!}
                <p>{{ $detailSimpang->controller }}</p>
            </td>
            <td> {!! Form::label('tahun_psng_controller', 'Thn Pasang Controller:') !!}
                <p>{{ $detailSimpang->thn_psng_controller }}</p>
            </td>
            <td>{!! Form::label('controller_pc', 'Controller Pc:') !!}
                <p>{{ $detailSimpang->controller_pc }}</p>
            </td>
        </tr>
    </table>
    <br>
    <h2>Laporan Informasi Planning Simpang {{ $detailSimpang->nama_lokasi }}</h2>
    <table id="table" class="uk-table uk-table-hover uk-table-striped">
    <tr>
    <td>{!! Form::label('nomor_plan', 'nomor plan:') !!}
    <p>{{ $plan['0']['nomor_plan'] }}</p>
    </td>
    <td>{!! Form::label('cycle_time', 'Cycle time:') !!}
    <p>{{ $plan['0']['cycle_time'] }}</p>
    </td>
    <td>{!! Form::label('offset', 'offset:') !!}
    <p>{{ $plan['0']['offset'] }}</p>
    </td>
    <td>{!! Form::label('a', 'a:') !!}
    <p>{{ $plan['0']['a'] }}</p>
    </td>
    <td>{!! Form::label('b', 'b:') !!}
    <p>{{ $plan['0']['b'] }}</p>
    </td>
    <td>{!! Form::label('c', 'c:') !!}
    <p>{{ $plan['0']['c'] }}</p>
    </td>
    <td>{!! Form::label('d', 'd:') !!}
    <p>{{ $plan['0']['d'] }}</p>
    </td>
    <td>{!! Form::label('e', 'e:') !!}
    <p>{{ $plan['0']['e'] }}</p>
    </td>

    <td>{!! Form::label('f', 'f:') !!}
    <p>{{ $plan['0']['f'] }}</p>
    </td>
    
    <tr>
    <td>{!! Form::label('r_minus', 'r minus:') !!}
    <p>{{ $plan['0']['r_minus'] }}</p>
    </td>
    <td>{!! Form::label('r_plus', 'r plus:') !!}
    <p>{{ $plan['0']['r_plus'] }}</p>
    </td>
    <td>{!! Form::label('y_minus', 'y minus:') !!}
    <p>{{ $plan['0']['y_minus'] }}</p>
    </td>
    <td>{!! Form::label('y_plus', 'y plus:') !!}
    <p>{{ $plan['0']['y_plus'] }}</p>
    </td>
    <td>{!! Form::label('z_minus', 'z minus:') !!}
    <p>{{ $plan['0']['z_minus'] }}</p>
    </td>
    <td>{!! Form::label('z_plus', 'z_plus:') !!}
    <p>{{ $plan['0']['z_plus'] }}</p>
    </td>
    <td>{!! Form::label('q_minus', 'q minus:') !!}
    <p>{{ $plan['0']['q_minus'] }}</p>
    </td>
    <td>{!! Form::label('q_plus', 'q plus:') !!}
    <p>{{ $plan['0']['q_plus'] }}</p>
    </td>
    <td></td>
    </tr>
    </table>
    <br>
    
    <h2>Laporan Informasi Schedule Simpang {{ $detailSimpang->nama_lokasi }}</h2>
    <table id="table" class="uk-table uk-table-hover uk-table-striped">
    <tr>
    <td>{!! Form::label('nomor_plan', 'nomor plan:') !!}
    <p>{{ $schedule['0']['nomor_plan'] }}</p>
    </td>
    <td>{!! Form::label('ct', 'Cycle Time:') !!}
    <p>{{ $schedule['0']['ct'] }}</p>
    </td>
    <td>{!! Form::label('time', 'time:') !!}
    <p>{{ $schedule['0']['time'] }}</p>
    </td>
    <td>{!! Form::label('days', 'days:') !!}
    <p>{{ $schedule['0']['days'] }}</p>
    </td>
    </tr>
    </table>
</body>

</html>