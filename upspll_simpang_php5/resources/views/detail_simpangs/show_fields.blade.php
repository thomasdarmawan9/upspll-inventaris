<!-- Id Lokasi Fk Field -->
<div class="col-sm-12">
    {!! Form::label('nama_lokasi', 'Nama lokasi:') !!}
    <p>{{ $detailSimpang->nama_lokasi }}</p>
</div>
<div class="col-sm-4">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $detailSimpang->alamat }}</p>
</div>
<!-- Lat Field -->
<div class="col-sm-4">
    {!! Form::label('lat', 'Lat:') !!}
    <p>{{ $detailSimpang->lat }}</p>
</div>

<!-- Long Field -->
<div class="col-sm-4">
    {!! Form::label('long', 'Long:') !!}
    <p>{{ $detailSimpang->long }}</p>
</div>

<!-- Tiang Field -->
<div class="col-sm-4">
    {!! Form::label('tiang', 'Tiang:') !!}
    <p>{{ $detailSimpang->tiang }}</p>
</div>
<div class="col-sm-4">
    {!! Form::label('jml_tiang', 'Jumlah Tiang:') !!}
    <p>{{ $detailSimpang->jml_tiang }}</p>
</div>
<!-- Unit Lampu Field -->
<div class="col-sm-4">
    {!! Form::label('unit_lampu', 'Unit Lampu:') !!}
    <p>{{ $detailSimpang->unit_lampu }}</p>
</div>
<div class="col-sm-4">
    {!! Form::label('jml_lampu', 'Jumlah Lampu:') !!}
    <p>{{ $detailSimpang->jml_lampu }}</p>
</div>
<!-- Thn Psng Unit Field -->
<div class="col-sm-4">
    {!! Form::label('tahun_psng_unit', 'Thn Psng Unit:') !!}
    <p>{{ $detailSimpang->thn_psng_unit }}</p>
</div>

<!-- Controller Field -->
<div class="col-sm-4">
    {!! Form::label('controller', 'Controller:') !!}
    <p>{{ $detailSimpang->controller }}</p>
</div>

<!-- Thn Psng Controller Field -->
<div class="col-sm-4">
    {!! Form::label('tahun_psng_controller', 'Thn Psng Controller:') !!}
    <p>{{ $detailSimpang->thn_psng_controller }}</p>
</div>

<!-- Controller Pc Field -->
<div class="col-sm-6">
    {!! Form::label('controller_pc', 'Controller Pc:') !!}
    <p>{{ $detailSimpang->controller_pc }}</p>
</div>

<!-- gambar_simpang Field -->
<div class="col-sm-12">
    {!! Form::label('gambar_simpang', 'Gambar Simpang:') !!}
    <p><img src="{{ asset('uploads'.'/'. $detailSimpang->gambar_simpang) }}" alt="gambar tidak ada" /></p>
</div>


<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
        {!! Form::label('fase_a', 'Fase A:') !!}
        <p> <img src="{{ asset('uploads'.'/'. $detailSimpang->fase_a) }}" alt="gambar tidak ada" />
        </p>
    </div>
</div>

<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_b', 'Fase B:') !!}
    <p><img src="{{ asset('uploads'.'/'. $detailSimpang->fase_b) }}" alt="gambar tidak ada" /></p>
    </div>
</div>

<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_c', 'Fase C:') !!}
    <p><img src="{{ asset('uploads'.'/'. $detailSimpang->fase_c) }}" alt="gambar tidak ada" /></p>
    </div>
</div>

<div class="col-sm-12 uk-margin-small-top"></div>

<!-- Fase D Field -->
<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_d', 'Fase D:') !!}
    <p> <img src="{{ asset('uploads'.'/'. $detailSimpang->fase_d) }}" alt="gambar tidak ada" /></p>
    </div>
</div>
<!-- Fase E Field -->
<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_e', 'Fase E:') !!}
    <p> <img src="{{ asset('uploads'.'/'. $detailSimpang->fase_e) }}" alt="gambar tidak ada" /></p>
    </div>
</div>

<!-- Fase F Field -->
<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_f', 'Fase F:') !!}
    <p> <img src="{{ asset('uploads'.'/'. $detailSimpang->fase_f) }}" alt="gambar tidak ada" /></p>
    </div>
</div>