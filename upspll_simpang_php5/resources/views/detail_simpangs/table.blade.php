
     <div class="table-responsive" style="width:100%;">
    <table id="table" class="uk-table uk-table-hover uk-table-striped" >
        <thead>
            <tr>
        <th>Nama Lokasi</th>
        <th>Lat</th>
        <th>Long</th>
        <th>Unit Lampu</th>
        <th>Controller</th>
        
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($detailSimpangs as $detailSimpang)
            <tr>
                <td>{{ $detailSimpang->nama_lokasi }}</td>
            <td>{{ $detailSimpang->lat }}</td>
            <td>{{ $detailSimpang->long }}</td>
            <td>{{ $detailSimpang->unit_lampu }}</td>
            <td>{{ $detailSimpang->controller }}</td>
            
                <td>
                    {!! Form::open(['route' => ['detailSimpangs.destroy', $detailSimpang->id], 'method' => 'delete']) !!}
                    <div class='btn-group text-center'>
                        <a href="{{ route('detailSimpangs.show', [$detailSimpang->id]) }}" class='uk-button uk-button-default uk-button-small '><i class="glyphicon glyphicon-eye-open"></i></a>
                        @if(auth::user()->role=='Admin')
                        <a href="{{ route('detailSimpangs.edit', [$detailSimpang->id]) }}" class='uk-button uk-button-primary uk-button-small'><i class="glyphicon glyphicon-edit"></i> </a>
                        <a href="/plansimpangadd/{{$detailSimpang->id}}" class="uk-button uk-button-primary uk-button-small" style="background:#0097a7 !important"><i class="glyphicon glyphicon-list-alt"></i> </a>
                      <a href="/schedulesimpangadd/{{$detailSimpang->id}}" class="uk-button uk-button-primary uk-button-small" style="background:#3949ab  !important"><i class="glyphicon glyphicon-check"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'uk-button uk-button-danger uk-button-small', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endif
                        @if(auth::user()->role=='Operator')
                        <a href="{{ route('detailSimpangs.edit', [$detailSimpang->id]) }}" class='uk-button uk-button-primary uk-button-small'><i class="glyphicon glyphicon-edit"></i> </a>
                        <a href="/plansimpangadd/{{$detailSimpang->id}}" class="uk-button uk-button-primary uk-button-small" style="background:#0097a7 !important"><i class="glyphicon glyphicon-list-alt"></i> </a>
                      <a href="/schedulesimpangadd/{{$detailSimpang->id}}" class="uk-button uk-button-primary uk-button-small" style="background:#3949ab  !important"><i class="glyphicon glyphicon-check"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'uk-button uk-button-danger uk-button-small', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endif
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>