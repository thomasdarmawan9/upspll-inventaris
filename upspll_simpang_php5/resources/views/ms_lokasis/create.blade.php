@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
           Master Lokasi
        </h1>
    </section>
    <div class="content">
    @if(auth::user()->role=='Admin')
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'msLokasis.store']) !!}

                        @include('ms_lokasis.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endif
        @if(auth::user()->role=='Operator')
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'msLokasis.store']) !!}

                        @include('ms_lokasis.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endif
        @if(auth::user()->role=='Teknisi')
        <h1>
        404 Akses ditolak
        </h1>
        @endif
    </div>
@endsection
