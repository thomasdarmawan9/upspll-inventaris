<div class="table-responsive" style="width:100%;">
<table id="table" class="uk-table uk-table-hover uk-table-striped">
<thead>
            <tr>
                <th>Nomor</th>
        <th>SID</th>
        <th>Nama Lokasi</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody> 
        @foreach($msLokasis as $msLokasi)
            <tr>
                <td>{{ $msLokasi->id }}</td>
            <td>{{ $msLokasi->nomor_lokasi }}</td>
            <td>{{ $msLokasi->nama_lokasi }}</td>
                <td>
                    {!! Form::open(['route' => ['msLokasis.destroy', $msLokasi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('msLokasis.show', [$msLokasi->id]) }}" class='uk-button uk-button-default '><i class="glyphicon glyphicon-eye-open"></i></a>
                        @if(auth::user()->role=='Admin')
                        <a href="{{ route('msLokasis.edit', [$msLokasi->id]) }}" class='uk-button uk-button-primary ' ><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'uk-button uk-button-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endif
                        @if(auth::user()->role=='Operator')
                        <a href="{{ route('msLokasis.edit', [$msLokasi->id]) }}" class='uk-button uk-button-primary ' ><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'uk-button uk-button-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endif
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
</table>
</div>

