<!-- Nomor Lokasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nomor_lokasi', 'SID:') !!}
    {!! Form::text('nomor_lokasi', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Lokasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    {!! Form::text('nama_lokasi', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'uk-button uk-button-primary uk-button uk-button-small']) !!}
    <a href="{!! route('msLokasis.index') !!}" class="uk-button uk-button-default uk-button-small" >Cancel</a>
</div>
