<table class="table table-responsive" id="scheduleSimpangs-table">
    <thead>
        <tr>
            <th>Nama Lokasi</th>
        <th>Nomor Plan</th>
        <th>Ct</th>
        <th>Time</th>
        <th>Days</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($scheduleSimpangs as $scheduleSimpang)
        <tr>
            <td>{!! $scheduleSimpang->nama_lokasi !!}</td>
            <td>{!! $scheduleSimpang->nomor_plan !!}</td>
            <td>{!! $scheduleSimpang->ct !!}</td>
            <td>{!! $scheduleSimpang->time !!}</td>
            <td>{!! $scheduleSimpang->days !!}</td>
            <td>
                {!! Form::open(['route' => ['scheduleSimpangs.destroy', $scheduleSimpang->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('scheduleSimpangs.show', [$scheduleSimpang->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('scheduleSimpangs.edit', [$scheduleSimpang->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>