@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Plan Simpang
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($planSimpang, ['route' => ['planSimpangs.update', $planSimpang->id], 'method' => 'patch']) !!}

                        @include('plan_simpangs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection