<table class="table table-responsive" id="planSimpangs-table">
    <thead>
        <tr>
            <th>Nama Lokasi</th>
        <th>Nomor Plan</th>
        <th>Cycle Time</th>
        <th>Offset</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
        <th>E</th>
        <th>F</th>
        <th>R Minus</th>
        <th>R Plus</th>
        <th>Y Minus</th>
        <th>Y Plus</th>
        <th>Z Minus</th>
        <th>Z Plus</th>
        <th>Q Minus</th>
        <th>Q Plus</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($planSimpangs as $planSimpang)
        <tr>
            <td>{!! $planSimpang->nama_lokasi !!}</td>
            <td>{!! $planSimpang->nomor_plan !!}</td>
            <td>{!! $planSimpang->cycle_time !!}</td>
            <td>{!! $planSimpang->offset !!}</td>
            <td>{!! $planSimpang->a !!}</td>
            <td>{!! $planSimpang->b !!}</td>
            <td>{!! $planSimpang->c !!}</td>
            <td>{!! $planSimpang->d !!}</td>
            <td>{!! $planSimpang->e !!}</td>
            <td>{!! $planSimpang->f !!}</td>
            <td>{!! $planSimpang->r_minus !!}</td>
            <td>{!! $planSimpang->r_plus !!}</td>
            <td>{!! $planSimpang->y_minus !!}</td>
            <td>{!! $planSimpang->y_plus !!}</td>
            <td>{!! $planSimpang->z_minus !!}</td>
            <td>{!! $planSimpang->z_plus !!}</td>
            <td>{!! $planSimpang->q_minus !!}</td>
            <td>{!! $planSimpang->q_plus !!}</td>
            <td>
                {!! Form::open(['route' => ['planSimpangs.destroy', $planSimpang->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('planSimpangs.show', [$planSimpang->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('planSimpangs.edit', [$planSimpang->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>