<!-- Id Simpang Fk Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    {!! Form::hidden('nama_lokasi', $planSimpang->nama_lokasi) !!}
    <p>{!! Form::label('nama_lokasi2', $planSimpang->nama_lokasi, ['disabled'=>'true']) !!}</p>
</div>

<!-- Nomor Plan Field -->
<div class="form-group col-sm-2">
    {!! Form::label('nomor_plan', 'Nomor Plan:') !!}
    {!! Form::number('nomor_plan', null, ['class' => 'form-control']) !!}
</div>

<!-- Cycle Time Field -->
<div class="form-group col-sm-2">
    {!! Form::label('cycle_time', 'Cycle Time:') !!}
    {!! Form::number('cycle_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Offset Field -->
<div class="form-group col-sm-2">
    {!! Form::label('offset', 'Offset:') !!}
    {!! Form::number('offset', null, ['class' => 'form-control']) !!}
</div>

<!-- A Field -->
<div class="form-group col-sm-2">
    {!! Form::label('a', 'A:') !!}
    {!! Form::number('a', null, ['class' => 'form-control']) !!}
</div>

<!-- B Field -->
<div class="form-group col-sm-2">
    {!! Form::label('b', 'B:') !!}
    {!! Form::number('b', null, ['class' => 'form-control']) !!}
</div>

<!-- C Field -->
<div class="form-group col-sm-2">
    {!! Form::label('c', 'C:') !!}
    {!! Form::number('c', null, ['class' => 'form-control']) !!}
</div>

<!-- D Field -->
<div class="form-group col-sm-2">
    {!! Form::label('d', 'D:') !!}
    {!! Form::number('d', null, ['class' => 'form-control']) !!}
</div>

<!-- E Field -->
<div class="form-group col-sm-2">
    {!! Form::label('e', 'E:') !!}
    {!! Form::number('e', null, ['class' => 'form-control']) !!}
</div>

<!-- F Field -->
<div class="form-group col-sm-2">
    {!! Form::label('f', 'F:') !!}
    {!! Form::number('f', null, ['class' => 'form-control']) !!}
</div>

<!-- R Minus Field -->
<div class="form-group col-sm-2">
    {!! Form::label('r_minus', 'R-:') !!}
    {!! Form::text('r_minus', null, ['class' => 'form-control']) !!}
</div>

<!-- R Plus Field -->
<div class="form-group col-sm-2">
    {!! Form::label('r_plus', 'R+:') !!}
    {!! Form::text('r_plus', null, ['class' => 'form-control']) !!}
</div>

<!-- Y Minus Field -->
<div class="form-group col-sm-2">
    {!! Form::label('y_minus', 'Y-:') !!}
    {!! Form::text('y_minus', null, ['class' => 'form-control']) !!}
</div>

<!-- Y Plus Field -->
<div class="form-group col-sm-2">
    {!! Form::label('y_plus', 'Y+:') !!}
    {!! Form::text('y_plus', null, ['class' => 'form-control']) !!}
</div>

<!-- Z Minus Field -->
<div class="form-group col-sm-2">
    {!! Form::label('z_minus', 'Z-:') !!}
    {!! Form::text('z_minus', null, ['class' => 'form-control']) !!}
</div>

<!-- Z Plus Field -->
<div class="form-group col-sm-2">
    {!! Form::label('z_plus', 'Z+:') !!}
    {!! Form::text('z_plus', null, ['class' => 'form-control']) !!}
</div>

<!-- Q Minus Field -->
<div class="form-group col-sm-2">
    {!! Form::label('q_minus', 'Q-:') !!}
    {!! Form::text('q_minus', null, ['class' => 'form-control']) !!}
</div>

<!-- Q Plus Field -->
<div class="form-group col-sm-2">
    {!! Form::label('q_plus', 'Q+:') !!}
    {!! Form::text('q_plus', null, ['class' => 'form-control']) !!}
</div>

<div id="mydiv"></div>

<div class="form-group col-sm-4">
    <input type="button" class="uk-button uk-button-default uk-button-small" value="tambah plan" onclick="createNew()" />
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'uk-button uk-button-primary uk-button uk-button-small']) !!}
    <a href="{{ route('detailSimpangs.index') }}" class="uk-button uk-button-default uk-button-small">Cancel</a>
</div>


<script>
    var i = 2;

    function createNew() {
        $("#mydiv").append('<div class="col-sm-12" style="border-top: 3px solid #3c8dbc;padding-bottom:10px;width:99%;margin-left:5px; ">'+'</div>' + '<div class="form-group col-sm-2">' +'<label>'+'Nomor Plan :'+'</label>'+
            '<input type="number" name="nomor_plan_arr[]" class="form-control" placeholder=""/>' +
            '</div>' + '<div class="form-group col-sm-2">' +'<label>'+'Cycle Time:'+'</label>'+
            '<input type="number" name="cycle_time_arr[]" class="form-control" placeholder=""/>' +
            '</div>' + '<div class="form-group  col-sm-2">' + '<label>'+'Offset:'+'</label>'+
            '<input type="number" name="offset_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'A :'+'</label>'+
            '<input type="number" name="a_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'B :'+'</label>'+
            '<input type="number" name="b_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'C :'+'</label>'+
            '<input type="number" name="c_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'D :'+'</label>'+
            '<input type="number" name="d_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'E :'+'</label>'+
            '<input type="number" name="e_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'F :'+'</label>'+
            '<input type="number" name="f_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'R- :'+'</label>'+
            '<input type="text" name="r_minus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'R+ :'+'</label>'+
            '<input type="text" name="r_plus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Y- :'+'</label>'+
            '<input type="text" name="y_minus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Y+ :'+'</label>'+
            '<input type="text" name="y_plus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Z- :'+'</label>'+
            '<input type="text" name="z_minus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Z+ :'+'</label>'+
            '<input type="text" name="z_plus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Q- :'+'</label>'+
            '<input type="text" name="q_minus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Q+ :'+'</label>'+
            '<input type="text" name="q_plus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<br/>'+
            '<div class="form-group col-sm-2">' +
            '<input type="hidden" name="nama_lokasi_arr[]" class="form-control" placeholder="" value="{!!$planSimpang->nama_lokasi!!}"/>' +
            '</div>');
        i++;
    }

</script>
