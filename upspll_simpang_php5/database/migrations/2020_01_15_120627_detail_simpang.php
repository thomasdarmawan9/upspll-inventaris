<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetailSimpang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_simpang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_lokasi');
            $table->string('alamat');
            $table->integer('lat');
            $table->integer('long');
            $table->string('tiang');
            $table->integer('jml_tiang');
            $table->string('unit_lampu');
            $table->integer('jml_lampu');
            $table->string('controller');
            $table->string('thn_psng_controller');
            $table->string('thn_psng_unit');
            $table->string('controller_pc');
            $table->string('fase_a')->nullable();
            $table->string('fase_b')->nullable();
            $table->string('fase_c')->nullable();
            $table->string('fase_d')->nullable();
            $table->string('fase_e')->nullable();
            $table->string('fase_f')->nullable();
            $table->string('gambar_simpang')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
