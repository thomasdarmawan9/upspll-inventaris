<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanSimpang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_simpang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_lokasi');
            $table->integer('nomor_plan');
            $table->integer('cycle_time')->nullable();
            $table->integer('offset')->nullable();
            $table->integer('a')->nullable();
            $table->integer('b')->nullable();
            $table->integer('c')->nullable();
            $table->integer('d')->nullable();
            $table->integer('e')->nullable();
            $table->integer('f')->nullable();
            $table->string('r_minus')->nullable();
            $table->string('r_plus')->nullable();
            $table->string('y_minus')->nullable();
            $table->string('y_plus')->nullable();
            $table->string('z_minus')->nullable();
            $table->string('z_plus')->nullable();
            $table->string('q_minus')->nullable();
            $table->string('q_plus')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
