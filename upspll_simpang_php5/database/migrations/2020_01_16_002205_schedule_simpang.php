<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScheduleSimpang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_simpang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_lokasi');
            $table->integer('nomor_plan');
            $table->integer('ct')->nullable();
            $table->time('time')->nullable();
            $table->string('days')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
