<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('auth/login');
});

Route::group(['middleware' => ['auth']], function() {
    Route::get('/', 'ms_lokasiController@index');
    
Route::get('/home', 'HomeController@index');
// Route::get('/lokasi', 'lokasiController@index');
// Route::get('/lokasi/cetak_pdf', 'lokasiController@cetak_pdf');


Route::get('/detailSimpang/cetak_pdf/{id}', 'detail_simpangController@cetak_pdf');

Route::resource('msLokasis', 'ms_lokasiController');

Route::resource('detailSimpangs', 'detail_simpangController');

Route::resource('planSimpangs', 'plan_simpangController');

Route::get('/plansimpangadd/{id}', 'plan_simpangController@addsimpang');

Route::resource('scheduleSimpangs', 'schedule_simpangController');

Route::get('/schedulesimpangadd/{id}', 'schedule_simpangController@addsimpang');

Route::get('/detail_simpangController/fetch_data', 'detail_simpangController@fetch_data');

Route::post('/detail_simpangController/update_data', 'detail_simpangController@update_data')->name('detail_simpangController.update_data');
Route::post('/detail_simpangController/delete_data', 'detail_simpangController@delete_data')->name('detail_simpangController.delete_data');


Route::get('/detail_simpangController/fetch_data2', 'detail_simpangController@fetch_data2');

Route::post('/detail_simpangController/update_data2', 'detail_simpangController@update_data2')->name('detail_simpangController.update_data2');
Route::post('/detail_simpangController/delete_data2', 'detail_simpangController@delete_data2')->name('detail_simpangController.delete_data2');


});

      