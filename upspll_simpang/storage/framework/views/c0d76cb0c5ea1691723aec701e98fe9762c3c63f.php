<!-- Id Simpang Fk Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('nama_lokasi', 'Nama Lokasi:'); ?>

    <?php echo Form::hidden('nama_lokasi', $planSimpang->nama_lokasi); ?>

    <?php echo Form::text('nama_lokasi2', $planSimpang->nama_lokasi, ['class' => 'form-control','disabled'=>'true']); ?>

</div>

<!-- Nomor Plan Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('nomor_plan', 'Nomor Plan:'); ?>

    <?php echo Form::number('nomor_plan', null, ['class' => 'form-control']); ?>

</div>

<!-- Cycle Time Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('cycle_time', 'Cycle Time:'); ?>

    <?php echo Form::number('cycle_time', null, ['class' => 'form-control']); ?>

</div>

<!-- Offset Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('offset', 'Offset:'); ?>

    <?php echo Form::number('offset', null, ['class' => 'form-control']); ?>

</div>

<!-- A Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('a', 'A:'); ?>

    <?php echo Form::number('a', null, ['class' => 'form-control']); ?>

</div>

<!-- B Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('b', 'B:'); ?>

    <?php echo Form::number('b', null, ['class' => 'form-control']); ?>

</div>

<!-- C Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('c', 'C:'); ?>

    <?php echo Form::number('c', null, ['class' => 'form-control']); ?>

</div>

<!-- D Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('d', 'D:'); ?>

    <?php echo Form::number('d', null, ['class' => 'form-control']); ?>

</div>

<!-- E Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('e', 'E:'); ?>

    <?php echo Form::number('e', null, ['class' => 'form-control']); ?>

</div>

<!-- F Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('f', 'F:'); ?>

    <?php echo Form::number('f', null, ['class' => 'form-control']); ?>

</div>

<!-- R Minus Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('r_minus', 'R-:'); ?>

    <?php echo Form::text('r_minus', null, ['class' => 'form-control']); ?>

</div>

<!-- R Plus Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('r_plus', 'R+:'); ?>

    <?php echo Form::text('r_plus', null, ['class' => 'form-control']); ?>

</div>

<!-- Y Minus Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('y_minus', 'Y-:'); ?>

    <?php echo Form::text('y_minus', null, ['class' => 'form-control']); ?>

</div>

<!-- Y Plus Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('y_plus', 'Y+:'); ?>

    <?php echo Form::text('y_plus', null, ['class' => 'form-control']); ?>

</div>

<!-- Z Minus Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('z_minus', 'Z-:'); ?>

    <?php echo Form::text('z_minus', null, ['class' => 'form-control']); ?>

</div>

<!-- Z Plus Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('z_plus', 'Z+:'); ?>

    <?php echo Form::text('z_plus', null, ['class' => 'form-control']); ?>

</div>

<!-- Q Minus Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('q_minus', 'Q-:'); ?>

    <?php echo Form::text('q_minus', null, ['class' => 'form-control']); ?>

</div>

<!-- Q Plus Field -->
<div class="form-group col-sm-2">
    <?php echo Form::label('q_plus', 'Q+:'); ?>

    <?php echo Form::text('q_plus', null, ['class' => 'form-control']); ?>

</div>

<div id="mydiv"></div>

<div class="form-group col-sm-2">
    <input type="button" class="btn btn-default" value="tambah plan" onclick="createNew()" />
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo e(route('planSimpangs.index')); ?>" class="btn btn-default">Cancel</a>
</div>


<script>
    var i = 2;

    function createNew() {
        $("#mydiv").append('<div class="col-sm-12" style="border-top: 3px solid #3c8dbc;padding-bottom:10px;width:99%;margin-left:5px; ">'+'</div>' + '<div class="form-group col-sm-2">' +'<label>'+'Nama Lokasi :'+'</label>'+
            '<input type="text" name="nama_lokasi_arr[]" class="form-control" placeholder="" value="<?php echo $planSimpang->nama_lokasi; ?>"/>' +
            '</div>' + '<div class="form-group col-sm-2">' +'<label>'+'Nomor Plan :'+'</label>'+
            '<input type="number" name="nomor_plan_arr[]" class="form-control" placeholder=""/>' +
            '</div>' + '<div class="form-group col-sm-2">' +'<label>'+'Cycle Time:'+'</label>'+
            '<input type="number" name="cycle_time_arr[]" class="form-control" placeholder=""/>' +
            '</div>' + '<div class="form-group  col-sm-2">' + '<label>'+'Offset:'+'</label>'+
            '<input type="number" name="offset_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'A :'+'</label>'+
            '<input type="number" name="a_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'B :'+'</label>'+
            '<input type="number" name="b_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'C :'+'</label>'+
            '<input type="number" name="c_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'D :'+'</label>'+
            '<input type="number" name="d_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'E :'+'</label>'+
            '<input type="number" name="e_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'F :'+'</label>'+
            '<input type="number" name="f_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'R- :'+'</label>'+
            '<input type="number" name="r_minus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'R+ :'+'</label>'+
            '<input type="number" name="r_plus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Y- :'+'</label>'+
            '<input type="number" name="y_minus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Y+ :'+'</label>'+
            '<input type="number" name="y_plus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Z- :'+'</label>'+
            '<input type="number" name="z_minus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Z+ :'+'</label>'+
            '<input type="number" name="z_plus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Q- :'+'</label>'+
            '<input type="number" name="q_minus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group  col-sm-2">' +'<label>'+'Q+ :'+'</label>'+
            '<input type="number" name="q_plus_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<br/>');
        i++;
    }

</script>
