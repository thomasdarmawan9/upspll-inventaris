
    <table id="table" class="display" >
        <thead>
            <tr>
                <th>Nama Lokasi</th>
        <th>Lat</th>
        <th>Long</th>
        <th>Tiang</th>
        <th>Unit Lampu</th>
        <th>Controller</th>
        <th>Thn Psng Controller</th>
        <th>Thn Psng Unit</th>
        <th>Controller Pc</th>
        
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $detailSimpangs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detailSimpang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($detailSimpang->nama_lokasi); ?></td>
            <td><?php echo e($detailSimpang->lat); ?></td>
            <td><?php echo e($detailSimpang->long); ?></td>
            <td><?php echo e($detailSimpang->tiang); ?></td>
            <td><?php echo e($detailSimpang->unit_lampu); ?></td>
            <td><?php echo e($detailSimpang->controller); ?></td>
            <td><?php echo e($detailSimpang->thn_psng_controller); ?></td>
            <td><?php echo e($detailSimpang->thn_psng_unit); ?></td>
            <td><?php echo e($detailSimpang->controller_pc); ?></td>
            
                <td>
                    <?php echo Form::open(['route' => ['detailSimpangs.destroy', $detailSimpang->id], 'method' => 'delete']); ?>

                    <div class='btn-group'>
                        <a href="<?php echo e(route('detailSimpangs.show', [$detailSimpang->id])); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i> Lihat Detail</a>
                        <a href="/plansimpangadd/<?php echo e($detailSimpang->id); ?>" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Plan Simpang</a>
                      <a href="<?php echo e(route('detailSimpangs.edit', [$detailSimpang->id])); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i> Edit Info</a>
                        <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                    </div>
                    <?php echo Form::close(); ?>

                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
