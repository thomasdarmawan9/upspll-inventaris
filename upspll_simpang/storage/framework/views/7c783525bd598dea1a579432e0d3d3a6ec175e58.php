

<li class="<?php echo e(Request::is('msLokasis*') ? 'active' : ''); ?>">
    <a href="<?php echo e(route('msLokasis.index')); ?>" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Master Lokasi</span></a>
</li>

<li class="<?php echo e(Request::is('detailSimpangs*') ? 'active' : ''); ?>">
    <a href="<?php echo e(route('detailSimpangs.index')); ?>" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Detail Simpang</span></a>
</li>

<li class="<?php echo e(Request::is('planSimpangs*') ? 'active' : ''); ?>">
    <a href="<?php echo e(route('planSimpangs.index')); ?>" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Plan Simpang</span></a>
</li>

