<!-- gambar_simpang Field -->
<div class="col-sm-12">
    <?php echo Form::label('gambar_simpang', 'gambar simpang:'); ?>

    <p><?php echo e($detailSimpang->gambar_simpang); ?></p>
</div>

<!-- Id Lokasi Fk Field -->
<div class="col-sm-12">
    <?php echo Form::label('nama_lokasi', 'Nama lokasi:'); ?>

    <p><?php echo e($detailSimpang->nama_lokasi); ?></p>
</div>

<!-- Lat Field -->
<div class="col-sm-4">
    <?php echo Form::label('lat', 'Lat:'); ?>

    <p><?php echo e($detailSimpang->lat); ?></p>
</div>

<!-- Long Field -->
<div class="col-sm-4">
    <?php echo Form::label('long', 'Long:'); ?>

    <p><?php echo e($detailSimpang->long); ?></p>
</div>

<!-- Tiang Field -->
<div class="col-sm-4">
    <?php echo Form::label('tiang', 'Tiang:'); ?>

    <p><?php echo e($detailSimpang->tiang); ?></p>
</div>

<!-- Unit Lampu Field -->
<div class="col-sm-4">
    <?php echo Form::label('unit_lampu', 'Unit Lampu:'); ?>

    <p><?php echo e($detailSimpang->unit_lampu); ?></p>
</div>

<!-- Thn Psng Unit Field -->
<div class="col-sm-4">
    <?php echo Form::label('thn_psng_unit', 'Thn Psng Unit:'); ?>

    <p><?php echo e($detailSimpang->thn_psng_unit); ?></p>
</div>

<!-- Controller Field -->
<div class="col-sm-4">
    <?php echo Form::label('controller', 'Controller:'); ?>

    <p><?php echo e($detailSimpang->controller); ?></p>
</div>

<!-- Thn Psng Controller Field -->
<div class="col-sm-4">
    <?php echo Form::label('thn_psng_controller', 'Thn Psng Controller:'); ?>

    <p><?php echo e($detailSimpang->thn_psng_controller); ?></p>
</div>

<!-- Controller Pc Field -->
<div class="col-sm-4">
    <?php echo Form::label('controller_pc', 'Controller Pc:'); ?>

    <p><?php echo e($detailSimpang->controller_pc); ?></p>
</div>

<!-- Fase A Field -->
<div class="col-sm-4">
    <?php echo Form::label('fase_a', 'Fase A:'); ?>

    <p><?php echo e($detailSimpang->fase_a); ?></p>
    <img   src="<?php echo e(asset('uploads'.'/'. $detailSimpang->fase_a)); ?>"  alt="gambar tidak ada" />
    
</div>

<!-- Fase B Field -->
<div class="col-sm-4">
    <?php echo Form::label('fase_b', 'Fase B:'); ?>

    <p><?php echo e($detailSimpang->fase_b); ?></p>
    <img   src="<?php echo e(asset('uploads'.'/'. $detailSimpang->fase_b)); ?>" alt="gambar tidak ada" />
</div>

<!-- Fase C Field -->
<div class="col-sm-4">
    <?php echo Form::label('fase_c', 'Fase C:'); ?>

    <p><?php echo e($detailSimpang->fase_c); ?></p>
    <img   src="<?php echo e(asset('uploads'.'/'. $detailSimpang->fase_c)); ?>" alt="gambar tidak ada" />
</div>

<!-- Fase D Field -->
<div class="col-sm-4">
    <?php echo Form::label('fase_d', 'Fase D:'); ?>

    <p><?php echo e($detailSimpang->fase_d); ?></p>
    <img   src="<?php echo e(asset('uploads'.'/'. $detailSimpang->fase_d)); ?>" alt="gambar tidak ada" />
</div>

<!-- Fase E Field -->
<div class="col-sm-4">
    <?php echo Form::label('fase_e', 'Fase E:'); ?>

    <p><?php echo e($detailSimpang->fase_e); ?></p>
    <img   src="<?php echo e(asset('uploads'.'/'. $detailSimpang->fase_e)); ?>" alt="gambar tidak ada" />
</div>

<!-- Fase F Field -->
<div class="col-sm-4">
    <?php echo Form::label('fase_f', 'Fase F:'); ?>

    <p><?php echo e($detailSimpang->fase_f); ?></p>
    <img   src="<?php echo e(asset('uploads'.'/'. $detailSimpang->fase_f)); ?>" alt="gambar tidak ada" />
</div>
