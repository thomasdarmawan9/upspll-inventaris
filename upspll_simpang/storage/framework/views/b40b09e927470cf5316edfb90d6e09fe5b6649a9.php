<div class="table-responsive">
    <table class="table" id="planSimpangs-table">
        <thead>
            <tr>
                <th>Nomor Lokasi</th>
        <th>Nomor Plan</th>
        <th>Cycle Time</th>
        <th>Offset</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
        <th>E</th>
        <th>F</th>
        <th>R Minus</th>
        <th>R Plus</th>
        <th>Y Minus</th>
        <th>Y Plus</th>
        <th>Z Minus</th>
        <th>Z Plus</th>
        <th>Q Minus</th>
        <th>Q Plus</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $planSimpangs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $planSimpang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($planSimpang->nama_lokasi); ?></td>
            <td><?php echo e($planSimpang->nomor_plan); ?></td>
            <td><?php echo e($planSimpang->cycle_time); ?></td>
            <td><?php echo e($planSimpang->offset); ?></td>
            <td><?php echo e($planSimpang->a); ?></td>
            <td><?php echo e($planSimpang->b); ?></td>
            <td><?php echo e($planSimpang->c); ?></td>
            <td><?php echo e($planSimpang->d); ?></td>
            <td><?php echo e($planSimpang->e); ?></td>
            <td><?php echo e($planSimpang->f); ?></td>
            <td><?php echo e($planSimpang->r_minus); ?></td>
            <td><?php echo e($planSimpang->r_plus); ?></td>
            <td><?php echo e($planSimpang->y_minus); ?></td>
            <td><?php echo e($planSimpang->y_plus); ?></td>
            <td><?php echo e($planSimpang->z_minus); ?></td>
            <td><?php echo e($planSimpang->z_plus); ?></td>
            <td><?php echo e($planSimpang->q_minus); ?></td>
            <td><?php echo e($planSimpang->q_plus); ?></td>
                <td>
                    <?php echo Form::open(['route' => ['planSimpangs.destroy', $planSimpang->id], 'method' => 'delete']); ?>

                    <div class='btn-group'>
                        <a href="<?php echo e(route('planSimpangs.show', [$planSimpang->id])); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="<?php echo e(route('planSimpangs.edit', [$planSimpang->id])); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                    </div>
                    <?php echo Form::close(); ?>

                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</div>
