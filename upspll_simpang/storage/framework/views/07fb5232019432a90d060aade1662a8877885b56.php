<table id="table" class="display">
<thead>
            <tr>
                <th>Id Lokasi</th>
        <th>Nomor Lokasi</th>
        <th>Nama Lokasi</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $msLokasis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $msLokasi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($msLokasi->id); ?></td>
            <td><?php echo e($msLokasi->nomor_lokasi); ?></td>
            <td><?php echo e($msLokasi->nama_lokasi); ?></td>
                <td>
                    <?php echo Form::open(['route' => ['msLokasis.destroy', $msLokasi->id], 'method' => 'delete']); ?>

                    <div class='btn-group'>
                        <a href="<?php echo e(route('msLokasis.show', [$msLokasi->id])); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="<?php echo e(route('msLokasis.edit', [$msLokasi->id])); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                    </div>
                    <?php echo Form::close(); ?>

                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
</table>

