<?php $__env->startSection('content'); ?>
<section class="content-header">
    <h1>
        Detail Simpang
    </h1>
</section>

<div class="content">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row" style="padding-left: 20px">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home"><b>Planning</b></a></li>
                    <li><a data-toggle="tab" href="#menu1"><b>Schedule</b></a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active" style="padding-top:10px;">
                        <h3>PLANNING</h3>
                        <p>
                            <center>
                                <div class="table-responsive" style="width:95%;padding-top:20px;">
                                    <table class="display" id="table">
                                        <thead>
                                            <tr>
                                                <th>Nomor Lokasi</th>
                                                <th>Nomor Plan</th>
                                                <th>Cycle Time</th>
                                                <th>Offset</th>
                                                <th>A</th>
                                                <th>B</th>
                                                <th>C</th>
                                                <th>D</th>
                                                <th>E</th>
                                                <th>F</th>
                                                <th>R Minus</th>
                                                <th>R Plus</th>
                                                <th>Y Minus</th>
                                                <th>Y Plus</th>
                                                <th>Z Minus</th>
                                                <th>Z Plus</th>
                                                <th>Q Minus</th>
                                                <th>Q Plus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = $plan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $planSimpang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($planSimpang->nama_lokasi); ?></td>
                                                <td><?php echo e($planSimpang->nomor_plan); ?></td>
                                                <td><?php echo e($planSimpang->cycle_time); ?></td>
                                                <td><?php echo e($planSimpang->offset); ?></td>
                                                <td><?php echo e($planSimpang->a); ?></td>
                                                <td><?php echo e($planSimpang->b); ?></td>
                                                <td><?php echo e($planSimpang->c); ?></td>
                                                <td><?php echo e($planSimpang->d); ?></td>
                                                <td><?php echo e($planSimpang->e); ?></td>
                                                <td><?php echo e($planSimpang->f); ?></td>
                                                <td><?php echo e($planSimpang->r_minus); ?></td>
                                                <td><?php echo e($planSimpang->r_plus); ?></td>
                                                <td><?php echo e($planSimpang->y_minus); ?></td>
                                                <td><?php echo e($planSimpang->y_plus); ?></td>
                                                <td><?php echo e($planSimpang->z_minus); ?></td>
                                                <td><?php echo e($planSimpang->z_plus); ?></td>
                                                <td><?php echo e($planSimpang->q_minus); ?></td>
                                                <td><?php echo e($planSimpang->q_plus); ?></td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </center>
                        </p>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <h3>Schedule</h3>
                        <p></p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" style="padding-left: 20px">
                <?php echo $__env->make('detail_simpangs.show_fields', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="col-sm-12" style="padding-top:30px">
                <a href="<?php echo e(route('detailSimpangs.index')); ?>" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>