<!-- Id Lokasi Fk Field -->
<div class="form-group col-sm-4">
    <?php echo Form::label('nama_lokasi', 'Nama Lokasi:'); ?>

    <?php echo Form::select('nama_lokasi', $nama_lokasi, null, ['class' => 'form-control']); ?>

</div>

<!-- Lat Field -->
<div class="form-group col-sm-4">
    <?php echo Form::label('lat', 'Lat:'); ?>

    <?php echo Form::number('lat', null, ['class' => 'form-control']); ?>

</div>

<!-- Long Field -->
<div class="form-group col-sm-4">
    <?php echo Form::label('long', 'Long:'); ?>

    <?php echo Form::number('long', null, ['class' => 'form-control']); ?>

</div>

<!-- Tiang Field -->

<style>
.setengah {
  content: "\00BD";
}
</style>
<div class="form-group has-feedback<?php echo e($errors->has('tiang') ? ' has-error' : ''); ?> col-sm-12">
<?php echo Form::label('tiang', 'Tiang:'); ?>

            <select class="form-control" name="tiang"  value="<?php echo e(old('tiang')); ?>" >
                <option value="">----pilih jenis tiang----</option>
                <option value="tiang lurus 4 meter bulat">tiang lurus 4 meter bulat</option>
                <option value="tiang lurus 4 meter oktagonal">tiang lurus 4 meter oktagonal</option>
                <option value="tiang lurus 6 meter bulat">tiang lurus 6 meter bulat</option>
                <option value="tiang lurus 6 meter oktagonal">tiang lurus 6 meter oktagonal</option>
                <option value="tiang lengkung bulat">tiang lengkung bulat</option>
                <option value="tiang lengkung oktagonal">tiang lengkung oktagonal</option>
                <option value="tiang F">tiang F</option>
                <option value="tiang siku bentangan 7 1&frasl;2">tiang siku bentangan 7<span class="setengah">&#189;</span></option>
                <option value="tiang siku bentangan 4 1&frasl;2">tiang siku bentangan 4<span class="setengah">&#189;</span></option>
            </select>
            </div>
<!-- Unit Lampu Field -->
<div class="form-group has-feedback<?php echo e($errors->has('unit_lampung') ? ' has-error' : ''); ?> col-sm-6 ">
<?php echo Form::label('unit_lampu', 'Unit Lampu:'); ?>

            <select class="form-control" name="unit_lampu"  value="<?php echo e(old('unit_lampu')); ?>" >
                <option value="">----pilih jenis unit lampu----</option>
                <option value="2 X 30 cm (MKH)">2 X 30 cm (MKH)</option>
                <option value="2 X 30 cm (K)">2 X 30 cm (K)</option>
                <option value="2 X 30 cm (MH)">2 X 30 cm (MH)</option>
                <option value="3 X 20 cm (MKH)">3 X 20 cm (MKH)</option>
                <option value="2 X 20 cm (K)">2 X 20 cm (K)</option>
                <option value="2 X 20 cm (MH)">2 X 20 cm (MH)</option>
            </select>
            </div>

<!-- Thn Psng Unit Field -->
<style>
#thn_psng_unit{
    display: block;
    width: 100%;
    padding: 6px 12px;
    font-size: 14px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
}
</style>
<div class="form-group col-sm-6">
    <?php echo Form::label('thn_psng_unit', 'Tahun Pasang Unit:'); ?>

    <br>
    <?php echo Form::selectYear('thn_psng_unit',1998, 2029, ['class' => 'form-control tahununit']); ?>

</div>

<div class="form-group col-sm-6">
</div>


<!-- Controller Field -->

<div class="form-group has-feedback<?php echo e($errors->has('controller') ? ' has-error' : ''); ?> col-sm-6">
<?php echo Form::label('controller', 'Controller:'); ?>

            <select class="form-control" name="controller"  value="<?php echo e(old('controller')); ?>" >
                <option value="">----pilih jenis controller----</option>
                <option value="Stats A (ATCS)">Stats A (ATCS)</option>
                <option value="Stats A (NON ATCS)">Stats A (NON ATCS)</option>
                <option value="Newtrap">Newtrap</option>
                <option value="UMC AC">UMC AC</option>
                <option value="UMC DC">UMC DC</option>
                <option value="HMK">HMK</option>
            </select>
            </div>

<!-- Thn Psng Controller Field -->
<style>
#thn_psng_controller{
    display: block;
    width: 100%;
    padding: 6px 12px;
    font-size: 14px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
}
</style>
<div class="form-group col-sm-6">
    <?php echo Form::label('thn_psng_controller', 'Tahun Pasang Controller:'); ?>

    <br>
    <?php echo Form::selectYear('thn_psng_controller',1998, 2029, ['class' => 'form-control']); ?>

</div>


<div class="form-group col-sm-6">
</div>


<!-- Controller Pc Field -->
<div class="form-group has-feedback<?php echo e($errors->has('controller_pc') ? ' has-error' : ''); ?> col-sm-6">
<?php echo Form::label('controller_pc', 'Controller PC:'); ?>

            <select class="form-control" name="controller_pc"  value="<?php echo e(old('controller_pc')); ?>" >
                <option value="">----pilih jenis controller PC----</option>
                <option value="Newtrap">Newtrap</option>
                <option value="UMC">UMC</option>
                <option value="HMK">HMK</option>
                <option value="Telepico">Telepico</option>
            </select>
            </div>


            <div class="form-group col-sm-12">
</div>


<!-- Fase A Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('fase_a', 'Fase A:'); ?>

    <?php echo Form::file('fase_a', null, ['class' => 'form-control']); ?>

</div>

<!-- Fase B Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('fase_b', 'Fase B:'); ?>

    <?php echo Form::file('fase_b', null, ['class' => 'form-control']); ?>

</div>

<!-- Fase C Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('fase_c', 'Fase C:'); ?>

    <?php echo Form::file('fase_c', null, ['class' => 'form-control']); ?>

</div>

<!-- Fase D Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('fase_d', 'Fase D:'); ?>

    <?php echo Form::file('fase_d', null, ['class' => 'form-control']); ?>

</div>

<!-- Fase E Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('fase_e', 'Fase E:'); ?>

    <?php echo Form::file('fase_e', null, ['class' => 'form-control']); ?>

</div>

<!-- Fase F Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('fase_f', 'Fase F:'); ?>

    <?php echo Form::file('fase_f', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo e(route('detailSimpangs.index')); ?>" class="btn btn-default">Cancel</a>
</div>
