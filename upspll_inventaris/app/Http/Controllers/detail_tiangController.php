<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createdetail_tiangRequest;
use App\Http\Requests\Updatedetail_tiangRequest;
use App\Repositories\detail_tiangRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class detail_tiangController extends AppBaseController
{
    /** @var  detail_tiangRepository */
    private $detailTiangRepository;

    public function __construct(detail_tiangRepository $detailTiangRepo)
    {
        $this->detailTiangRepository = $detailTiangRepo;
    }

    /**
     * Display a listing of the detail_tiang.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->detailTiangRepository->pushCriteria(new RequestCriteria($request));
        $detailTiangs = $this->detailTiangRepository->all();

        return view('detail_tiangs.index')
            ->with('detailTiangs', $detailTiangs);
    }

    /**
     * Show the form for creating a new detail_tiang.
     *
     * @return Response
     */
    public function create()
    {
        return view('detail_tiangs.create');
    }

    /**
     * Store a newly created detail_tiang in storage.
     *
     * @param Createdetail_tiangRequest $request
     *
     * @return Response
     */
    public function store(Createdetail_tiangRequest $request)
    {
        $input = $request->all();

        $detailTiang = $this->detailTiangRepository->create($input);

        Flash::success('Detail Tiang saved successfully.');

        return redirect(route('detailTiangs.index'));
    }

    /**
     * Display the specified detail_tiang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detailTiang = $this->detailTiangRepository->findWithoutFail($id);

        if (empty($detailTiang)) {
            Flash::error('Detail Tiang not found');

            return redirect(route('detailTiangs.index'));
        }

        return view('detail_tiangs.show')->with('detailTiang', $detailTiang);
    }

    /**
     * Show the form for editing the specified detail_tiang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detailTiang = $this->detailTiangRepository->findWithoutFail($id);

        if (empty($detailTiang)) {
            Flash::error('Detail Tiang not found');

            return redirect(route('detailTiangs.index'));
        }

        return view('detail_tiangs.edit')->with('detailTiang', $detailTiang);
    }

    /**
     * Update the specified detail_tiang in storage.
     *
     * @param  int              $id
     * @param Updatedetail_tiangRequest $request
     *
     * @return Response
     */
    public function update($id, Updatedetail_tiangRequest $request)
    {
        $detailTiang = $this->detailTiangRepository->findWithoutFail($id);

        if (empty($detailTiang)) {
            Flash::error('Detail Tiang not found');

            return redirect(route('detailTiangs.index'));
        }

        $detailTiang = $this->detailTiangRepository->update($request->all(), $id);

        Flash::success('Detail Tiang updated successfully.');

        return redirect(route('detailTiangs.index'));
    }

    /**
     * Remove the specified detail_tiang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detailTiang = $this->detailTiangRepository->findWithoutFail($id);

        if (empty($detailTiang)) {
            Flash::error('Detail Tiang not found');

            return redirect(route('detailTiangs.index'));
        }

        $this->detailTiangRepository->delete($id);

        Flash::success('Detail Tiang deleted successfully.');

        return redirect(route('detailTiangs.index'));
    }
}
