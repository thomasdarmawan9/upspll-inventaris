<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createmaster_tiangRequest;
use App\Http\Requests\Updatemaster_tiangRequest;
use App\Repositories\master_tiangRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class master_tiangController extends AppBaseController
{
    /** @var  master_tiangRepository */
    private $masterTiangRepository;

    public function __construct(master_tiangRepository $masterTiangRepo)
    {
        $this->masterTiangRepository = $masterTiangRepo;
    }

    /**
     * Display a listing of the master_tiang.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->masterTiangRepository->pushCriteria(new RequestCriteria($request));
        $masterTiangs = $this->masterTiangRepository->all();

        return view('master_tiangs.index')
            ->with('masterTiangs', $masterTiangs);
    }

    /**
     * Show the form for creating a new master_tiang.
     *
     * @return Response
     */
    public function create()
    {
        return view('master_tiangs.create');
    }

    /**
     * Store a newly created master_tiang in storage.
     *
     * @param Createmaster_tiangRequest $request
     *
     * @return Response
     */
    public function store(Createmaster_tiangRequest $request)
    {
        $input = $request->all();

        $masterTiang = $this->masterTiangRepository->create($input);

        Flash::success('Master Tiang saved successfully.');

        return redirect(route('masterTiangs.index'));
    }

    /**
     * Display the specified master_tiang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $masterTiang = $this->masterTiangRepository->findWithoutFail($id);

        if (empty($masterTiang)) {
            Flash::error('Master Tiang not found');

            return redirect(route('masterTiangs.index'));
        }

        return view('master_tiangs.show')->with('masterTiang', $masterTiang);
    }

    /**
     * Show the form for editing the specified master_tiang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $masterTiang = $this->masterTiangRepository->findWithoutFail($id);

        if (empty($masterTiang)) {
            Flash::error('Master Tiang not found');

            return redirect(route('masterTiangs.index'));
        }

        return view('master_tiangs.edit')->with('masterTiang', $masterTiang);
    }

    /**
     * Update the specified master_tiang in storage.
     *
     * @param  int              $id
     * @param Updatemaster_tiangRequest $request
     *
     * @return Response
     */
    public function update($id, Updatemaster_tiangRequest $request)
    {
        $masterTiang = $this->masterTiangRepository->findWithoutFail($id);

        if (empty($masterTiang)) {
            Flash::error('Master Tiang not found');

            return redirect(route('masterTiangs.index'));
        }

        $masterTiang = $this->masterTiangRepository->update($request->all(), $id);

        Flash::success('Master Tiang updated successfully.');

        return redirect(route('masterTiangs.index'));
    }

    /**
     * Remove the specified master_tiang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $masterTiang = $this->masterTiangRepository->findWithoutFail($id);

        if (empty($masterTiang)) {
            Flash::error('Master Tiang not found');

            return redirect(route('masterTiangs.index'));
        }

        $this->masterTiangRepository->delete($id);

        Flash::success('Master Tiang deleted successfully.');

        return redirect(route('masterTiangs.index'));
    }
}
