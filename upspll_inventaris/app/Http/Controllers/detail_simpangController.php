<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createdetail_simpangRequest;
use App\Http\Requests\Updatedetail_simpangRequest;
use App\Repositories\detail_simpangRepository;
use App\Repositories\schedule_simpangRepository;
use App\Repositories\plan_simpangRepository;
use App\Repositories\detail_lampuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use PDF;
use App\Models\detail_simpang;
use App\Models\plan_simpang;
use App\Models\schedule_simpang;
use App\Repositories\master_tiangRepository;
use App\Http\Requests\Createdetail_tiangRequest;
use App\Http\Requests\Updatedetail_tiangRequest;
use App\Http\Requests\Createdetail_lampuRequest;
use App\Http\Requests\Updatedetail_lampuRequest;
use App\Repositories\detail_tiangRepository;
class detail_simpangController extends AppBaseController
{
    /** @var  detail_simpangRepository */
    private $detailSimpangRepository;

    public function __construct(detail_simpangRepository $detailSimpangRepo, detail_lampuRepository $detailLampuRepo, detail_tiangRepository $detailTiangRepo, master_tiangRepository $masterTiangRepo, plan_simpangRepository $planSimpangRepo, schedule_simpangRepository $scheduleSimpangRepo)
    {
        $this->detailSimpangRepository = $detailSimpangRepo;
        $this->planSimpangRepository = $planSimpangRepo;
        $this->scheduleSimpangRepository = $scheduleSimpangRepo;
        $this->masterTiangRepository = $masterTiangRepo;
        $this->detailTiangRepository = $detailTiangRepo;
        $this->detailLampuRepository = $detailLampuRepo;
    }

    /**
     * Display a listing of the detail_simpang.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->detailSimpangRepository->pushCriteria(new RequestCriteria($request));
        $detailSimpangs = $this->detailSimpangRepository->all();

        return view('detail_simpangs.index')
            ->with('detailSimpangs', $detailSimpangs);
    }

    /**
     * Show the form for creating a new detail_simpang.
     *
     * @return Response
     */
    public function create()
    {
        $nama_lokasi = DB::table('ms_lokasi')->pluck('nama_lokasi','nama_lokasi');
        $jenis_tiang = DB::table('master_tiang')->pluck('jenis_tiang','jenis_tiang');

        $jenis_lampu = DB::table('master_lampu')->pluck('jenis_lampu','jenis_lampu');
        return view('detail_simpangs.creeate2', compact('nama_lokasi','nama_lokasi','jenis_tiang','jenis_lampu'));
    }

    /**
     * Store a newly created detail_simpang in storage.
     *
     * @param Createdetail_simpangRequest $request
     *
     * @return Response
     */
    public function store(Createdetail_simpangRequest $request)
    {
        $input = $request->all();

        echo "<pre>";
        // print_r($input);

        $gambar_simpang = $request->file('gambar_simpang');
        if($gambar_simpang != null){
        $input['gambar_simpang'] = md5($request->file('gambar_simpang')->getClientOriginalName().time()).'.'.$request->file('gambar_simpang')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('gambar_simpang')->move($destination, $input['gambar_simpang']);    
        }

        $fase_a = $request->file('fase_a');
        if($fase_a != null){
        $input['fase_a'] = md5($request->file('fase_a')->getClientOriginalName().time()).'.'.$request->file('fase_a')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_a')->move($destination, $input['fase_a']);    
        }

        $fase_b = $request->file('fase_b');
        if($fase_b != null){
        $input['fase_b'] = md5($request->file('fase_b')->getClientOriginalName().time()).'.'.$request->file('fase_b')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_b')->move($destination, $input['fase_b']);
        }

        $fase_c = $request->file('fase_c');
        if($fase_c != null){
        $input['fase_c'] = md5($request->file('fase_c')->getClientOriginalName().time()).'.'.$request->file('fase_c')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_c')->move($destination, $input['fase_c']);
        }

        $fase_d = $request->file('fase_d');
        if($fase_d != null){
        $input['fase_d'] = md5($request->file('fase_d')->getClientOriginalName().time()).'.'.$request->file('fase_d')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_d')->move($destination, $input['fase_d']);
        }

        $fase_e = $request->file('fase_e');
        if($fase_e != null){
        $input['fase_e'] = md5($request->file('fase_e')->getClientOriginalName().time()).'.'.$request->file('fase_e')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_e')->move($destination, $input['fase_e']);
        }

        $fase_f = $request->file('fase_f');
        if($fase_f != null){
        $input['fase_f'] = md5($request->file('fase_f')->getClientOriginalName().time()).'.'.$request->file('fase_f')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_f')->move($destination, $input['fase_f']);
        }
        
        $detailSimpang = $this->detailSimpangRepository->create($input);
        if (isset($input['jenis_tiang'])) {
        if(count($input['jenis_tiang'])>0){
           
            foreach ($input['jenis_tiang'] as $key => $value) {
               
                    
                //             // dd($detailLampu);
                            $detailTiang = $this->detailTiangRepository->create([
                                'nama_lokasi' => $input['nama_lokasi'],
                                'jenis_tiang' => $input['jenis_tiang'][$key],
                                'jml_tiang' => $input['jml_tiang'][$key],
                      
                            ]);
        }
        }
        }
        if (isset($input['jenis_lampu'])) {
            if(count($input['jenis_lampu'])>0){
               
                foreach ($input['jenis_lampu'] as $key => $value) {
                   
                        
                    //             // dd($detailLampu);
                                $detailLampu = $this->detailLampuRepository->create([
                                    'nama_lokasi' => $input['nama_lokasi'],
                                    'jenis_lampu' => $input['jenis_lampu'][$key],
                                    'jml_lampu' => $input['jml_lampu'][$key],
                                    'thn_psng_unit' => $input['thn_psng_unit'][$key],
                          
                                ]);
            }
            }
            }
        
        // dd($detailTiang);
        // if (isset($input['nama_lokasi_arr'])) {
        //     if(count($input['jenis_tiang'])>0){
              
        //         foreach ($input['jenis_tiang'] as $key => $value) {
                    
        //             // dd($detailLampu);
        //             $detailTiang = [
        //                 'nama_lokasi' => $i,
        //                 'jenis_tiang' => $input['jenis_tiang'],
              
        //             ];
                    
        //             // $detailLampu = $this->detailLampuRepository->create([
        //             //     'nama_lokasi' => $input['nama_lokasi_arr'][$key],
        //             //     'jenis_lampu' => $input['jenis_lampu'][$key],
        //             //     'jml_lampu' => $input['jml_lampu'][$key],
        //             //     'thn_psng_unit' => $input['thn_psng_unit'][$key],
        //             // ]);
                
        //     }
        // }
        // }
        
        // $detailTiang = $this->detailTiangRepository->create($input);
      
        Flash::success('Detail Simpang saved successfully.');

        return redirect(route('detailSimpangs.index'));
    }

    /**
     * Display the specified detail_simpang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);
        $plan= \App\Models\plan_simpang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->get();
        $schedule= \App\Models\schedule_simpang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->get();
        $detailTiang= \App\Models\detail_tiang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->where('jml_tiang','!=','null')->get();
        $detailLampu= \App\Models\detail_lampu::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->where('jml_lampu','!=','null')->get();
        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('detailSimpangs.index'));
        }
        // dd($plan);
        return view('detail_simpangs.show')->with(['detailSimpang'=> $detailSimpang,'plan'=>$plan,'schedule'=>$schedule, 'detailLampu' => $detailLampu, 'detailTiang' => $detailTiang]);
    }

 

    /**
     * Show the form for editing the specified detail_simpang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);

        $nama_lokasi = DB::table('ms_lokasi')->pluck('nama_lokasi','nama_lokasi');

        $jenis_tiang = DB::table('master_tiang')->pluck('jenis_tiang','jenis_tiang');

        $jenis_lampu = DB::table('master_lampu')->pluck('jenis_lampu','jenis_lampu');

        $detailTiang= \App\Models\detail_tiang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->get();
        $detailLampu= \App\Models\detail_lampu::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->get();
       
        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('detailSimpangs.index'));
        }

        return view('detail_simpangs.edit',compact('nama_lokasi','nama_lokasi','jenis_tiang','jenis_lampu'))->with(['detailSimpang'=> $detailSimpang,'detailLampu' => $detailLampu, 'detailTiang' => $detailTiang]);
    }

    /**
     * Update the specified detail_simpang in storage.
     *
     * @param  int              $id
     * @param Updatedetail_simpangRequest $request
     *
     * @return Response
     */
    public function update($id, Updatedetail_simpangRequest $request, Updatedetail_tiangRequest $request, Updatedetail_lampuRequest $request)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);
        $input = $request->all();
        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('detailSimpangs.index'));
        }
        if ($request->hasFile('fase_a')) {
           $fase_a = $request->file('fase_a');
        $input['fase_a'] = md5($request->file('fase_a')->getClientOriginalName().time()).'.'.$request->file('fase_a')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_a')->move($destination, $input['fase_a']);    
        // dd($var);
        }
        if ($request->hasFile('fase_b')) {
            $fase_a = $request->file('fase_b');
         $input['fase_b'] = md5($request->file('fase_b')->getClientOriginalName().time()).'.'.$request->file('fase_b')->getClientOriginalExtension();
         $destination = base_path() . '/public/uploads';
         $request->file('fase_b')->move($destination, $input['fase_b']);    
         // dd($var);
         }
         if ($request->hasFile('fase_c')) {
            $fase_a = $request->file('fase_c');
         $input['fase_c'] = md5($request->file('fase_c')->getClientOriginalName().time()).'.'.$request->file('fase_c')->getClientOriginalExtension();
         $destination = base_path() . '/public/uploads';
         $request->file('fase_c')->move($destination, $input['fase_c']);    
         // dd($var);
         }
         if ($request->hasFile('fase_d')) {
            $fase_a = $request->file('fase_d');
         $input['fase_d'] = md5($request->file('fase_d')->getClientOriginalName().time()).'.'.$request->file('fase_d')->getClientOriginalExtension();
         $destination = base_path() . '/public/uploads';
         $request->file('fase_d')->move($destination, $input['fase_d']);    
         // dd($var);
         }
         if ($request->hasFile('fase_e')) {
            $fase_a = $request->file('fase_e');
         $input['fase_e'] = md5($request->file('fase_e')->getClientOriginalName().time()).'.'.$request->file('fase_e')->getClientOriginalExtension();
         $destination = base_path() . '/public/uploads';
         $request->file('fase_e')->move($destination, $input['fase_e']);    
         // dd($var);
         }
         if ($request->hasFile('fase_f')) {
            $fase_a = $request->file('fase_f');
         $input['fase_f'] = md5($request->file('fase_f')->getClientOriginalName().time()).'.'.$request->file('fase_f')->getClientOriginalExtension();
         $destination = base_path() . '/public/uploads';
         $request->file('fase_f')->move($destination, $input['fase_f']);    
         // dd($var);
         }
        $detailSimpang = $this->detailSimpangRepository->update($input, $id);
        if (isset($input['jenis_tiang'])) {
            if(count($input['jenis_tiang'])>0){
               
                foreach ($input['jenis_tiang'] as $key => $value) {
                   
                        
                    //             // dd($detailLampu);
                                $detailTiang =  DB::table('detail_tiang')
                                ->where('id','=',$input['id'][$key])->update([
                                    'nama_lokasi' => $input['nama_lokasi'],
                                    'jenis_tiang' => $input['jenis_tiang'][$key],
                                    'jml_tiang' => $input['jml_tiang'][$key],
                          
                                ]);
            }
            }
            }
            if (isset($input['jenis_lampu'])) {
                if(count($input['jenis_lampu'])>0){
                   
                    foreach ($input['jenis_lampu'] as $key => $value) {
                       
                            
                        //             // dd($detailLampu);
                                    $detailLampu = DB::table('detail_lampu')
                                    ->where('id','=',$input['id'][$key])->update([
                                        'nama_lokasi' => $input['nama_lokasi'],
                                        'jenis_lampu' => $input['jenis_lampu'][$key],
                                        'jml_lampu' => $input['jml_lampu'][$key],
                                        'thn_psng_unit' => $input['thn_psng_unit'][$key],
                              
                                    ]);
                }
                }
                }
        Flash::success('Detail Simpang updated successfully.');

        return redirect(route('detailSimpangs.index'));
    }

    /**
     * Remove the specified detail_simpang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, request $request)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);
        $input = $request->all();
        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('detailSimpangs.index'));
        }

        $this->detailSimpangRepository->delete($id);
        
            $detailTiang =  DB::table('detail_tiang')
            ->where('nama_lokasi','=',$detailSimpang->nama_lokasi)->delete();
                                
            $detailLampu =  DB::table('detail_lampu')
            ->where('nama_lokasi','=',$detailSimpang->nama_lokasi)->delete();

            $plan =  DB::table('plan_simpang')
            ->where('nama_lokasi','=',$detailSimpang->nama_lokasi)->delete();

            $schedule =  DB::table('schedule_simpang')
            ->where('nama_lokasi','=',$detailSimpang->nama_lokasi)->delete();
     
        Flash::success('Detail Simpang deleted successfully.');

        return redirect(route('detailSimpangs.index'));
    }


    public function cetak_pdf($id)
    {
        // $pdf = App::make('dompdf.wrapper');
        // $pdf->loadHTML('<h1>Test</h1>');
        // return $pdf->stream();
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);
        $plan= \App\Models\plan_simpang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->where('nomor_plan','!=','null')->get();
        $schedule= \App\Models\schedule_simpang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->where('nomor_plan','!=','null')->get();
        $detailTiang= \App\Models\detail_tiang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->where('jml_tiang','!=','null')->get();
        $detailLampu= \App\Models\detail_lampu::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->where('jml_lampu','!=','null')->get();
        // dd($schedule);

        $pdf = PDF::loadview('detail_simpangs.showpdf', ['detailSimpang'=> $detailSimpang], ['plan' => $plan,'schedule' => $schedule ,'detailLampu' => $detailLampu, 'detailTiang' => $detailTiang])->setPaper('legal', 'portrait');
        
    	return $pdf->download('laporan-detail-simpang-pdf.pdf');
    }



    //-------------ajax controller plan detail simapng -------------------//
    function fetch_data( Request $request)
    {
        if($request->ajax())
        {

        //     $detailSimpangs = DB::table('detail_simpang')->where('nama_lokasi','=',$id)->pluck('nama_lokasi',$id)->first();
        // dd($detailSimpangs);
            // $detailSimpangs = $this->detailSimpangRepository->findWithoutFail($id);
            // $detailSimpangs = DB::table('detail_simpang')->get();
            $datas = array(
                $request->datas
            );
            $data= \App\Models\plan_simpang::where('nama_lokasi','=',$datas)->get();
         
            // dd($data);   
            // $data=  DB::table('plan_simpang')->where('nama_lokasi','=',$detailSimpangs->nama_lokasi)->get();
            echo json_encode($data);
        }
    }

    function update_data(Request $request)
    {
        if($request->ajax())
        {
            $data = array(
                $request->column_name       =>  $request->column_value
            );
            DB::table('plan_simpang')
                ->where('id', $request->id)
                ->update($data);
            echo '<div class="alert alert-success">Data Updated</div>';
        }
    }

    function delete_data(Request $request)
    {
        if($request->ajax())
        {
            DB::table('plan_simpang')
                ->where('id', $request->id)
                ->delete();
            echo '<div class="alert alert-success">Data Deleted</div>';
        }
    }

     //-------------ajax controller schedule detail simapng -------------------//
     function fetch_data2(Request $request)
     {
        if($request->ajax())
        {
        //     $detailSimpangs = DB::table('detail_simpang')->where('nama_lokasi','=',$id)->pluck('nama_lokasi',$id)->first();
        // dd($detailSimpangs);
            // $detailSimpangs = $this->detailSimpangRepository->findWithoutFail($id);
            $datas = array(
                $request->datas
            );
            $data= \App\Models\schedule_simpang::where('nama_lokasi','=',$datas)->get();
         
            // dd($data);
            // $data=  DB::table('plan_simpang')->where('nama_lokasi','=',$detailSimpangs->nama_lokasi)->get();
            
            echo json_encode($data);
        }
     }
 
     function update_data2(Request $request)
     {
         if($request->ajax())
         {
             $data = array(
                 $request->column_name       =>  $request->column_value
             );
             DB::table('schedule_simpang')
                 ->where('id', $request->id)
                 ->update($data);
             echo '<div class="alert alert-success">Data Updated</div>';
         }
     }
 
     function delete_data2(Request $request)
     {
         if($request->ajax())
         {
             DB::table('schedule_simpang')
                 ->where('id', $request->id)
                 ->delete();
             echo '<div class="alert alert-success">Data Deleted</div>';
         }
     }
}
