<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 
use App\Models\ms_lokasi;
 
use PDF;
 
class lokasiController extends Controller
{
    public function index()
    {
    	return view('lokasi');
    }
 
    public function cetak_pdf()
    {
 
    	$pdf = PDF::loadview('lokasi_pdf');
    	return $pdf->download('laporan-pegawai-pdf');
    }
}
