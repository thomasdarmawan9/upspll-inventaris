<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createdetail_simpangRequest;
use App\Http\Requests\Updatedetail_simpangRequest;
use App\Repositories\detail_simpangRepository;
use App\Repositories\schedule_simpangRepository;
use App\Repositories\plan_simpangRepository;
use App\Repositories\detail_lampuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use PDF;
use App\Models\detail_simpang;
use App\Models\plan_simpang;
use App\Models\schedule_simpang;
use App\Repositories\master_tiangRepository;
use App\Http\Requests\Createdetail_tiangRequest;
use App\Http\Requests\Updatedetail_tiangRequest;
use App\Http\Requests\Createdetail_lampuRequest;
use App\Http\Requests\Updatedetail_lampuRequest;
use App\Repositories\detail_tiangRepository;
class HomeController extends Controller
{
    private $detailSimpangRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(detail_simpangRepository $detailSimpangRepo, detail_lampuRepository $detailLampuRepo, detail_tiangRepository $detailTiangRepo, master_tiangRepository $masterTiangRepo, plan_simpangRepository $planSimpangRepo, schedule_simpangRepository $scheduleSimpangRepo)
    {
        $this->middleware('auth');
        
        $this->detailSimpangRepository = $detailSimpangRepo;
        $this->planSimpangRepository = $planSimpangRepo;
        $this->scheduleSimpangRepository = $scheduleSimpangRepo;
        $this->masterTiangRepository = $masterTiangRepo;
        $this->detailTiangRepository = $detailTiangRepo;
        $this->detailLampuRepository = $detailLampuRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard(Request $request)
    {
        //dashboard controller untuk teknisi//
        $this->detailSimpangRepository->pushCriteria(new RequestCriteria($request));
        $detailSimpangs = $this->detailSimpangRepository->all();

        return view('dashboard')
            ->with('detailSimpangs', $detailSimpangs);
    }


    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$detailSimpangs = DB::table('detail_simpang')
		->where('nama_lokasi','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('dashboard',['detailSimpangs' => $detailSimpangs]);
 
    }
    

    public function detail($id)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);
        $plan= \App\Models\plan_simpang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->get();
        $schedule= \App\Models\schedule_simpang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->get();
        $detailTiang= \App\Models\detail_tiang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->where('jml_tiang','!=','null')->get();
        $detailLampu= \App\Models\detail_lampu::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->where('jml_lampu','!=','null')->get();
        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('dashboard'));
        }
        // dd($plan);
        return view('detail')->with(['detailSimpang'=> $detailSimpang,'plan'=>$plan,'schedule'=>$schedule, 'detailLampu' => $detailLampu, 'detailTiang' => $detailTiang]);
    }


}
