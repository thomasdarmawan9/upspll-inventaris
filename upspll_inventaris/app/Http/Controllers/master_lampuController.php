<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createmaster_lampuRequest;
use App\Http\Requests\Updatemaster_lampuRequest;
use App\Repositories\master_lampuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class master_lampuController extends AppBaseController
{
    /** @var  master_lampuRepository */
    private $masterLampuRepository;

    public function __construct(master_lampuRepository $masterLampuRepo)
    {
        $this->masterLampuRepository = $masterLampuRepo;
    }

    /**
     * Display a listing of the master_lampu.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->masterLampuRepository->pushCriteria(new RequestCriteria($request));
        $masterLampus = $this->masterLampuRepository->all();

        return view('master_lampus.index')
            ->with('masterLampus', $masterLampus);
    }

    /**
     * Show the form for creating a new master_lampu.
     *
     * @return Response
     */
    public function create()
    {
        return view('master_lampus.create');
    }

    /**
     * Store a newly created master_lampu in storage.
     *
     * @param Createmaster_lampuRequest $request
     *
     * @return Response
     */
    public function store(Createmaster_lampuRequest $request)
    {
        $input = $request->all();

        $masterLampu = $this->masterLampuRepository->create($input);

        Flash::success('Master Lampu saved successfully.');

        return redirect(route('masterLampus.index'));
    }

    /**
     * Display the specified master_lampu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $masterLampu = $this->masterLampuRepository->findWithoutFail($id);

        if (empty($masterLampu)) {
            Flash::error('Master Lampu not found');

            return redirect(route('masterLampus.index'));
        }

        return view('master_lampus.show')->with('masterLampu', $masterLampu);
    }

    /**
     * Show the form for editing the specified master_lampu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $masterLampu = $this->masterLampuRepository->findWithoutFail($id);

        if (empty($masterLampu)) {
            Flash::error('Master Lampu not found');

            return redirect(route('masterLampus.index'));
        }

        return view('master_lampus.edit')->with('masterLampu', $masterLampu);
    }

    /**
     * Update the specified master_lampu in storage.
     *
     * @param  int              $id
     * @param Updatemaster_lampuRequest $request
     *
     * @return Response
     */
    public function update($id, Updatemaster_lampuRequest $request)
    {
        $masterLampu = $this->masterLampuRepository->findWithoutFail($id);

        if (empty($masterLampu)) {
            Flash::error('Master Lampu not found');

            return redirect(route('masterLampus.index'));
        }

        $masterLampu = $this->masterLampuRepository->update($request->all(), $id);

        Flash::success('Master Lampu updated successfully.');

        return redirect(route('masterLampus.index'));
    }

    /**
     * Remove the specified master_lampu from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $masterLampu = $this->masterLampuRepository->findWithoutFail($id);

        if (empty($masterLampu)) {
            Flash::error('Master Lampu not found');

            return redirect(route('masterLampus.index'));
        }

        $this->masterLampuRepository->delete($id);

        Flash::success('Master Lampu deleted successfully.');

        return redirect(route('masterLampus.index'));
    }
}
