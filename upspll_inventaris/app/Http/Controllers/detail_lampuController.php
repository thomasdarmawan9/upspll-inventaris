<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createdetail_lampuRequest;
use App\Http\Requests\Updatedetail_lampuRequest;
use App\Repositories\detail_lampuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class detail_lampuController extends AppBaseController
{
    /** @var  detail_lampuRepository */
    private $detailLampuRepository;

    public function __construct(detail_lampuRepository $detailLampuRepo)
    {
        $this->detailLampuRepository = $detailLampuRepo;
    }

    /**
     * Display a listing of the detail_lampu.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->detailLampuRepository->pushCriteria(new RequestCriteria($request));
        $detailLampus = $this->detailLampuRepository->all();

        return view('detail_lampus.index')
            ->with('detailLampus', $detailLampus);
    }

    /**
     * Show the form for creating a new detail_lampu.
     *
     * @return Response
     */
    public function create()
    {
        return view('detail_lampus.create');
    }

    /**
     * Store a newly created detail_lampu in storage.
     *
     * @param Createdetail_lampuRequest $request
     *
     * @return Response
     */
    public function store(Createdetail_lampuRequest $request)
    {
        $input = $request->all();

        $detailLampu = $this->detailLampuRepository->create($input);

        Flash::success('Detail Lampu saved successfully.');

        return redirect(route('detailLampus.index'));
    }

    /**
     * Display the specified detail_lampu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detailLampu = $this->detailLampuRepository->findWithoutFail($id);

        if (empty($detailLampu)) {
            Flash::error('Detail Lampu not found');

            return redirect(route('detailLampus.index'));
        }

        return view('detail_lampus.show')->with('detailLampu', $detailLampu);
    }

    /**
     * Show the form for editing the specified detail_lampu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detailLampu = $this->detailLampuRepository->findWithoutFail($id);

        if (empty($detailLampu)) {
            Flash::error('Detail Lampu not found');

            return redirect(route('detailLampus.index'));
        }

        return view('detail_lampus.edit')->with('detailLampu', $detailLampu);
    }

    /**
     * Update the specified detail_lampu in storage.
     *
     * @param  int              $id
     * @param Updatedetail_lampuRequest $request
     *
     * @return Response
     */
    public function update($id, Updatedetail_lampuRequest $request)
    {
        $detailLampu = $this->detailLampuRepository->findWithoutFail($id);

        if (empty($detailLampu)) {
            Flash::error('Detail Lampu not found');

            return redirect(route('detailLampus.index'));
        }

        $detailLampu = $this->detailLampuRepository->update($request->all(), $id);

        Flash::success('Detail Lampu updated successfully.');

        return redirect(route('detailLampus.index'));
    }

    /**
     * Remove the specified detail_lampu from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detailLampu = $this->detailLampuRepository->findWithoutFail($id);

        if (empty($detailLampu)) {
            Flash::error('Detail Lampu not found');

            return redirect(route('detailLampus.index'));
        }

        $this->detailLampuRepository->delete($id);

        Flash::success('Detail Lampu deleted successfully.');

        return redirect(route('detailLampus.index'));
    }
}
