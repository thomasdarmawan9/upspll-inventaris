<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class detail_tiang
 * @package App\Models
 * @version January 29, 2020, 7:07 am UTC
 *
 * @property string nama_lokasi
 * @property string jenis_tiang
 * @property integer jml_tiang
 */
class detail_tiang extends Model
{
    use SoftDeletes;

    public $table = 'detail_tiang';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_lokasi',
        'jenis_tiang',
        'jml_tiang'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_lokasi' => 'string',
        'jenis_tiang' => 'string',
        'jml_tiang' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
