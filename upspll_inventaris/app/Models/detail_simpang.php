<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class detail_simpang
 * @package App\Models
 * @version January 10, 2020, 2:43 am UTC
 *
 * @property integer id_lokasi_fk
 * @property integer lat
 * @property integer long
 * @property string tiang
 * @property string unit_lampu
 * @property string controller
 * @property string thn_psng_controller
 * @property string thn_psng_unit
 * @property string controller_pc
 * @property string fase_a
 * @property string fase_b
 * @property string fase_c
 * @property string fase_d
 * @property string fase_e
 * @property string fase_f
 */
class detail_simpang extends Model
{
    use SoftDeletes;

    public $table = 'detail_simpang';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nama_lokasi',
        'alamat',
        'lat',
        'long',
        'controller',
        'thn_psng_controller',
        'controller_pc',
        'fase_a',
        'fase_b',
        'fase_c',
        'fase_d',
        'fase_e',
        'fase_f',
        'gambar_simpang'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_lokasi' => 'string',
        'alamat' => 'string',
        'lat' => 'string',
        'long' => 'string',
        'controller' => 'string',
        'thn_psng_controller' => 'string',
        'controller_pc' => 'string',
        'fase_a' => 'string',
        'fase_b' => 'string',
        'fase_c' => 'string',
        'fase_d' => 'string',
        'fase_e' => 'string',
        'fase_f' => 'string',
        'gambar_simpang' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_lokasi' => 'required',
        'alamat' => 'nullable',
        'lat' => 'required',
        'long' => 'required',
        'controller' => 'nullable',
        'thn_psng_controller' => 'required',
        'controller_pc' => 'nullable',
        'fase_a' => 'nullable',
        'fase_b' => 'nullable',
        'fase_c' => 'nullable',
        'fase_d' => 'nullable',
        'fase_e' => 'nullable',
        'fase_f' => 'nullable',
        'gambar_simpang' => 'nullable'
    ];

    public function plan_simpang(){
        return $this->hasMany('\App\Models\plan_simpang','nama_lokasi');
    }
    public function schedule_simpang(){
        return $this->hasMany('\App\Models\schedule_simpang','nama_lokasi');
    }
}
