<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class detail_lampu
 * @package App\Models
 * @version January 30, 2020, 6:25 am UTC
 *
 * @property string nama_lokasi
 * @property string jenis_lampu
 * @property integer jml_lampu
 * @property string thn_psng_unit
 */
class detail_lampu extends Model
{
    use SoftDeletes;

    public $table = 'detail_lampu';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_lokasi',
        'jenis_lampu',
        'jml_lampu',
        'thn_psng_unit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_lokasi' => 'string',
        'jenis_lampu' => 'string',
        'jml_lampu' => 'integer',
        'thn_psng_unit' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
