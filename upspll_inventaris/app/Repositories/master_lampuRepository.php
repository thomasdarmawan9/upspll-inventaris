<?php

namespace App\Repositories;

use App\Models\master_lampu;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class master_lampuRepository
 * @package App\Repositories
 * @version January 29, 2020, 2:33 am UTC
 *
 * @method master_lampu findWithoutFail($id, $columns = ['*'])
 * @method master_lampu find($id, $columns = ['*'])
 * @method master_lampu first($columns = ['*'])
*/
class master_lampuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jenis_lampu'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return master_lampu::class;
    }
}
