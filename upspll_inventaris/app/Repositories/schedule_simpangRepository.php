<?php

namespace App\Repositories;

use App\Models\schedule_simpang;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class schedule_simpangRepository
 * @package App\Repositories
 * @version January 16, 2020, 2:19 am UTC
 *
 * @method schedule_simpang findWithoutFail($id, $columns = ['*'])
 * @method schedule_simpang find($id, $columns = ['*'])
 * @method schedule_simpang first($columns = ['*'])
*/
class schedule_simpangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_lokasi',
        'nomor_plan',
        'ct',
        'time',
        'days'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return schedule_simpang::class;
    }
}
