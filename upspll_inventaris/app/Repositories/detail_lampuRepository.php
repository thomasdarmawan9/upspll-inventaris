<?php

namespace App\Repositories;

use App\Models\detail_lampu;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class detail_lampuRepository
 * @package App\Repositories
 * @version January 30, 2020, 6:25 am UTC
 *
 * @method detail_lampu findWithoutFail($id, $columns = ['*'])
 * @method detail_lampu find($id, $columns = ['*'])
 * @method detail_lampu first($columns = ['*'])
*/
class detail_lampuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_lokasi',
        'jenis_lampu',
        'jml_lampu',
        'thn_psng_unit'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return detail_lampu::class;
    }
}
