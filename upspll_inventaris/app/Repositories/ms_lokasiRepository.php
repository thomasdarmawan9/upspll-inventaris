<?php

namespace App\Repositories;

use App\Models\ms_lokasi;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ms_lokasiRepository
 * @package App\Repositories
 * @version January 15, 2020, 12:32 pm UTC
 *
 * @method ms_lokasi findWithoutFail($id, $columns = ['*'])
 * @method ms_lokasi find($id, $columns = ['*'])
 * @method ms_lokasi first($columns = ['*'])
*/
class ms_lokasiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nomor_lokasi',
        'nama_lokasi',
        'kategori'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ms_lokasi::class;
    }
}
