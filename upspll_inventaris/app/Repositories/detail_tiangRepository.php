<?php

namespace App\Repositories;

use App\Models\detail_tiang;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class detail_tiangRepository
 * @package App\Repositories
 * @version January 29, 2020, 7:07 am UTC
 *
 * @method detail_tiang findWithoutFail($id, $columns = ['*'])
 * @method detail_tiang find($id, $columns = ['*'])
 * @method detail_tiang first($columns = ['*'])
*/
class detail_tiangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_lokasi',
        'jenis_tiang',
        'jml_tiang'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return detail_tiang::class;
    }
}
