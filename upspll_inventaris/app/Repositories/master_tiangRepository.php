<?php

namespace App\Repositories;

use App\Models\master_tiang;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class master_tiangRepository
 * @package App\Repositories
 * @version January 29, 2020, 1:58 am UTC
 *
 * @method master_tiang findWithoutFail($id, $columns = ['*'])
 * @method master_tiang find($id, $columns = ['*'])
 * @method master_tiang first($columns = ['*'])
*/
class master_tiangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jenis_tiang'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return master_tiang::class;
    }
}
