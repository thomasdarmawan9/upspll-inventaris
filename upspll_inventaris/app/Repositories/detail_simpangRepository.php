<?php

namespace App\Repositories;

use App\Models\detail_simpang;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class detail_simpangRepository
 * @package App\Repositories
 * @version January 15, 2020, 12:29 pm UTC
 *
 * @method detail_simpang findWithoutFail($id, $columns = ['*'])
 * @method detail_simpang find($id, $columns = ['*'])
 * @method detail_simpang first($columns = ['*'])
*/
class detail_simpangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_lokasi',
        'alamat',
        'lat',
        'long',
        'controller',
        'thn_psng_controller',
        'controller_pc',
        'fase_a',
        'fase_b',
        'fase_c',
        'fase_d',
        'fase_e',
        'fase_f',
        'gambar_simpang'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return detail_simpang::class;
    }
}
