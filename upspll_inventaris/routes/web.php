<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/', 'ms_lokasiController@index')->middleware('admin');
    
Route::get('/home', 'ms_lokasiController@index')->middleware('admin');
Route::get('/maps', 'ms_lokasiController@maps')->middleware('admin');
// Route::get('/lokasi', 'lokasiController@index');
// Route::get('/lokasi/cetak_pdf', 'lokasiController@cetak_pdf');

Route::get('upspll_inventaris/detailSimpang/cetak_pdf/{id}', 'detail_simpangController@cetak_pdf')->middleware('admin');

Route::get('upspll_inventaris/ms_lokasi/cetak_pdf_lokasiatcs', 'ms_lokasiController@cetak_pdf_lokasiatcs')->middleware('admin');

Route::get('upspll_inventaris/ms_lokasi/cetak_pdf_lokasinonatcs', 'ms_lokasiController@cetak_pdf_lokasinonatcs')->middleware('admin');

Route::resource('msLokasis', 'ms_lokasiController', ['middleware' => ['admin']]);

Route::resource('detailSimpangs', 'detail_simpangController', ['middleware' => ['admin']]);

Route::resource('planSimpangs', 'plan_simpangController', ['middleware' => ['admin']]);

Route::get('upspll_inventaris/plansimpangadd/{id}', 'plan_simpangController@addsimpang')->middleware('admin');

Route::resource('scheduleSimpangs', 'schedule_simpangController', ['middleware' => ['admin']]);

Route::get('upspll_inventaris/schedulesimpangadd/{id}', 'schedule_simpangController@addsimpang')->middleware('admin');

Route::get('upspll_inventaris/detail_simpangController/fetch_data', 'detail_simpangController@fetch_data')->middleware('admin');

Route::post('/detail_simpangController/update_data', 'detail_simpangController@update_data')->name('detail_simpangController.update_data');
Route::post('/detail_simpangController/delete_data', 'detail_simpangController@delete_data')->name('detail_simpangController.delete_data');


Route::get('upspll_inventaris/detail_simpangController/fetch_data2', 'detail_simpangController@fetch_data2')->middleware('admin');

Route::post('/detail_simpangController/update_data2', 'detail_simpangController@update_data2')->name('detail_simpangController.update_data2');
Route::post('/detail_simpangController/delete_data2', 'detail_simpangController@delete_data2')->name('detail_simpangController.delete_data2');



Route::resource('masterTiangs', 'master_tiangController', ['middleware' => ['admin']]);
Route::resource('masterLampus', 'master_lampuController', ['middleware' => ['admin']]);

Route::resource('detailTiangs', 'detail_tiangController', ['middleware' => ['admin']]);

Route::resource('detailLampus', 'detail_lampuController', ['middleware' => ['admin']]);


Route::get('/', 'HomeController@dashboard')->middleware('teknisi');


Route::get('/detail/{id}', 'HomeController@detail')->middleware('teknisi');
Route::get('/detailSimpang/cari','HomeController@cari')->middleware('teknisi');