-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Jan 2020 pada 06.26
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `up_spll_inventaris`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_simpang`
--

CREATE TABLE `detail_simpang` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_lokasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` int(11) NOT NULL,
  `long` int(11) NOT NULL,
  `tiang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_lampu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thn_psng_controller` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thn_psng_unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `controller_pc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fase_a` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fase_b` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fase_c` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fase_d` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fase_e` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fase_f` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar_simpang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `detail_simpang`
--

INSERT INTO `detail_simpang` (`id`, `nama_lokasi`, `lat`, `long`, `tiang`, `unit_lampu`, `controller`, `thn_psng_controller`, `thn_psng_unit`, `controller_pc`, `fase_a`, `fase_b`, `fase_c`, `fase_d`, `fase_e`, `fase_f`, `gambar_simpang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cawang Kodam (control Newtel)', 12, 21, 'tiang lurus 6 meter bulat', '2 X 30 cm (K)', 'UMC DC', '2002', '2002', 'Newtrap', '3ea6bcb2f918c34ae801a0cfa900057d.JPG', NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-13 22:23:38', '2020-01-13 22:23:38', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2020_01_10_003804_create_lokasis_table', 1),
(65, '2014_10_12_000000_create_users_table', 2),
(66, '2014_10_12_100000_create_password_resets_table', 2),
(67, '2020_01_10_022104_create_detail_simpang_table', 2),
(68, '2020_01_12_070743_plan_simpang', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_lokasi`
--

CREATE TABLE `ms_lokasi` (
  `id` int(12) NOT NULL,
  `nomor_lokasi` varchar(20) NOT NULL,
  `nama_lokasi` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_lokasi`
--

INSERT INTO `ms_lokasi` (`id`, `nomor_lokasi`, `nama_lokasi`, `created_at`, `updated_at`) VALUES
(1, '717', 'Cawang Kodam (control Newtel)', '2020-01-10 01:21:26', '2020-01-09 18:25:53'),
(2, '718', 'Garuda Tmii', '2020-01-10 01:21:26', NULL),
(3, '719', 'Toll Halim', '2020-01-10 01:21:26', NULL),
(4, '815', 'Exit Toll Ciledug', '2020-01-10 01:21:26', NULL),
(5, '114', 'O T E P A', '2020-01-10 01:21:26', NULL),
(6, '201', 'A S E M K A', '2020-01-10 01:21:26', NULL),
(7, '203', 'Mangga Besar', '2020-01-10 01:21:26', NULL),
(8, '204', 'Sawah Besar', '2020-01-10 01:21:26', NULL),
(9, '205', 'H A R M O N I', '2020-01-10 01:21:26', NULL),
(10, '313', 'Patung Kuda', '2020-01-10 01:21:26', NULL),
(11, '314', 'Kebon Sirih', '2020-01-10 01:21:26', NULL),
(12, '315', 'S A R I N A H', '2020-01-10 01:21:26', NULL),
(13, '318', 'Sabang - Wahid H', '2020-01-10 01:21:26', NULL),
(14, '405', 'Bunderan H I', '2020-01-10 01:21:26', NULL),
(15, '602', 'Patung Obor', '2020-01-10 01:21:26', NULL),
(16, '604', 'Al Azhar', '2020-01-10 01:21:26', NULL),
(17, '605', 'C S W', '2020-01-10 01:21:26', NULL),
(18, '120', 'Batu', '2020-01-10 01:21:26', NULL),
(19, '807', 'Vittoria', '2020-01-10 01:21:26', NULL),
(20, '213', 'Holland Bakery', '2020-01-10 01:21:26', NULL),
(21, '214', 'Sangaji', '2020-01-10 01:21:26', NULL),
(22, '501', 'Pasar Senen', '2020-01-10 01:21:26', NULL),
(23, '504', 'Galur', '2020-01-10 01:21:26', NULL),
(24, '506', 'Tugu Tani', '2020-01-10 01:21:26', NULL),
(25, '507', 'Kwitang', '2020-01-10 01:21:26', NULL),
(26, '701', 'Kelapa Gading', '2020-01-10 01:21:26', NULL),
(27, '702', 'Coca Cola', '2020-01-10 01:21:26', NULL),
(28, '801', 'Grogol', '2020-01-10 01:21:26', NULL),
(29, '802', 'Pasar Cengkareng', '2020-01-10 01:21:26', NULL),
(30, '803', 'Satelindo', '2020-01-10 01:21:26', NULL),
(31, '420', 'Halimun', '2020-01-10 01:21:26', NULL),
(32, '421', 'Guntur', '2020-01-10 01:21:26', NULL),
(33, '422', 'Manggarai', '2020-01-10 01:21:26', NULL),
(34, '423', 'Dukuh Bawah', '2020-01-10 01:21:26', NULL),
(35, '513', 'Matraman', '2020-01-10 01:21:26', NULL),
(36, '515', 'Tambak', '2020-01-10 01:21:26', NULL),
(37, '703', 'Pemuda Pramuka', '2020-01-10 01:21:26', NULL),
(38, '704', 'Sunan Giri', '2020-01-10 01:21:26', NULL),
(39, '705', 'Balai Pustaka', '2020-01-10 01:21:26', NULL),
(40, '706', 'Arion', '2020-01-10 01:21:26', NULL),
(41, '707', 'Tu Gas', '2020-01-10 01:21:26', NULL),
(42, '629', 'Rs Fatmawati', '2020-01-10 01:21:26', NULL),
(43, '101', 'Jembatan Merah', '2020-01-10 01:21:26', NULL),
(44, '102', 'Kartini', '2020-01-10 01:21:26', NULL),
(45, '105', 'Pintu Besi', '2020-01-10 01:21:26', NULL),
(46, '403', 'Taman Menteng', '2020-01-10 01:21:26', NULL),
(47, '108', 'Mbal', '2020-01-10 01:21:26', NULL),
(48, '109', 'Garuda', '2020-01-10 01:21:26', NULL),
(49, '110', 'Wahidin', '2020-01-10 01:21:26', NULL),
(50, '111', 'Gki Bina Mental', '2020-01-10 01:21:26', NULL),
(51, '121', 'Ancol', '2020-01-10 01:21:26', NULL),
(52, '122', 'Mangga Dua', '2020-01-10 01:21:26', NULL),
(53, '508', 'Cikini', '2020-01-10 01:21:26', NULL),
(54, '509', 'Rivoli', '2020-01-10 01:21:26', NULL),
(55, '510', 'Raden Saleh', '2020-01-10 01:21:26', NULL),
(56, '511', 'Paseban', '2020-01-10 01:21:26', NULL),
(57, '512', 'Salemba', '2020-01-10 01:21:26', NULL),
(58, '514', 'Slamet Riyadi', '2020-01-10 01:21:26', NULL),
(59, '404', 'Imam Bonjol Cokro', '2020-01-10 01:21:26', NULL),
(60, '407', 'Pamekasan', '2020-01-10 01:21:26', NULL),
(61, '408', 'Teuku Umar', '2020-01-10 01:21:26', NULL),
(62, '409', 'Rs Bunda', '2020-01-10 01:21:26', NULL),
(63, '412', 'Taman Suropati', '2020-01-10 01:21:26', NULL),
(64, '413', 'Cik Ditiro', '2020-01-10 01:21:26', NULL),
(65, '415', 'Surabaya', '2020-01-10 01:21:26', NULL),
(66, '416', 'Megaria', '2020-01-10 01:21:26', NULL),
(67, '301', 'Biak Caringin', '2020-01-10 01:21:26', NULL),
(68, '302', 'Rs Tarakan', '2020-01-10 01:21:26', NULL),
(69, '304', 'Tanah Abang 2', '2020-01-10 01:21:26', NULL),
(70, '309', 'Budi Kemuliaan', '2020-01-10 01:21:26', NULL),
(71, '310', 'Jembatan Serong', '2020-01-10 01:21:26', NULL),
(72, '311', 'Jatibaru', '2020-01-10 01:21:26', NULL),
(73, '317', 'Sabang', '2020-01-10 01:21:26', NULL),
(74, '804', 'Pakin', '2020-01-10 01:21:26', NULL),
(75, '805', 'Tol Gedong Panjang', '2020-01-10 01:21:26', NULL),
(76, '806', 'Pluit Junction', '2020-01-10 01:21:26', NULL),
(77, '601', 'Pondok Indah', '2020-01-10 01:21:26', NULL),
(78, '631', 'Kuningan', '2020-01-10 01:21:26', NULL),
(79, '633', 'Mampang', '2020-01-10 01:21:26', NULL),
(80, '607', 'Mabak', '2020-01-10 01:21:26', NULL),
(81, '810', 'Tomang', '2020-01-10 01:21:26', NULL),
(82, '811', 'Slipi', '2020-01-10 01:21:26', NULL),
(83, '630', 'Dep. Pertanian', '2020-01-10 01:21:26', NULL),
(84, '425', 'Karet Bivak', '2020-01-10 01:21:26', NULL),
(85, '808', 'Jembatan Tiga', '2020-01-10 01:21:26', NULL),
(86, '809', 'Jembatan Dua', '2020-01-10 01:21:26', NULL),
(87, '628', 'Cilandak Kko', '2020-01-10 01:21:26', NULL),
(88, '708', 'Stasiun Jatinegara', '2020-01-10 01:21:26', NULL),
(89, '709', 'Kalimalang - Trikora', '2020-01-10 01:21:26', NULL),
(90, '622', 'Pintu Satu Senayan', '2020-01-10 01:21:26', NULL),
(91, '623', 'Gerbang Pemuda', '2020-01-10 01:21:26', NULL),
(92, '710', 'Mac D Buaran', '2020-01-10 01:21:26', NULL),
(93, '634', 'Pejaten Raya', '2020-01-10 01:21:26', NULL),
(94, '635', 'Ragunan', '2020-01-10 01:21:26', NULL),
(95, '901', 'P G C - Cililitan', '2020-01-10 01:21:26', NULL),
(96, '130', 'Marunda Kebantenan', '2020-01-10 01:21:26', NULL),
(97, '614', 'Melawai', '2020-01-10 01:21:26', NULL),
(98, '615', 'Wijaya 2', '2020-01-10 01:21:26', NULL),
(99, '616', 'Kemang Prapanca', '2020-01-10 01:21:26', NULL),
(100, '617', 'Brawijaya Antasari', '2020-01-10 01:21:26', NULL),
(101, '619', 'Abdul Majid Antasari', '2020-01-10 01:21:26', NULL),
(102, '620', 'Manunggal Juang', '2020-01-10 01:21:26', NULL),
(103, '625', 'Polim Melawai', '2020-01-10 01:21:26', NULL),
(104, '711', 'J G C', '2020-01-10 01:21:26', NULL),
(105, '632', 'Pancoran', '2020-01-10 01:21:26', NULL),
(106, '636', 'Mampang 7 / 8', '2020-01-10 01:21:26', NULL),
(107, '637', 'Basmar', '2020-01-10 01:21:26', NULL),
(108, '638', 'Duren 3 Selatan', '2020-01-10 01:21:26', NULL),
(109, '712', 'Buaran Bioskop', '2020-01-10 01:21:26', NULL),
(110, '713', 'Klender', '2020-01-10 01:21:26', NULL),
(111, '900', 'Pasar Rebo', '2020-01-10 01:21:26', NULL),
(112, '902', 'Jambul', '2020-01-10 01:21:26', NULL),
(113, '903', 'Tmp Kalibata', '2020-01-10 01:21:26', NULL),
(114, '904', 'Duren Tiga', '2020-01-10 01:21:26', NULL),
(115, '905', 'Perdatam', '2020-01-10 01:21:26', NULL),
(116, '410', 'Pamekasan', '2020-01-10 01:21:26', NULL),
(117, '640', 'Antasari Cipete', '2020-01-10 01:21:26', NULL),
(118, '116', 'Veteran 3 - Juanda', '2020-01-10 01:21:26', NULL),
(119, '307', 'Tanah Abang 2', '2020-01-10 01:21:26', NULL),
(120, '319', 'Hotel Cemara', '2020-01-10 01:21:26', NULL),
(121, '606', 'Bulungan', '2020-01-10 01:21:26', NULL),
(122, '611', 'Rs Pertamina', '2020-01-10 01:21:26', NULL),
(123, '612', 'Mayestik', '2020-01-10 01:21:26', NULL),
(124, '624', 'Radio Dalam', '2020-01-10 01:21:26', NULL),
(125, '609', 'Stm Penerbangan', '2020-01-10 01:21:26', NULL),
(126, '610', 'Gereja Santa', '2020-01-10 01:21:26', NULL),
(127, '642', 'Polim Barito 2', '2020-01-10 01:21:26', NULL),
(128, '643', 'Wijaya 2', '2020-01-10 01:21:26', NULL),
(129, '644', 'Haji Nawi', '2020-01-10 01:21:26', NULL),
(130, '645', 'Fatmawati Cipete', '2020-01-10 01:21:26', NULL),
(131, '639', 'Pondok Pinang', '2020-01-10 01:21:26', NULL),
(132, '641', 'Kartini - Simatupang', '2020-01-10 01:21:26', NULL),
(133, '646', 'Sekolah Duta', '2020-01-10 01:21:26', NULL),
(134, '647', 'Lebak Bulus', '2020-01-10 01:21:26', NULL),
(135, '648', 'Deplu', '2020-01-10 01:21:26', NULL),
(136, '816', 'Tubagus Angke', '2020-01-10 01:21:26', NULL),
(137, '817', 'Peta Selatan', '2020-01-10 01:21:26', NULL),
(138, '818', 'Panjang', '2020-01-10 01:21:26', NULL),
(139, '819', 'Terminal Kalideres', '2020-01-10 01:21:26', NULL),
(140, '820', 'Tampak Siring', '2020-01-10 01:21:26', NULL),
(141, '127', 'Permai Pemadam', '2020-01-10 01:21:26', NULL),
(142, '128', 'Sulawesi', '2020-01-10 01:21:26', NULL),
(143, '129', 'Hibrida', '2020-01-10 01:21:26', NULL),
(144, '714', 'Cawang Uki', '2020-01-10 01:21:26', NULL),
(145, '715', 'Haji Ten', '2020-01-10 01:21:26', NULL),
(146, '716', 'Golf Rawamangun', '2020-01-10 01:21:26', NULL),
(147, '720', 'Pulo Gebang', '2020-01-10 01:21:26', NULL),
(148, '812', 'Pos Pengumben', '2020-01-10 01:21:26', NULL),
(149, '813', 'Seskoal', '2020-01-10 01:21:26', NULL),
(150, '814', 'Swadarma', '2020-01-10 01:21:26', NULL),
(151, '821', 'Kebayoran Lama', '2020-01-10 01:21:26', NULL),
(152, '822', 'Kembangan Raya', '2020-01-10 01:21:26', NULL),
(153, '907', 'Cijantung', '2020-01-10 01:21:26', NULL),
(154, '908', 'Ciracas', '2020-01-10 01:21:26', NULL),
(155, '909', 'Cibubur', '2020-01-10 01:21:26', NULL),
(156, '910', 'Pondol Gede', '2020-01-10 01:21:26', NULL),
(157, '911', 'Bulak Rantai', '2020-01-10 01:21:26', NULL),
(158, '912', 'Jatiwaringin', '2020-01-10 01:21:26', NULL),
(159, '913', 'H Naman', '2020-01-10 01:21:26', NULL),
(160, '914', 'Ceger', '2020-01-10 01:21:26', NULL),
(161, '426', 'Pejompongan Spbu', '2020-01-10 01:21:26', NULL),
(162, '1000', 'mangga besar 8', '2020-01-10 01:21:26', NULL),
(163, '1001', 'gereja sion', '2020-01-10 01:21:26', NULL),
(164, '123123', 'disini', '2020-01-09 18:25:26', '2020-01-09 18:25:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `plan_simpang`
--

CREATE TABLE `plan_simpang` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_lokasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_plan` int(11) NOT NULL,
  `cycle_time` int(11) DEFAULT NULL,
  `offset` int(11) DEFAULT NULL,
  `a` int(11) DEFAULT NULL,
  `b` int(11) DEFAULT NULL,
  `c` int(11) DEFAULT NULL,
  `d` int(11) DEFAULT NULL,
  `e` int(11) DEFAULT NULL,
  `f` int(11) DEFAULT NULL,
  `r_minus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_plus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `y_minus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `y_plus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `z_minus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `z_plus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `q_minus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `q_plus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `schedule_simpang`
--

CREATE TABLE `schedule_simpang` (
  `id` int(11) NOT NULL,
  `id_simpang_fk` int(12) NOT NULL,
  `plan` int(12) NOT NULL,
  `ct` int(12) NOT NULL,
  `xsf` int(12) NOT NULL,
  `time` int(12) NOT NULL,
  `days` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$H.eP2xVAE1bh5i3Z7hxDD.VO46j7o1ku27pBT/J9MTIBnbkUs2RYO', 'Admin', NULL, '2020-01-13 22:23:20', '2020-01-13 22:23:20');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_simpang`
--
ALTER TABLE `detail_simpang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_lokasi`
--
ALTER TABLE `ms_lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `plan_simpang`
--
ALTER TABLE `plan_simpang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `schedule_simpang`
--
ALTER TABLE `schedule_simpang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_simpang`
--
ALTER TABLE `detail_simpang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT untuk tabel `ms_lokasi`
--
ALTER TABLE `ms_lokasi`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT untuk tabel `plan_simpang`
--
ALTER TABLE `plan_simpang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `schedule_simpang`
--
ALTER TABLE `schedule_simpang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
