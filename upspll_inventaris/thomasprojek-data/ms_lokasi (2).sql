-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10 Feb 2020 pada 05.11
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `up_spll_inventaris`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_lokasi`
--

CREATE TABLE `ms_lokasi` (
  `id` int(12) NOT NULL,
  `nomor_lokasi` varchar(20) DEFAULT NULL,
  `nama_lokasi` varchar(50) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_lokasi`
--

INSERT INTO `ms_lokasi` (`id`, `nomor_lokasi`, `nama_lokasi`, `kategori`, `created_at`, `updated_at`) VALUES
(1, '717', 'Cawang Kodam (control Newtel)', 'ATCS', '2020-01-10 01:21:26', '2020-01-09 18:25:53'),
(2, '718', 'Garuda Tmii', 'ATCS', '2020-01-10 01:21:26', NULL),
(3, '719', 'Toll Halim', 'ATCS', '2020-01-10 01:21:26', NULL),
(4, '815', 'Exit Toll Ciledug', 'ATCS', '2020-01-10 01:21:26', NULL),
(5, '114', 'O T E P A', 'ATCS', '2020-01-10 01:21:26', NULL),
(6, '201', 'A S E M K A', 'ATCS', '2020-01-10 01:21:26', NULL),
(7, '203', 'Mangga Besar', 'ATCS', '2020-01-10 01:21:26', NULL),
(8, '204', 'Sawah Besar', 'ATCS', '2020-01-10 01:21:26', NULL),
(9, '205', 'H A R M O N I', 'ATCS', '2020-01-10 01:21:26', NULL),
(10, '313', 'Patung Kuda', 'ATCS', '2020-01-10 01:21:26', NULL),
(11, '314', 'Kebon Sirih', 'ATCS', '2020-01-10 01:21:26', NULL),
(12, '315', 'S A R I N A H', 'ATCS', '2020-01-10 01:21:26', NULL),
(13, '318', 'Sabang - Wahid H', 'ATCS', '2020-01-10 01:21:26', NULL),
(14, '405', 'Bunderan H I', 'ATCS', '2020-01-10 01:21:26', NULL),
(15, '602', 'Patung Obor', 'ATCS', '2020-01-10 01:21:26', NULL),
(16, '604', 'Al Azhar', 'ATCS', '2020-01-10 01:21:26', NULL),
(17, '605', 'C S W', 'ATCS', '2020-01-10 01:21:26', NULL),
(18, '120', 'Batu', 'ATCS', '2020-01-10 01:21:26', NULL),
(19, '807', 'Vittoria', 'ATCS', '2020-01-10 01:21:26', NULL),
(20, '213', 'Holland Bakery', 'ATCS', '2020-01-10 01:21:26', NULL),
(21, '214', 'Sangaji', 'ATCS', '2020-01-10 01:21:26', NULL),
(22, '501', 'Pasar Senen', 'ATCS', '2020-01-10 01:21:26', NULL),
(23, '504', 'Galur', 'ATCS', '2020-01-10 01:21:26', NULL),
(24, '506', 'Tugu Tani', 'ATCS', '2020-01-10 01:21:26', NULL),
(25, '507', 'Kwitang', 'ATCS', '2020-01-10 01:21:26', NULL),
(26, '701', 'Kelapa Gading', 'ATCS', '2020-01-10 01:21:26', NULL),
(27, '702', 'Coca Cola', 'ATCS', '2020-01-10 01:21:26', NULL),
(28, '801', 'Grogol', 'ATCS', '2020-01-10 01:21:26', NULL),
(29, '802', 'Pasar Cengkareng', 'ATCS', '2020-01-10 01:21:26', NULL),
(30, '803', 'Satelindo', 'ATCS', '2020-01-10 01:21:26', NULL),
(31, '420', 'Halimun', 'ATCS', '2020-01-10 01:21:26', NULL),
(32, '421', 'Guntur', 'ATCS', '2020-01-10 01:21:26', NULL),
(33, '422', 'Manggarai', 'ATCS', '2020-01-10 01:21:26', NULL),
(34, '423', 'Dukuh Bawah', 'ATCS', '2020-01-10 01:21:26', NULL),
(35, '513', 'Matraman', 'ATCS', '2020-01-10 01:21:26', NULL),
(36, '515', 'Tambak', 'ATCS', '2020-01-10 01:21:26', NULL),
(37, '703', 'Pemuda Pramuka', 'ATCS', '2020-01-10 01:21:26', NULL),
(38, '704', 'Sunan Giri', 'ATCS', '2020-01-10 01:21:26', NULL),
(39, '705', 'Balai Pustaka', 'ATCS', '2020-01-10 01:21:26', NULL),
(40, '706', 'Arion', 'ATCS', '2020-01-10 01:21:26', NULL),
(41, '707', 'Tu Gas', 'ATCS', '2020-01-10 01:21:26', NULL),
(42, '629', 'Rs Fatmawati', 'ATCS', '2020-01-10 01:21:26', NULL),
(43, '101', 'Jembatan Merah', 'ATCS', '2020-01-10 01:21:26', NULL),
(44, '102', 'Kartini', 'ATCS', '2020-01-10 01:21:26', NULL),
(45, '105', 'Pintu Besi', 'ATCS', '2020-01-10 01:21:26', NULL),
(46, '403', 'Taman Menteng', 'ATCS', '2020-01-10 01:21:26', NULL),
(47, '108', 'Mbal', 'ATCS', '2020-01-10 01:21:26', NULL),
(48, '109', 'Garuda', 'ATCS', '2020-01-10 01:21:26', NULL),
(49, '110', 'Wahidin', 'ATCS', '2020-01-10 01:21:26', NULL),
(50, '111', 'Gki Bina Mental', 'ATCS', '2020-01-10 01:21:26', NULL),
(51, '121', 'Ancol', 'ATCS', '2020-01-10 01:21:26', NULL),
(52, '122', 'Mangga Dua', 'ATCS', '2020-01-10 01:21:26', NULL),
(53, '508', 'Cikini', 'ATCS', '2020-01-10 01:21:26', NULL),
(54, '509', 'Rivoli', 'ATCS', '2020-01-10 01:21:26', NULL),
(55, '510', 'Raden Saleh', 'ATCS', '2020-01-10 01:21:26', NULL),
(56, '511', 'Paseban', 'ATCS', '2020-01-10 01:21:26', NULL),
(57, '512', 'Salemba', 'ATCS', '2020-01-10 01:21:26', NULL),
(58, '514', 'Slamet Riyadi', 'ATCS', '2020-01-10 01:21:26', NULL),
(59, '404', 'Imam Bonjol Cokro', 'ATCS', '2020-01-10 01:21:26', NULL),
(60, '407', 'Pamekasan', 'ATCS', '2020-01-10 01:21:26', NULL),
(61, '408', 'Teuku Umar', 'ATCS', '2020-01-10 01:21:26', NULL),
(62, '409', 'Rs Bunda', 'ATCS', '2020-01-10 01:21:26', NULL),
(63, '412', 'Taman Suropati', 'ATCS', '2020-01-10 01:21:26', NULL),
(64, '413', 'Cik Ditiro', 'ATCS', '2020-01-10 01:21:26', NULL),
(65, '415', 'Surabaya', 'ATCS', '2020-01-10 01:21:26', NULL),
(66, '416', 'Megaria', 'ATCS', '2020-01-10 01:21:26', NULL),
(67, '301', 'Biak Caringin', 'ATCS', '2020-01-10 01:21:26', NULL),
(68, '302', 'Rs Tarakan', 'ATCS', '2020-01-10 01:21:26', NULL),
(69, '304', 'Tanah Abang 2', 'ATCS', '2020-01-10 01:21:26', NULL),
(70, '309', 'Budi Kemuliaan', 'ATCS', '2020-01-10 01:21:26', NULL),
(71, '310', 'Jembatan Serong', 'ATCS', '2020-01-10 01:21:26', NULL),
(72, '311', 'Jatibaru', 'ATCS', '2020-01-10 01:21:26', NULL),
(73, '317', 'Sabang', 'ATCS', '2020-01-10 01:21:26', NULL),
(74, '804', 'Pakin', 'ATCS', '2020-01-10 01:21:26', NULL),
(75, '805', 'Tol Gedong Panjang', 'ATCS', '2020-01-10 01:21:26', NULL),
(76, '806', 'Pluit Junction', 'ATCS', '2020-01-10 01:21:26', NULL),
(77, '601', 'Pondok Indah', 'ATCS', '2020-01-10 01:21:26', NULL),
(78, '631', 'Kuningan', 'ATCS', '2020-01-10 01:21:26', NULL),
(79, '633', 'Mampang', 'ATCS', '2020-01-10 01:21:26', NULL),
(80, '607', 'Mabak', 'ATCS', '2020-01-10 01:21:26', NULL),
(81, '810', 'Tomang', 'ATCS', '2020-01-10 01:21:26', NULL),
(82, '811', 'Slipi', 'ATCS', '2020-01-10 01:21:26', NULL),
(83, '630', 'Dep. Pertanian', 'ATCS', '2020-01-10 01:21:26', NULL),
(84, '425', 'Karet Bivak', 'ATCS', '2020-01-10 01:21:26', NULL),
(85, '808', 'Jembatan Tiga', 'ATCS', '2020-01-10 01:21:26', NULL),
(86, '809', 'Jembatan Dua', 'ATCS', '2020-01-10 01:21:26', NULL),
(87, '628', 'Cilandak Kko', 'ATCS', '2020-01-10 01:21:26', NULL),
(88, '708', 'Stasiun Jatinegara', 'ATCS', '2020-01-10 01:21:26', NULL),
(89, '709', 'Kalimalang - Trikora', 'ATCS', '2020-01-10 01:21:26', NULL),
(90, '622', 'Pintu Satu Senayan', 'ATCS', '2020-01-10 01:21:26', NULL),
(91, '623', 'Gerbang Pemuda', 'ATCS', '2020-01-10 01:21:26', NULL),
(92, '710', 'Mac D Buaran', 'ATCS', '2020-01-10 01:21:26', NULL),
(93, '634', 'Pejaten Raya', 'ATCS', '2020-01-10 01:21:26', NULL),
(94, '635', 'Ragunan', 'ATCS', '2020-01-10 01:21:26', NULL),
(95, '901', 'P G C - Cililitan', 'ATCS', '2020-01-10 01:21:26', NULL),
(96, '130', 'Marunda Kebantenan', 'ATCS', '2020-01-10 01:21:26', NULL),
(97, '614', 'Melawai', 'ATCS', '2020-01-10 01:21:26', NULL),
(98, '615', 'Wijaya 2', 'ATCS', '2020-01-10 01:21:26', NULL),
(99, '616', 'Kemang Prapanca', 'ATCS', '2020-01-10 01:21:26', NULL),
(100, '617', 'Brawijaya Antasari', 'ATCS', '2020-01-10 01:21:26', NULL),
(101, '619', 'Abdul Majid Antasari', 'ATCS', '2020-01-10 01:21:26', NULL),
(102, '620', 'Manunggal Juang', 'ATCS', '2020-01-10 01:21:26', NULL),
(103, '625', 'Polim Melawai', 'ATCS', '2020-01-10 01:21:26', NULL),
(104, '711', 'J G C', 'ATCS', '2020-01-10 01:21:26', NULL),
(105, '632', 'Pancoran', 'ATCS', '2020-01-10 01:21:26', NULL),
(106, '636', 'Mampang 7 / 8', 'ATCS', '2020-01-10 01:21:26', NULL),
(107, '637', 'Basmar', 'ATCS', '2020-01-10 01:21:26', NULL),
(108, '638', 'Duren 3 Selatan', 'ATCS', '2020-01-10 01:21:26', NULL),
(109, '712', 'Buaran Bioskop', 'ATCS', '2020-01-10 01:21:26', NULL),
(110, '713', 'Klender', 'ATCS', '2020-01-10 01:21:26', NULL),
(111, '900', 'Pasar Rebo', 'ATCS', '2020-01-10 01:21:26', NULL),
(112, '902', 'Jambul', 'ATCS', '2020-01-10 01:21:26', NULL),
(113, '903', 'Tmp Kalibata', 'ATCS', '2020-01-10 01:21:26', NULL),
(114, '904', 'Duren Tiga', 'ATCS', '2020-01-10 01:21:26', NULL),
(115, '905', 'Perdatam', 'ATCS', '2020-01-10 01:21:26', NULL),
(116, '410', 'Pamekasan', 'ATCS', '2020-01-10 01:21:26', NULL),
(117, '640', 'Antasari Cipete', 'ATCS', '2020-01-10 01:21:26', NULL),
(118, '116', 'Veteran 3 - Juanda', 'ATCS', '2020-01-10 01:21:26', NULL),
(119, '307', 'Tanah Abang 2', 'ATCS', '2020-01-10 01:21:26', NULL),
(120, '319', 'Hotel Cemara', 'ATCS', '2020-01-10 01:21:26', NULL),
(121, '606', 'Bulungan', 'ATCS', '2020-01-10 01:21:26', NULL),
(122, '611', 'Rs Pertamina', 'ATCS', '2020-01-10 01:21:26', NULL),
(123, '612', 'Mayestik', 'ATCS', '2020-01-10 01:21:26', NULL),
(124, '624', 'Radio Dalam', 'ATCS', '2020-01-10 01:21:26', NULL),
(125, '609', 'Stm Penerbangan', 'ATCS', '2020-01-10 01:21:26', NULL),
(126, '610', 'Gereja Santa', 'ATCS', '2020-01-10 01:21:26', NULL),
(127, '642', 'Polim Barito 2', 'ATCS', '2020-01-10 01:21:26', NULL),
(128, '643', 'Wijaya 2', 'ATCS', '2020-01-10 01:21:26', NULL),
(129, '644', 'Haji Nawi', 'ATCS', '2020-01-10 01:21:26', NULL),
(130, '645', 'Fatmawati Cipete', 'ATCS', '2020-01-10 01:21:26', NULL),
(131, '639', 'Pondok Pinang', 'ATCS', '2020-01-10 01:21:26', NULL),
(132, '641', 'Kartini - Simatupang', 'ATCS', '2020-01-10 01:21:26', NULL),
(133, '646', 'Sekolah Duta', 'ATCS', '2020-01-10 01:21:26', NULL),
(134, '647', 'Lebak Bulus', 'ATCS', '2020-01-10 01:21:26', NULL),
(135, '648', 'Deplu', 'ATCS', '2020-01-10 01:21:26', NULL),
(136, '816', 'Tubagus Angke', 'ATCS', '2020-01-10 01:21:26', NULL),
(137, '817', 'Peta Selatan', 'ATCS', '2020-01-10 01:21:26', NULL),
(138, '818', 'Panjang', 'ATCS', '2020-01-10 01:21:26', NULL),
(139, '819', 'Terminal Kalideres', 'ATCS', '2020-01-10 01:21:26', NULL),
(140, '820', 'Tampak Siring', 'ATCS', '2020-01-10 01:21:26', NULL),
(141, '127', 'Permai Pemadam', 'ATCS', '2020-01-10 01:21:26', NULL),
(142, '128', 'Sulawesi', 'ATCS', '2020-01-10 01:21:26', NULL),
(143, '129', 'Hibrida', 'ATCS', '2020-01-10 01:21:26', NULL),
(144, '714', 'Cawang Uki', 'ATCS', '2020-01-10 01:21:26', NULL),
(145, '715', 'Haji Ten', 'ATCS', '2020-01-10 01:21:26', NULL),
(146, '716', 'Golf Rawamangun', 'ATCS', '2020-01-10 01:21:26', NULL),
(147, '720', 'Pulo Gebang', 'ATCS', '2020-01-10 01:21:26', NULL),
(148, '812', 'Pos Pengumben', 'ATCS', '2020-01-10 01:21:26', NULL),
(149, '813', 'Seskoal', 'ATCS', '2020-01-10 01:21:26', NULL),
(150, '814', 'Swadarma', 'ATCS', '2020-01-10 01:21:26', NULL),
(151, '821', 'Kebayoran Lama', 'ATCS', '2020-01-10 01:21:26', NULL),
(152, '822', 'Kembangan Raya', 'ATCS', '2020-01-10 01:21:26', NULL),
(153, '907', 'Cijantung', 'ATCS', '2020-01-10 01:21:26', NULL),
(154, '908', 'Ciracas', 'ATCS', '2020-01-10 01:21:26', NULL),
(155, '909', 'Cibubur', 'ATCS', '2020-01-10 01:21:26', NULL),
(156, '910', 'Pondol Gede', 'ATCS', '2020-01-10 01:21:26', NULL),
(157, '911', 'Bulak Rantai', 'ATCS', '2020-01-10 01:21:26', NULL),
(158, '912', 'Jatiwaringin', 'ATCS', '2020-01-10 01:21:26', NULL),
(159, '913', 'H Naman', 'ATCS', '2020-01-10 01:21:26', NULL),
(160, '914', 'Ceger', 'ATCS', '2020-01-10 01:21:26', NULL),
(161, '426', 'Pejompongan Spbu', 'ATCS', '2020-01-10 01:21:26', NULL),
(162, '1000', 'mangga besar 8', 'ATCS', '2020-01-10 01:21:26', NULL),
(163, '1001', 'gereja sion', 'ATCS', '2020-01-10 01:21:26', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ms_lokasi`
--
ALTER TABLE `ms_lokasi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_lokasi`
--
ALTER TABLE `ms_lokasi`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
