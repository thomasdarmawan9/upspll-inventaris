<!-- Nama Lokasi Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    {!! Form::hidden('nama_lokasi', $scheduleSimpang->nama_lokasi, ['class' => 'form-control']) !!}
    <p>{!! Form::label('nama_lokasi2', $scheduleSimpang->nama_lokasi, ['disabled'=>'true']) !!}</p>
</div>

<!-- Nomor Plan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nomor_plan', 'Nomor Plan:') !!}
    {!! Form::number('nomor_plan', null, ['class' => 'form-control']) !!}
</div>

<!-- Ct Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ct', 'Ct:') !!}
    {!! Form::number('ct', null, ['class' => 'form-control']) !!}
</div>

<!-- Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('time', 'Time:') !!}
    {!! Form::time('time', null, ['class' => 'form-control']) !!}
</div>

<!-- Days Field -->
<div class="form-group has-feedback{{ $errors->has('days') ? ' has-error' : '' }} col-sm-6">
{!! Form::label('days', 'days :') !!}
            <select class="form-control" name="days"  value="{{ old('days') }}" >
                <option value="">----pilih hari----</option>
                <option value="Every Day">Every Day</option>
                <option value="Monday to Friday">Monday to Friday</option>
                <option value="Saturday to Sunday">Saturday to Sunday</option>
                <option value="End of Schedule">End of Schedule</option>
            </select>
            </div>

<div id="mydiv"></div>

<div class="form-group col-sm-12">
    <input type="button" class="uk-button uk-button-default uk-button-small" value="tambah schedule" onclick="createNew()" />
</div>
<script>
    var i = 2;

    function createNew() {
        $("#mydiv").append('<div class="col-sm-12" style="border-top: 3px solid #3c8dbc;padding-bottom:10px;width:99%;margin-left:5px; ">'+'</div>' +
            '</div>' + '<div class="form-group col-sm-6">' +'<label>'+'Nomor Plan :'+'</label>'+
            '<input type="number" name="nomor_plan_arr[]" class="form-control" placeholder=""/>' +
            '</div>' + '<div class="form-group col-sm-6">' +'<label>'+'CT:'+'</label>'+
            '<input type="number" name="ct_arr[]" class="form-control" placeholder=""/>' +
            '</div>' + '<div class="form-group  col-sm-6">' + '<label>'+'time:'+'</label>'+
            '<input type="TIME" name="time_arr[]" class="form-control" placeholder=""/>' + '</div>' +
            '<div class="form-group col-sm-6">'+'<label>'+'days :'+'</label>'+
            '<select class="form-control" name="days_arr[]"'+
            '<option value="">----pilih hari----</option>'+
            '<option value="Every Day">Every Day</option>'+
            '<option value="Monday to Friday">Monday to Friday</option>'+
            '<option value="Saturday to Sunday">Saturday to Sunday</option>'+
            '<option value="End of Schedule">End of Schedule</option>'+
            '</select>'+'</div>'+
            '<input type="hidden" name="nama_lokasi_arr[]" class="form-control" placeholder="" value="{!!$scheduleSimpang->nama_lokasi!!}" />'+
            '<br/>');
        i++;
    }

</script>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'uk-button uk-button-primary uk-button-small']) !!}
    <a href="{!! route('detailSimpangs.index') !!}" class="uk-button uk-button-default uk-button-small">Cancel</a>
</div>
