<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $scheduleSimpang->id !!}</p>
</div>

<!-- Nama Lokasi Field -->
<div class="form-group">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    <p>{!! $scheduleSimpang->nama_lokasi !!}</p>
</div>

<!-- Nomor Plan Field -->
<div class="form-group">
    {!! Form::label('nomor_plan', 'Nomor Plan:') !!}
    <p>{!! $scheduleSimpang->nomor_plan !!}</p>
</div>

<!-- Ct Field -->
<div class="form-group">
    {!! Form::label('ct', 'Ct:') !!}
    <p>{!! $scheduleSimpang->ct !!}</p>
</div>

<!-- Time Field -->
<div class="form-group">
    {!! Form::label('time', 'Time:') !!}
    <p>{!! $scheduleSimpang->time !!}</p>
</div>

<!-- Days Field -->
<div class="form-group">
    {!! Form::label('days', 'Days:') !!}
    <p>{!! $scheduleSimpang->days !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $scheduleSimpang->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $scheduleSimpang->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $scheduleSimpang->deleted_at !!}</p>
</div>

