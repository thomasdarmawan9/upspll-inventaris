<!DOCTYPE html>
<html lang="en">

<head>

    <head>
        <meta charset="UTF-8">
        <title>UP SPLL DISHUB</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css">
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/_all-skins.min.css">

        <!-- iCheck -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <!-- UIkit CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/css/uikit.min.css" />

    </head>
    <style type="text/css">
        .rounded-circle {
            margin-right: 15px !important;
        }

        a {
            color: #fafafa !important;
        }

        a:hover {
            text-decoration: none;
            cursor:pointer;
        }

        .lokasi:hover{
            cursor:pointer;
        }

        .maps:hover{
            cursor:pointer;
        }

        @media only screen and (max-width: 600px) {
            .rounded {
                width: 60px;
            }

            .rounded-circle {
                width: 40px !important;
                margin-right: 7px !important;
            }

            .tulisan {
                font-size: 13px !important;
            }

            .logo {
                margin-right: 40px;
                margin-left: -10px;
            }

            .tombol {}
        }
    </style>
</head>

<body style="background:#2C156F">
    <header style="width: 100%;color: #fafafa;">

        <div class="row" style="background: #2C156F">

            <div class="col-md col-5 text-left pl-4">
                <div class=" mx-auto mt-4">
                    <p class="tulisan"> teknisi</p>
                </div>
            </div>

            <div class="col-md col-3 text-center covlogo">
                <img src="{{asset('public/img/login.png')}}" class="rounded mt-3 mb-3 logo bg-white"
                    style="width:60px;height:40px;">
            </div>

            <div class="col-md col-4 text-right pr-4">
                <div class=" mx-auto mt-3">
                    <p class="tulisan" style=""> <a style="color:black !important;font-size: 13px;"
                            href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Sign out
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            @csrf
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                </div>

            </div>

        </div>
        <div class="row" style="background: #f8f8f8">
            <div class="col-md col-6  border-secondary border-right text-center lokasi">
                <a href="{!! url('/') !!}" style="color:black !important;">
                    <div class=" mx-auto mt-3">
                        <p class="tulisan" style="font-size: 19px;"> Lokasi
                    </div>
                </a>
            </div>
            <div class="col-md col-6  border-secondary border-left text-center maps">
                <a href="http://upspll.jakarta.go.id/maps" style="color:black !important">
                    <div class=" mx-auto mt-3">
                        <p class="tulisan" style="font-size: 19px;"> Maps
                    </div>
                </a>
            </div>
        </div>
    </header>


<div class="container ">

<div class="content" style="color:black !important;height:80vh;overflow-y:scroll;" >
        <div class="box box-primary">
            <div class="box-body">
                <div class="row p-4">

                    <div class="col-sm-12 pt-3">
                        <h3>Data Lokasi</h3>
                    </div>
                    <!-- Id Lokasi Fk Field -->
                    <div class="col-sm-12">
                        {!! Form::label('nama_lokasi', 'Nama lokasi:') !!}
                        <p>{{ $detailSimpang->nama_lokasi }}</p>
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('alamat', 'Alamat:') !!}
                        <p>{{ $detailSimpang->alamat }}</p>
                    </div>
                    <!-- Lat Field -->
                    <div class="col-sm-4">
                        {!! Form::label('lat', 'Lat:') !!}
                        <p>{{ $detailSimpang->lat }}</p>
                    </div>

                    <!-- Long Field -->
                    <div class="col-sm-4">
                        {!! Form::label('long', 'Long:') !!}
                        <p>{{ $detailSimpang->long }}</p>
                    </div>


                    <div class="col-sm-12 pt-3">
                        <h3>Data Tiang</h3>
                    </div>
                    <div class="col-sm-6 col-6 border-bottom">
                        <label>Jenis Tiang</label>
                    </div>
                    <div class="col-sm-6 col-6 border-bottom">
                        <label>Jumlah Tiang</label>
                    </div>

                    @foreach($detailTiang as $detailTiangs)
                    <!-- Jenis Tiang Field -->
                    <div class="col-sm-6 col-6">
                        <p>{!! $detailTiangs->jenis_tiang !!}</p>
                    </div>

                    <!-- Jml Tiang Field -->
                    <div class="col-sm-6 col-6">
                        <p>{!! $detailTiangs->jml_tiang !!}</p>
                    </div>
                    @endforeach


                    <div class="col-sm-12 pt-3">
                        <h3>Data Lampu</h3>
                    </div>
                    <div class="col-sm-4 col-4 border-bottom ">
                        <label>Jenis Lampu</label>
                    </div>
                    <div class="col-sm-4 col-4 border-bottom">
                        <label>Jumlah Lampu</label>
                    </div>
                    <div class="col-sm-4 col-4 border-bottom ">
                        <label>Tahun Pasang Lampu</label>
                    </div>
                    @foreach($detailLampu as $detailLampus)
                    <!-- Jenis Tiang Field -->
                    <div class="col-sm-4 col-4">
                        <p>{!! $detailLampus->jenis_lampu !!}</p>
                    </div>
                    <!-- Jml Tiang Field -->
                    <div class="col-sm-4 col-4">
                        <p>{!! $detailLampus->jml_lampu !!}</p>
                    </div>
                    <!-- Jml Tiang Field -->
                    <div class="col-sm-4 col-4">
                        <p>{!! $detailLampus->thn_psng_unit !!}</p>
                    </div>
                    @endforeach


                    <div class="col-sm-12 pt-3">
                        <h3>Data Controller</h3>
                    </div>
                    <!-- Controller Field -->
                    <div class="col-sm-4">
                        {!! Form::label('controller', 'Controller:') !!}
                        @if( $detailSimpang->controller != 'null' )
                        <p>{{ $detailSimpang->controller }}</p>
                        @endif
                        @if( $detailSimpang->controller == "" )
                        <label class="label-danger"> Tidak terpasang Controller Pelican Crossing</label>
                        @endif
                    </div>
                    <!-- Controller Pc Field -->
                    <div class="col-sm-4">
                        {!! Form::label('controller_pc', 'Controller Pelican crossing:') !!}
                        @if( $detailSimpang->controller_pc != 'null' )
                        <p>{{ $detailSimpang->controller_pc }}</p>
                        @endif
                        @if( $detailSimpang->controller_pc == "")
                        <label class="label-danger"> Tidak terpasang Controller Pelican Crossing</label>
                        @endif
                    </div>
                    <!-- Thn Psng Controller Field -->
                    <div class="col-sm-4">
                        {!! Form::label('tahun_psng_controller', 'Tahun Psng Controller:') !!}
                        <p>{{ $detailSimpang->thn_psng_controller }}</p>
                    </div>




                    <div class="col-sm-12 pt-3">
                        <h3>Data Simpang</h3>
                    </div>
                    <!-- gambar_simpang Field -->
                    <div class="col-sm-12">
                        {!! Form::label('gambar_simpang', 'Gambar Simpang:') !!}
                        <p><img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->gambar_simpang) }}"
                                alt="gambar tidak ada" /></p>
                    </div>


                    <div class="col-sm-4 text-center">
                        <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                            {!! Form::label('fase_a', 'Fase A:') !!}
                            <p> <img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_a) }}"
                                    alt="gambar tidak ada" />
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-4 text-center">
                        <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                            {!! Form::label('fase_b', 'Fase B:') !!}
                            <p><img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_b) }}"
                                    alt="gambar tidak ada" /></p>
                        </div>
                    </div>

                    <div class="col-sm-4 text-center">
                        <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                            {!! Form::label('fase_c', 'Fase C:') !!}
                            <p><img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_c) }}"
                                    alt="gambar tidak ada" /></p>
                        </div>
                    </div>

                    <div class="col-sm-12 uk-margin-small-top"></div>

                    <!-- Fase D Field -->
                    <div class="col-sm-4 text-center">
                        <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                            {!! Form::label('fase_d', 'Fase D:') !!}
                            <p> <img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_d) }}"
                                    alt="gambar tidak ada" /></p>
                        </div>
                    </div>
                    <!-- Fase E Field -->
                    <div class="col-sm-4 text-center">
                        <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                            {!! Form::label('fase_e', 'Fase E:') !!}
                            <p> <img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_e) }}"
                                    alt="gambar tidak ada" /></p>
                        </div>
                    </div>

                    <!-- Fase F Field -->
                    <div class="col-sm-4 text-center">
                        <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                            {!! Form::label('fase_f', 'Fase F:') !!}
                            <p> <img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_f) }}"
                                    alt="gambar tidak ada" /></p>
                        </div>
                    </div>



                    <div class="col-sm-12 pb-4" style="padding-top:30px">
                        <a href="{{ url('/') }}" class="uk-button uk-button-secondary uk-button-small">Back</a>
                    </div>
                </div>

            </div>
        </div>


    </div>


</div>

<div class="row bg-white uk-position-bottom p-1" style="max-width:none !important">
<div class="col-md-12 col-12 text-center text-bold p-2" style="font-size:12px;">Copyright © 2020 UPSPLL DISHUB DKI JAKARTA. All rights reserved.</div>
</div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js">
    </script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit-icons.min.js"></script>
    <!-- chartJS -->

</body>

</html>