<!-- Nama Lokasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    {!! Form::text('nama_lokasi', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Tiang Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_tiang', 'Jenis Tiang:') !!}
    {!! Form::text('jenis_tiang', null, ['class' => 'form-control']) !!}
</div>

<!-- Jml Tiang Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jml_tiang', 'Jml Tiang:') !!}
    {!! Form::number('jml_tiang', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('detailTiangs.index') !!}" class="btn btn-default">Cancel</a>
</div>
