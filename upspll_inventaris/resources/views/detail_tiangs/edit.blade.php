@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Detail Tiang
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($detailTiang, ['route' => ['detailTiangs.update', $detailTiang->id], 'method' => 'patch']) !!}

                        @include('detail_tiangs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection