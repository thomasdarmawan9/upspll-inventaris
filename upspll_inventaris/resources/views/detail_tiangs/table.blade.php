<table class="table table-responsive" id="tabless">
    <thead>
        <tr>
            <th>Nama Lokasi</th>
        <th>Jenis Tiang</th>
        <th>Jml Tiang</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($detailTiangs as $detailTiang)
        <tr>
            <td>{!! $detailTiang->nama_lokasi !!}</td>
            <td>{!! $detailTiang->jenis_tiang !!}</td>
            <td>{!! $detailTiang->jml_tiang !!}</td>
            <td>
                {!! Form::open(['route' => ['detailTiangs.destroy', $detailTiang->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('detailTiangs.show', [$detailTiang->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('detailTiangs.edit', [$detailTiang->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>