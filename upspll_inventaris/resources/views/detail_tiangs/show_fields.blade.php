<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $detailTiang->id !!}</p>
</div>

<!-- Nama Lokasi Field -->
<div class="form-group">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    <p>{!! $detailTiang->nama_lokasi !!}</p>
</div>

<!-- Jenis Tiang Field -->
<div class="form-group">
    {!! Form::label('jenis_tiang', 'Jenis Tiang:') !!}
    <p>{!! $detailTiang->jenis_tiang !!}</p>
</div>

<!-- Jml Tiang Field -->
<div class="form-group">
    {!! Form::label('jml_tiang', 'Jml Tiang:') !!}
    <p>{!! $detailTiang->jml_tiang !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $detailTiang->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $detailTiang->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $detailTiang->deleted_at !!}</p>
</div>

