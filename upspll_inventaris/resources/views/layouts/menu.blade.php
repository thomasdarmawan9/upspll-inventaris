

<li class="{{ Request::is('msLokasis*') ? 'active' : '' }}">
    <a href="{{ route('msLokasis.index') }}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Master Lokasi</span></a>
</li>

<li class="{{ Request::is('detailSimpangs*') ? 'active' : '' }}">
    <a href="{{ route('detailSimpangs.index') }}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Detail Simpang</span></a>
</li>

<!-- <li class="{{ Request::is('planSimpangs*') ? 'active' : '' }}">
    <a href="{{ route('planSimpangs.index') }}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Plan Simpang</span></a>
</li>

<li class="{{ Request::is('scheduleSimpangs*') ? 'active' : '' }}">
    <a href="{!! route('scheduleSimpangs.index') !!}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Schedule Simpangs</span></a>
</li>
 -->
<li class="{{ Request::is('masterTiangs*') ? 'active' : '' }}">
    <a href="{!! route('masterTiangs.index') !!}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Master Tiang</span></a>
</li><li class="{{ Request::is('masterLampus*') ? 'active' : '' }}">
    <a href="{!! route('masterLampus.index') !!}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Master Lampu</span></a>
</li>
<!-- 
<li class="{{ Request::is('detailTiangs*') ? 'active' : '' }}">
    <a href="{!! route('detailTiangs.index') !!}"><i class="fa fa-edit"></i><span>Detail Tiangs</span></a>
</li>
 -->
<!-- <li class="{{ Request::is('detailLampus*') ? 'active' : '' }}">
    <a href="{!! route('detailLampus.index') !!}"><i class="fa fa-edit"></i><span>Detail Lampus</span></a>
</li>
 -->
