@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Master Tiang
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($masterTiang, ['route' => ['masterTiangs.update', $masterTiang->id], 'method' => 'patch']) !!}

                        @include('master_tiangs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection