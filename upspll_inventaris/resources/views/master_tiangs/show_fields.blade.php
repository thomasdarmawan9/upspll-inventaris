<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $masterTiang->id !!}</p>
</div>

<!-- Jenis Tiang Field -->
<div class="form-group">
    {!! Form::label('jenis_tiang', 'Jenis Tiang:') !!}
    <p>{!! $masterTiang->jenis_tiang !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $masterTiang->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $masterTiang->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $masterTiang->deleted_at !!}</p>
</div>

