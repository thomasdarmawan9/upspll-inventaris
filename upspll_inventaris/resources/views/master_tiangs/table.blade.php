<table class="table table-responsive uk-table uk-table-hover uk-table-striped" id="tabless ">
    <thead>
        <tr>
            <th>Jenis Tiang</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($masterTiangs as $masterTiang)
        <tr>
            <td>{!! $masterTiang->jenis_tiang !!}</td>
            <td>
                {!! Form::open(['route' => ['masterTiangs.destroy', $masterTiang->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('masterTiangs.show', [$masterTiang->id]) !!}" class='uk-button uk-button-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('masterTiangs.edit', [$masterTiang->id]) !!}" class='uk-button uk-button-primary'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'uk-button uk-button-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>