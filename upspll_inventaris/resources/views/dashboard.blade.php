<!DOCTYPE html>
<html lang="en">

<head>

    <head>
        <meta charset="UTF-8">
        <title>UP SPLL DISHUB</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css">
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/_all-skins.min.css">

        <!-- iCheck -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <!-- UIkit CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/css/uikit.min.css" />

    </head>
    <style type="text/css">
        .rounded-circle {
            margin-right: 15px !important;
        }

        a {
            color: #fafafa !important;
        }

        a:hover {
            text-decoration: none;
        }

        @media only screen and (max-width: 600px) {
            .rounded {
                width: 60px;
            }

            .rounded-circle {
                width: 40px !important;
                margin-right: 7px !important;
            }

            .tulisan {
                font-size: 13px !important;
            }

            .logo {
                margin-right: 40px;
                margin-left: -10px;
            }

            .tombol {}
        }
    </style>
</head>

<body style="background:#2C156F">
    <header style="width: 100%;color: #fafafa;" class="fixed-top">

        <div class="row" style="background: #2C156F">

            <div class="col-md col-5 text-left pl-4">
                <div class=" mx-auto mt-4">
                    <p class="tulisan"> teknisi</p>
                </div>
            </div>

            <div class="col-md col-3 text-center covlogo">
                <img src="{{asset('public/img/login.png')}}" class="rounded mt-3 mb-3 logo bg-white"
                    style="width:60px;height:40px;">
            </div>

            <div class="col-md col-4 text-right pr-4">
                <div class=" mx-auto mt-3">
                    <p class="tulisan" style=""> <a style="color:black !important;font-size: 13px;"
                            href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Sign out
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            @csrf
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                </div>

            </div>

        </div>
        <div class="row" style="background: #f8f8f8">
            <div class="col-md col-6  border-secondary border-right text-center">
                <a href="{!! url('/') !!}" style="color:black !important">
                    <div class=" mx-auto mt-3">
                        <p class="tulisan" style="font-size: 19px;"> Lokasi
                    </div>
                </a>
            </div>
            <div class="col-md col-6  border-secondary border-left text-center">
                <a href="http://upspll.jakarta.go.id/maps" style="color:black !important">
                    <div class=" mx-auto mt-3">
                        <p class="tulisan" style="font-size: 19px;"> Maps
                    </div>
                </a>
            </div>
        </div>
    </header>
    <div class="container pb-5" style="padding-top:140px;height:100vh;overflow-y:scroll;">
        <div class="row">
            <div class="col-md-12 col-12">

                <form class="uk-search uk-search-default bg-white ml-3" action="{!! url('/detailSimpang/cari') !!}"
                    method="GET">
                    <span uk-search-icon></span>
                    <input class="uk-search-input" type="search" placeholder="Search..." name="cari">

                </form>

            </div>
        </div>
        <div class="row p-3 pb-2">

            @foreach($detailSimpangs as $detailSimpang)
            <div class="col-12 col-md-4 pt-2">
                <div class="uk-card uk-card-default uk-card-hover uk-card-body" style="height:210px;">
                    <h3 class="uk-card-title" style="height:70px">{{ $detailSimpang->nama_lokasi }}</h3>
                    <div class="row">
                        <div class="col-4">
                            Lat : <p>{{ $detailSimpang->lat }} </p>
                        </div>
                        <div class="col-4">
                            Long : <p>{{ $detailSimpang->long }}</p>
                        </div>
                        <div class="col-4">
                            <button class="uk-button uk-button-primary tombol"><a
                                    href="{!! url('/detail', [$detailSimpang->id]) !!}">Detail</a></button>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach


        </div>
    </div>

    <div class="row bg-white fixed-bottom p-1" style="max-width:none !important">
        <div class="col-md-12 col-12 text-center text-bold p-2" style="font-size:12px;">Copyright © 2020 UPSPLL DISHUB
            DKI JAKARTA. All rights reserved.</div>
    </div>




    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js">
    </script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit-icons.min.js"></script>
    <!-- chartJS -->

</body>

</html>