<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
           table {
  border-collapse: collapse;
}
 th {
  background: #ccc;
}

th, td {
  border: 1px solid #ccc;
  padding: 8px;
}

tr:nth-child(even) {
  background: #efefef;
}

tr:hover {
  background: #d1d1d1;
}
    </style>
</head>

<body>

    <h2>Laporan Informasi Detail Simpang</h2>
    <p><b> {!! Form::label('nama_lokasi', 'Nama lokasi:') !!} : {{ $detailSimpang->nama_lokasi }}</b></p>
    <table id="table" class="uk-table uk-table-hover uk-table-striped">

        <tr>
            <td> {!! Form::label('lat', 'Lat:') !!}
                <p>{{ $detailSimpang->lat }}</p>
            </td>
            <td>{!! Form::label('long', 'Long:') !!}
                <p>{{ $detailSimpang->long }}</p>
            </td>
            <td> {!! Form::label('alamat', 'Alamat:') !!}
                <p>{{ $detailSimpang->alamat }}</p>
            </td>
           
        </tr>
        <tr>
            <td> {!! Form::label('controller', 'Controller:') !!}
                <p>{{ $detailSimpang->controller }}</p>
            </td>
            <td> {!! Form::label('tahun_psng_controller', 'Thn Pasang Controller:') !!}
                <p>{{ $detailSimpang->thn_psng_controller }}</p>
            </td>
            <td>{!! Form::label('controller_pc', 'Controller Pc:') !!}
                <p>{{ $detailSimpang->controller_pc }}</p>
            </td>
        </tr>
    </table>
    <br>

    
    
    @if($detailTiang != "null")
    <table>
        <thead>
            <tr>
        <th>Jenis Tiang</th>
        <th>Jumlah Tiang</th>
        
            </tr>
        </thead>
        <tbody>
        
    @foreach($detailTiang as $detailTiangs)
            <tr>
                <td>{!! $detailTiangs->jenis_tiang !!}</td>
            <td>{!! $detailTiangs->jml_tiang !!}</td>
            
           </tr>
    @endforeach
        </tbody>
    </table>
    @endif
<br>

@if($detailLampu != "null")
<table>
        <thead>
            <tr>
        <th>Jenis Lampu</th>
        <th>Jumlah Lampu</th>
        <th>Tahun Pasang Unit</th>
            </tr>
        </thead>
        <tbody>
       
@foreach($detailLampu as $detailLampus)
            <tr>
                <td>{!! $detailLampus->jenis_lampu !!}</td>
            <td>{!! $detailLampus->jml_lampu !!}</td>
            <td>{!! $detailLampus->thn_psng_unit !!}</td>
            
           </tr>
    @endforeach
        </tbody>
    </table>
    @endif
    <br>

    @if($plan != "null")
    <h2>Laporan Informasi Planning Simpang</h2>
    <table id="table" class="uk-table uk-table-hover uk-table-striped">
    <thead>
            <tr>
        <th>nomor plan</th>
        <th>Cycle time</th>
        <th>offset</th>
        <th>a</th>
        <th>b</th>
        <th>c</th>
        <th>d</th>
        <th>e</th>
        <th>f</th>
        <th>r minus</th>
        <th>r plus</th>
        <th>y minus</th>
        <th>y plus</th>
        <th>z minus</th>
        <th>z plus</th>
        <th>q minus</th>
        <th>q plus</th>
            </tr>
        </thead>
    <tbody>
    
    @foreach($plan as $plans)
    <tr>
    <td>{{ $plans->nomor_plan }}</td>
    <td>{{ $plans->cycle_time }}</td>
    <td>{{ $plans->offset }}</td>
    <td>{{ $plans->a }}</td>
    <td>{{ $plans->b }}</td>
    <td>{{ $plans->c }}</td>
    <td>{{ $plans->d }}</td>
    <td>{{ $plans->e }}</td>
    <td>{{ $plans->f }}</td>
    <td>{{ $plans->r_minus }}</td>
    <td>{{ $plans->r_plus }}</td>
    <td>{{ $plans->y_minus }}</td>
    <td>{{ $plans->y_plus }}</td>
    <td>{{ $plans->z_minus }}</td>
    <td>{{ $plans->z_plus }}</td>
    <td>{{ $plans->q_minus }}</td>
    <td>{{ $plans->q_plus }}</td>
    </tr>
    
    
    @endforeach
    </tbody>
    
    </table>
    @endif
    
    <br>

    @if($schedule != "null")
<h2>Laporan Informasi Schedule Simpang</h2>
<table id="table" class="uk-table uk-table-hover uk-table-striped">
<thead>
        <tr>
    <th>nomor plan</th>
    <th>Cycle time</th>
    <th>time</th>
    <th>days</th>
        </tr>
    </thead>
<tbody>

@foreach($schedule as $schedules)
<tr>
<td>{{ $schedules->nomor_plan }}</td>
<td>{{ $schedules->cycle_time }}</td>
<td>{{ $schedules->time }}</td>
<td>{{ $schedules->days }}</td>
</tr>
@endforeach
</tbody>

</table>
@endif
</body>

</html>