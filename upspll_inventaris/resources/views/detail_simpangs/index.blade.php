@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Detail Simpang</h1>
        <h1 class="pull-right">
        @if(auth::user()->role=='Admin')
           <a class="uk-button uk-button-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('detailSimpangs.create') !!}">Add New</a>
        @endif
        @if(auth::user()->role=='Operator')
           <a class="uk-button uk-button-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('detailSimpangs.create') !!}">Add New</a>
        @endif
        @if(auth::user()->role=='teknisi')
           <a class="uk-button uk-button-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('detailSimpangs.create') !!}" disabled>Add New</a>
        @endif
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('detail_simpangs.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

