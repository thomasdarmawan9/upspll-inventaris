
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#copc").click(function(){
    $(".co").hide();
  });
  $("#copc").click(function(){
    $(".copc").show();
  });
   $("#co").click(function(){
    $(".copc").hide();
  });
  $("#co").click(function(){
    $(".co").show();
  });
});
</script>
<!-- Id Lokasi Fk Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    {!! Form::select('nama_lokasi', $nama_lokasi, null, ['class' => 'form-control']) !!}
</div>

<!-- Lat Field -->
<div class="form-group col-sm-4">
    {!! Form::label('lat', 'Lat:') !!}
    {!! Form::number('lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Long Field -->
<div class="form-group col-sm-4">
    {!! Form::label('long', 'Long:') !!}
    {!! Form::number('long', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('alamat', 'Alamat:') !!}
    {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
</div>
<!-- Tiang Field -->

<style>
.setengah {
  content: "\00BD";
}
</style>


<div class="col-sm-12">
  <div class="uk-card uk-card-default uk-card-large uk-card-body">
     <div class="form-group col-sm-12" style="padding-left:0px;">
     
        <h3>Data Tiang dan Unit Lampu</h3>
     </div>   
     <div class="col-sm-12  " style="padding:0px;">

     <label>Data Tiang</label>
     <div class="row">
  <div class="col-lg-6">
            @foreach ($detailTiang as  $detailTiangs)
            <input  type="text" name="jenis_tiang[]" value="{{ $detailTiangs->jenis_tiang }} " class="form-control"  style="margin:7px;" disabled>
            
            <input  type="hidden" name="id[]" value="{{ $detailTiangs->id }} " class="form-control">
            <input  type="hidden" name="jenis_tiang[]" value="{{ $detailTiangs->jenis_tiang }} " class="form-control">
             @endforeach
  </div><!-- /.col-lg-6 -->
  <div class="col-lg-6">
  @foreach ($detailTiang as  $detailTiangs)
  {!! Form::number('jml_tiang[]', $detailTiangs->jml_tiang, ['class' => 'form-control jml_tiang']) !!}
           @endforeach
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->

<label>Data Lampu</label>
     <div class="row">
  <div class="col-lg-4">
            @foreach ($detailLampu as $detailLampus)
            <input  type="text" name="jenis_lampu[]" value="{{ $detailLampus->jenis_lampu }} " class="form-control"  style="margin:7px;" disabled>
            
            <input  type="hidden" name="id[]" value="{{ $detailLampus->id }} " class="form-control">
            <input  type="hidden" name="jenis_lampu[]" value="{{ $detailLampus->jenis_lampu }} " class="form-control">
             @endforeach
  </div><!-- /.col-lg-6 -->
  <div class="col-lg-4">
  @foreach ($detailLampu as $detailLampus)
          
  {!! Form::number('jml_lampu[]', $detailLampus->jml_lampu, ['class' => 'form-control jml_lampu']) !!}   
   @endforeach
  </div><!-- /.col-lg-3 -->
  <div class="col-lg-4">
  @foreach ($detailLampu as $detailLampus)
  {!! Form::selectYear('thn_psng_unit[]', $detailLampus->thn_psng_unit,'', null,['class' => 'form-control thn_lampu']) !!}
@endforeach
  </div><!-- /.col-lg-3 -->
</div><!-- /.row -->




            
          
          
             <input type="hidden" name="nama_lokasi_arr[]" class="form-control nama_lokasi" placeholder="" value="" />
            
     </div>
    
<!-- <div id="mydiv" class=" col-sm-9" style="float:right;"></div>
<div id="mydiv2" class=" col-sm-9" style="float:right;"></div> -->
    </div>
</div>

    


<div class="col-sm-12">

<div class="uk-card uk-card-default uk-card-large uk-card-body">
<div class="form-group col-sm-12" style="padding-left:0px;">
     
     <h3>Data Controller</h3><small>*required</small>
     </div>  
<div class="col-sm-12" style="padding:30px;">
    <label>Pilih Jenis Controller</label>
    <a id="co" class="uk-button uk-button-secondary uk-button-small">Controller</a>
    <a id="copc" class="uk-button uk-button-secondary uk-button-small">Controller PC</a>
</div>


<div class="form-group col-sm-6">
    {!! Form::label('thn_psng_controller', 'Tahun Pasang Controller:') !!}
    <br>
    {!! Form::selectYear('thn_psng_controller', $detailSimpang->thn_psng_controller, ['class' => 'form-control']) !!}
</div>

<!-- Thn Psng Unit Field -->




<!-- Controller Field -->

<div class="form-group has-feedback{{ $errors->has('controller') ? ' has-error' : '' }} col-sm-6 co">
{!! Form::label('controller', 'Controller:') !!}
            <select class="form-control" name="controller"  value="{{ old('controller') }}" >
                <option value="">----pilih jenis controller----</option>
                <option value="Stats A (ATCS)" {{ $detailSimpang->controller == 'Stats A (ATCS)' ? 'selected' : '' }}>Stats A (ATCS)</option>
                <option value="Stats A (NON ATCS)" {{ $detailSimpang->controller == 'Stats A (NON ATCS)' ? 'selected' : '' }}>Stats A (NON ATCS)</option>
                <option value="Newtrap" {{ $detailSimpang->controller == 'Newtrap' ? 'selected' : '' }}>Newtrap</option>
                <option value="UMC AC" {{ $detailSimpang->controller == 'UMC AC' ? 'selected' : '' }}>UMC AC</option>
                <option value="UMC DC" {{ $detailSimpang->controller == 'UMC DC' ? 'selected' : '' }}>UMC DC</option>
                <option value="HMK" {{ $detailSimpang->controller == 'HMK' ? 'selected' : '' }}>HMK</option>
            </select>
            </div>
           
<!-- Thn Psng Controller Field -->
<style>
#thn_psng_controller{
    display: block;
    width: 100%;
    padding: 6px 12px;
    font-size: 14px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
}
.thn_lampu,
.jml_tiang,
.jml_lampu{
    margin:7px;
}
</style>
<div class="form-group has-feedback{{ $errors->has('controller_pc') ? ' has-error' : '' }} col-sm-6 copc" style="display:none">
{!! Form::label('controller_pc', 'Controller PC:') !!}
            <select class="form-control" name="controller_pc"  value="{{ old('controller_pc') }}" >
                <option value="">----pilih jenis controller PC----</option>
                <option value="Newtrap" {{ $detailSimpang->controller_pc == 'Newtrap' ? 'selected' : '' }}>Newtrap</option>
                <option value="UMC" {{ $detailSimpang->controller_pc == 'UMC' ? 'selected' : '' }}>UMC</option>
                <option value="HMK" {{ $detailSimpang->controller_pc == 'HMK' ? 'selected' : '' }}>HMK</option>
                <option value="Telepico" {{ $detailSimpang->controller_pc == 'Telepico' ? 'selected' : '' }}>Telepico</option>
            </select>
            </div>


            <div class="form-group col-sm-12">
</div>

</div>

</div>

<!-- Fase A Field -->
<div class="form-group col-sm-12" style="padding-top:30px;">

<h3>Data Simpang</h3>
    {!! Form::label('gambar_simpang', 'Gambar Simpang:') !!}
    {!! Form::file('gambar_simpang', null, ['class' => 'form-control']) !!}
</div>
<!-- Fase A Field -->
<div class="form-group col-sm-6" >
    {!! Form::label('fase_a', 'Fase A:') !!}
    {!! Form::file('fase_a', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase B Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_b', 'Fase B:') !!}
    {!! Form::file('fase_b', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase C Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_c', 'Fase C:') !!}
    {!! Form::file('fase_c', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase D Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_d', 'Fase D:') !!}
    {!! Form::file('fase_d', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase E Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_e', 'Fase E:') !!}
    {!! Form::file('fase_e', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_f', 'Fase F:') !!}
    {!! Form::file('fase_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'uk-button uk-button-primary uk-button-small']) !!}
    <a href="{{ route('detailSimpangs.index') }}" class="uk-button uk-button-default uk-button-small">Cancel</a>
</div>
