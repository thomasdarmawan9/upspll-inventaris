
     <div class="table-responsive" style="width:100%;">
    <table id="table" class="uk-table uk-table-hover uk-table-striped" >
        <thead>
            <tr>
        <th>Nama Lokasi</th>
        <th>Lat</th>
        <th>Long</th>
        <th>Alamat</th>
        
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($detailSimpangs as $detailSimpang)
            <tr>
                <td>{{ $detailSimpang->nama_lokasi }}</td>
            <td>{{ $detailSimpang->lat }}</td>
            <td>{{ $detailSimpang->long }}</td>
            <td>{{ $detailSimpang->alamat }}</td>
            
            <input type="hidden" name="nama_lokasi" class="form-control nama_lokasi" placeholder="" value="{{ $detailSimpang->nama_lokasi }}" />
                <td>
                    {!! Form::open(['route' => ['detailSimpangs.destroy', $detailSimpang->id], 'method' => 'delete']) !!}
                    <div class='btn-group text-center'>
                        <a href="{{ route('detailSimpangs.show', [$detailSimpang->id]) }}" class='uk-button uk-button-default '><i class="glyphicon glyphicon-eye-open"></i></a>
                        @if(auth::user()->role=='Admin')
                        <a href="{{ route('detailSimpangs.edit', [$detailSimpang->id]) }}" class='uk-button uk-button-primary '><i class="glyphicon glyphicon-edit"></i> </a>
                        <a href="upspll_inventaris/plansimpangadd/{{$detailSimpang->id}}" class="uk-button uk-button-primary " style="background:#0097a7 !important"><i class="glyphicon glyphicon-list-alt"></i> </a>
                      <a href="upspll_inventaris/schedulesimpangadd/{{$detailSimpang->id}}" class="uk-button uk-button-primary " style="background:#3949ab  !important"><i class="glyphicon glyphicon-check"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'uk-button uk-button-danger ', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endif
                        @if(auth::user()->role=='Operator')
                        <a href="{{ route('detailSimpangs.edit', [$detailSimpang->id]) }}" class='uk-button uk-button-primary'><i class="glyphicon glyphicon-edit"></i> </a>
                        <a href="upspll_inventaris/plansimpangadd/{{$detailSimpang->id}}" class="uk-button uk-button-primary " style="background:#0097a7 !important"><i class="glyphicon glyphicon-list-alt"></i> </a>
                      <a href="upspll_inventaris/schedulesimpangadd/{{$detailSimpang->id}}" class="uk-button uk-button-primary " style="background:#3949ab  !important"><i class="glyphicon glyphicon-check"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'uk-button uk-button-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endif
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>