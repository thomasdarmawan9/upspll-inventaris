@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Detail Simpang
        </h1>
    </section>
    <div class="content">
    @if(auth::user()->role=='Admin')
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
            <div class="row">
                    {!! Form::open(['route' => 'detailSimpangs.store','enctype' => 'multipart/form-data','file'=>'true']) !!}

                        @include('detail_simpangs.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endif
        @if(auth::user()->role=='Operator')
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
            <div class="row">
                    {!! Form::open(['route' => 'detailSimpangs.store','enctype' => 'multipart/form-data','file'=>'true']) !!}

                        @include('detail_simpangs.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endif
        @if(auth::user()->role=='Teknisi')
        <h1>
        404 Akses ditolak
        </h1>
        @endif
    </div>
@endsection
