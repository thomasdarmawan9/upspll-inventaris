<h3>Data Lokasi</h3>
<!-- Id Lokasi Fk Field -->
<div class="col-sm-12">
    {!! Form::label('nama_lokasi', 'Nama lokasi:') !!}
    <p>{{ $detailSimpang->nama_lokasi }}</p>
</div>
<div class="col-sm-4">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $detailSimpang->alamat }}</p>
</div>
<!-- Lat Field -->
<div class="col-sm-4">
    {!! Form::label('lat', 'Lat:') !!}
    <p>{{ $detailSimpang->lat }}</p>
</div>

<!-- Long Field -->
<div class="col-sm-4">
    {!! Form::label('long', 'Long:') !!}
    <p>{{ $detailSimpang->long }}</p>
</div>


<h3>Data Tiang</h3>
<div class="col-sm-6">
    <label>Jenis Tiang</label>
</div>
<div class="col-sm-6">
    <label>Jumlah Tiang</label>
</div>
@foreach($detailTiang as $detailTiangs)
<!-- Jenis Tiang Field -->
<div class="col-sm-6">
    <p>{!! $detailTiangs->jenis_tiang !!}</p>
</div>

<!-- Jml Tiang Field -->
<div class="col-sm-6">
    <p>{!! $detailTiangs->jml_tiang !!}</p>
</div>
@endforeach




<h3>Data Lampu</h3>
<div class="col-sm-4">
    <label>Jenis Lampu</label>
</div>
<div class="col-sm-4">
    <label>Jumlah Lampu</label>
</div>
<div class="col-sm-4">
    <label>Tahun Pasang Unit Lampu</label>
</div>
@foreach($detailLampu as $detailLampus)
<!-- Jenis Tiang Field -->
<div class="col-sm-4">
    <p>{!! $detailLampus->jenis_lampu !!}</p>
</div>
<!-- Jml Tiang Field -->
<div class="col-sm-4">
    <p>{!! $detailLampus->jml_lampu !!}</p>
</div>
<!-- Jml Tiang Field -->
<div class="col-sm-4">
    <p>{!! $detailLampus->thn_psng_unit !!}</p>
</div>
@endforeach

<h3>Data Controller</h3>
<!-- Controller Field -->
<div class="col-sm-4">
    {!! Form::label('controller', 'Controller:') !!}
    @if( $detailSimpang->controller != 'null' )
    <p>{{ $detailSimpang->controller }}</p>
    @endif
    @if( $detailSimpang->controller == "" )
    <label class="label-danger"> Tidak terpasang Controller Pelican Crossing</label>
    @endif
</div>
<!-- Controller Pc Field -->
<div class="col-sm-4">
    {!! Form::label('controller_pc', 'Controller Pelican crossing:') !!}
    @if( $detailSimpang->controller_pc != 'null' )
    <p>{{ $detailSimpang->controller_pc }}</p>
    @endif
    @if( $detailSimpang->controller_pc == "")
    <label class="label-danger"> Tidak terpasang Controller Pelican Crossing</label>
    @endif
</div>
<!-- Thn Psng Controller Field -->
<div class="col-sm-4">
    {!! Form::label('tahun_psng_controller', 'Tahun Psng Controller:') !!}
    <p>{{ $detailSimpang->thn_psng_controller }}</p>
</div>



<h3>Data Simpang</h3>
<!-- gambar_simpang Field -->
<div class="col-sm-12">
    {!! Form::label('gambar_simpang', 'Gambar Simpang:') !!}
    <p><img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->gambar_simpang) }}" alt="gambar tidak ada" /></p>
</div>


<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
        {!! Form::label('fase_a', 'Fase A:') !!}
        <p> <img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_a) }}" alt="gambar tidak ada" />
        </p>
    </div>
</div>

<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_b', 'Fase B:') !!}
    <p><img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_b) }}" alt="gambar tidak ada" /></p>
    </div>
</div>

<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_c', 'Fase C:') !!}
    <p><img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_c) }}" alt="gambar tidak ada" /></p>
    </div>
</div>

<div class="col-sm-12 uk-margin-small-top"></div>

<!-- Fase D Field -->
<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_d', 'Fase D:') !!}
    <p> <img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_d) }}" alt="gambar tidak ada" /></p>
    </div>
</div>
<!-- Fase E Field -->
<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_e', 'Fase E:') !!}
    <p> <img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_e) }}" alt="gambar tidak ada" /></p>
    </div>
</div>

<!-- Fase F Field -->
<div class="col-sm-4 text-center">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
    {!! Form::label('fase_f', 'Fase F:') !!}
    <p> <img src="{{ asset('public'.'/'.'uploads'.'/'. $detailSimpang->fase_f) }}" alt="gambar tidak ada" /></p>
    </div>
</div>
