
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#copc").click(function(){
    $(".co").hide();
  });
  $("#copc").click(function(){
    $(".copc").show();
  });
   $("#co").click(function(){
    $(".copc").hide();
  });
  $("#co").click(function(){
    $(".co").show();
  });
});
</script>
 <div class="form-group col-sm-12">
     
     <h3>Data Lokasi</h3><small>*required</small>
     </div>  
<!-- Id Lokasi Fk Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    <select class="form-control" name="nama_lokasi">
   
   <option>Pilih Nama Lokasi</option>
     
   @foreach ($nama_lokasi as $key => $value)
     <option value="{{ $key }}" {{ ( $key == $nama_lokasi) ? 'selected' : '' }}> 
         {{ $value }} 
     </option>
   @endforeach    
 </select>
</div>


<!-- Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lat', 'Lat:') !!}
    {!! Form::number('lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Long Field -->
<div class="form-group col-sm-6">
    {!! Form::label('long', 'Long:') !!}
    {!! Form::number('long', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('alamat', 'Alamat:') !!}
    {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
</div>

<div class="col-sm-12">
  <div class="uk-card uk-card-default uk-card-large uk-card-body">
     <div class="form-group col-sm-12" style="padding-left:0px;">
     
        <h3>Data Tiang dan Unit Lampu</h3>
     </div>   
     <div class="col-sm-12  " style="padding:0px;">

     <label>Data Tiang</label>
     <div class="row">
  <div class="col-lg-6">
            @foreach ($jenis_tiang as $key => $jenis_tiangs)
            <input  type="text" name="jenis_tiang[]" value="{{ $jenis_tiangs }} " class="form-control"  style="margin:7px;" disabled>
            
            <input  type="hidden" name="jenis_tiang[]" value="{{ $jenis_tiangs }} " class="form-control">
             @endforeach
  </div><!-- /.col-lg-6 -->
  <div class="col-lg-6">
            @foreach ($jenis_tiang as $key => $jenis_tiangs)
            
            <input type="number" name="jml_tiang[]" class="form-control" placeholder="masukan jumlah tiang"  style="margin:7px;"/>
             @endforeach
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->

<label>Data Lampu</label>
     <div class="row">
  <div class="col-lg-4">
            @foreach ($jenis_lampu as $key => $jenis_lampus)
            <input  type="text" name="jenis_lampu[]" value="{{ $jenis_lampus }} " class="form-control"  style="margin:7px;" disabled>
            
            <input  type="hidden" name="jenis_lampu[]" value="{{ $jenis_lampus }} " class="form-control">
             @endforeach
  </div><!-- /.col-lg-6 -->
  <div class="col-lg-4">
  @foreach ($jenis_lampu as $key => $jenis_lampus)
            
            <input type="number" name="jml_lampu[]" class="form-control" placeholder="masukan jumlah unit lampu"  style="margin:7px;"/>
             @endforeach
  </div><!-- /.col-lg-3 -->
  <div class="col-lg-4">
  @foreach ($jenis_lampu as $key => $jenis_lampus)
  {!! Form::selectYear('thn_psng_unit[]',1998, 2029, null, ['class' => 'form-control thn_lampu', 'placeholder' => 'Pilih tahun pasang unit lampu']) !!}
             @endforeach
  </div><!-- /.col-lg-3 -->
</div><!-- /.row -->


            
          
          
             <input type="hidden" name="nama_lokasi_arr[]" class="form-control nama_lokasi" placeholder="" value="" />
            
     </div>
    
<!-- <div id="mydiv" class=" col-sm-9" style="float:right;"></div>
<div id="mydiv2" class=" col-sm-9" style="float:right;"></div> -->
    </div>
</div>

    
<!-- <script>
    var i = 2;

    function createNew() {
        $("#mydiv").append('<div class="col-sm-12" style="border-top: 3px solid #3c8dbc;padding-bottom:10px;width:99%;margin-left:5px; ">'+'</div>' +
            '</div>' + '<div class="form-group col-sm-6">' +'<label>'+'Jenis Tiang :'+'</label>'+
            '{!! Form::select('jenis_tiang[]', $jenis_tiang, null, ['class' => 'form-control']) !!}' +
            '</div>' + '<div class="form-group col-sm-6">' +'<label>'+'Jumlah Tiang:'+'</label>'+
            '<input type="number" name="jml_tiang[]" class="form-control" placeholder=""/>' +
            '</div>'+'<br/>');
        i++;
    }

</script> -->

    <style>
#thn_psng_unit{
    display: block;
    width: 100%;
    padding: 6px 12px;
    font-size: 14px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
}

.thn_lampu{
    margin:7px;
}
</style>

   

<!-- <style>
.setengah {
  content: "\00BD";
}
</style>
<div class="form-group has-feedback{{ $errors->has('tiang') ? ' has-error' : '' }} col-sm-6 ">
{!! Form::label('tiang', 'Tiang:') !!}
            <select class="form-control" name="tiang"  value="{{ old('tiang') }}" >
                <option value="">----pilih jenis tiang----</option>
                <option value="tiang lurus 4 meter bulat">tiang lurus 4 meter bulat</option>
                <option value="tiang lurus 4 meter oktagonal">tiang lurus 4 meter oktagonal</option>
                <option value="tiang lurus 6 meter bulat">tiang lurus 6 meter bulat</option>
                <option value="tiang lurus 6 meter oktagonal">tiang lurus 6 meter oktagonal</option>
                <option value="tiang lengkung bulat">tiang lengkung bulat</option>
                <option value="tiang lengkung oktagonal">tiang lengkung oktagonal</option>
                <option value="tiang F">tiang F</option>
                <option value="tiang siku bentangan 7 1&frasl;2">tiang siku bentangan 7<span class="setengah">&#189;</span></option>
                <option value="tiang siku bentangan 4 1&frasl;2">tiang siku bentangan 4<span class="setengah">&#189;</span></option>
            </select>
            </div>

            <div class="form-group col-sm-6">
    {!! Form::label('jml_tiang', 'Jumlah Tiang:') !!}
    {!! Form::number('jml_tiang', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Unit Lampu Field -->
<!-- <div class="form-group has-feedback{{ $errors->has('unit_lampung') ? ' has-error' : '' }} col-sm-6 ">
{!! Form::label('unit_lampu', 'Unit Lampu:') !!}
            <select class="form-control" name="unit_lampu"  value="{{ old('unit_lampu') }}" >
                <option value="">----pilih jenis unit lampu----</option>
                <option value="2 X 30 cm (MKH)">2 X 30 cm (MKH)</option>
                <option value="2 X 30 cm (MKH)">2 X 30 cm (K)</option>
                <option value="2 X 30 cm (MH)">2 X 30 cm (MH)</option>
                <option value="3 X 20 cm (MKH)">3 X 20 cm (MKH)</option>
                <option value="3 X 20 cm (K)">3 X 20 cm (K)</option>
                <option value="3 X 20 cm (MH)">3 X 20 cm (MH)</option>
            </select>
            </div>
            <div class="form-group col-sm-6">
    {!! Form::label('jml_lampu', 'Jumlah Lampu:') !!}
    {!! Form::number('jml_lampu', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('thn_psng_unit', 'Tahun Pasang Unit:') !!}
    <br>
    {!! Form::selectYear('thn_psng_unit',1998, 2029, ['class' => 'form-control tahununit']) !!}
</div> -->

<div class="col-sm-12">

<div class="uk-card uk-card-default uk-card-large uk-card-body">
<div class="form-group col-sm-12" style="padding-left:0px;">
     
     <h3>Data Controller</h3><small>*required</small>
     </div>  
<div class="col-sm-12" style="padding:30px;">
    <label>Pilih Jenis Controller</label>
    <a id="co" class="uk-button uk-button-secondary uk-button-small">Controller</a>
    <a id="copc" class="uk-button uk-button-secondary uk-button-small">Controller PC</a>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('thn_psng_controller', 'Tahun Pasang Controller:') !!}
    <br>
    {!! Form::selectYear('thn_psng_controller',1998, 2029, ['class' => 'form-control']) !!}
</div>

<!-- Thn Psng Unit Field -->




<!-- Controller Field -->

<div class="form-group has-feedback{{ $errors->has('controller') ? ' has-error' : '' }} col-sm-6 co" style="">
{!! Form::label('controller', 'Controller:') !!}
            <select class="form-control" name="controller"  value="{{ old('controller') }}" >
                <option value="">----pilih jenis controller----</option>
                <option value="Stats A (ATCS)">Stats A (ATCS)</option>
                <option value="Stats A (NON ATCS)">Stats A (NON ATCS)</option>
                <option value="Newtrap">Newtrap</option>
                <option value="UMC AC">UMC AC</option>
                <option value="UMC DC">UMC DC</option>
                <option value="HMK">HMK</option>
            </select>
            </div>
           
<!-- Thn Psng Controller Field -->
<style>
#thn_psng_controller{
    display: block;
    width: 100%;
    padding: 6px 12px;
    font-size: 14px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
}
</style>

<div class="form-group has-feedback{{ $errors->has('controller_pc') ? ' has-error' : '' }} col-sm-6 copc" style="display:none">
{!! Form::label('controller_pc', 'Controller PC:') !!}
            <select class="form-control" name="controller_pc"  value="{{ old('controller_pc') }}" >
                <option value="">----pilih jenis controller PC----</option>
                <option value="Newtrap">Newtrap</option>
                <option value="UMC">UMC</option>
                <option value="HMK">HMK</option>
                <option value="Telepico">Telepico</option>
            </select>
            </div>


            <div class="form-group col-sm-12">
</div>


</div>

</div>
<!-- Controller Pc Field -->


<!-- Fase A Field -->
<div class="form-group col-sm-12" style="padding-top:30px;">

<h3>Data Simpang</h3>
    {!! Form::label('gambar_simpang', 'Gambar Simpang:') !!}
    {!! Form::file('gambar_simpang', null, ['class' => 'form-control']) !!}
</div>
<!-- Fase A Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_a', 'Fase A:') !!}
    {!! Form::file('fase_a', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase B Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_b', 'Fase B:') !!}
    {!! Form::file('fase_b', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase C Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_c', 'Fase C:') !!}
    {!! Form::file('fase_c', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase D Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_d', 'Fase D:') !!}
    {!! Form::file('fase_d', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase E Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_e', 'Fase E:') !!}
    {!! Form::file('fase_e', null, ['class' => 'form-control']) !!}
</div>

<!-- Fase F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fase_f', 'Fase F:') !!}
    {!! Form::file('fase_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'uk-button uk-button-primary uk-button-small']) !!}
    <a href="{{ route('detailSimpangs.index') }}" class="uk-button uk-button-default uk-button-small">Cancel</a>
</div>

