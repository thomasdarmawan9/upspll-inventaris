<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $detailLampu->id !!}</p>
</div>

<!-- Nama Lokasi Field -->
<div class="form-group">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    <p>{!! $detailLampu->nama_lokasi !!}</p>
</div>

<!-- Jenis Lampu Field -->
<div class="form-group">
    {!! Form::label('jenis_lampu', 'Jenis Lampu:') !!}
    <p>{!! $detailLampu->jenis_lampu !!}</p>
</div>

<!-- Jml Lampu Field -->
<div class="form-group">
    {!! Form::label('jml_lampu', 'Jml Lampu:') !!}
    <p>{!! $detailLampu->jml_lampu !!}</p>
</div>

<!-- Thn Psng Unit Field -->
<div class="form-group">
    {!! Form::label('thn_psng_unit', 'Thn Psng Unit:') !!}
    <p>{!! $detailLampu->thn_psng_unit !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $detailLampu->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $detailLampu->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $detailLampu->deleted_at !!}</p>
</div>

