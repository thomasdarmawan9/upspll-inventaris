<table class="table table-responsive" id="detailLampus-table">
    <thead>
        <tr>
            <th>Nama Lokasi</th>
        <th>Jenis Lampu</th>
        <th>Jml Lampu</th>
        <th>Thn Psng Unit</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($detailLampus as $detailLampu)
        <tr>
            <td>{!! $detailLampu->nama_lokasi !!}</td>
            <td>{!! $detailLampu->jenis_lampu !!}</td>
            <td>{!! $detailLampu->jml_lampu !!}</td>
            <td>{!! $detailLampu->thn_psng_unit !!}</td>
            <td>
                {!! Form::open(['route' => ['detailLampus.destroy', $detailLampu->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('detailLampus.show', [$detailLampu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('detailLampus.edit', [$detailLampu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>