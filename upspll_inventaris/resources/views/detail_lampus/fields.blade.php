<!-- Nama Lokasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    {!! Form::text('nama_lokasi', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Lampu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_lampu', 'Jenis Lampu:') !!}
    {!! Form::text('jenis_lampu', null, ['class' => 'form-control']) !!}
</div>

<!-- Jml Lampu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jml_lampu', 'Jml Lampu:') !!}
    {!! Form::number('jml_lampu', null, ['class' => 'form-control']) !!}
</div>

<!-- Thn Psng Unit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('thn_psng_unit', 'Thn Psng Unit:') !!}
    {!! Form::text('thn_psng_unit', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('detailLampus.index') !!}" class="btn btn-default">Cancel</a>
</div>
