@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Detail Lampu
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($detailLampu, ['route' => ['detailLampus.update', $detailLampu->id], 'method' => 'patch']) !!}

                        @include('detail_lampus.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection