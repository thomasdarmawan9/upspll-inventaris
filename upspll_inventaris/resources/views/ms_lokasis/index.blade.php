@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Master Lokasi</h1>
        <h1 class="pull-right">  
            @if(auth::user()->role == 'Admin')
                <a class="uk-button uk-button-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('msLokasis.create') !!}">Add New</a>
            @endif
            @if(auth::user()->role=='Operator')
                <a class="uk-button uk-button-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('msLokasis.create') !!}">Add New</a>
            @endif
            @if(auth::user()->role=='teknisi')
                <a class="uk-button uk-button-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('msLokasis.create') !!}" disabled>Add New</a>
            @endif
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
            <a href="{{url('upspll_inventaris/ms_lokasi/cetak_pdf_lokasiatcs')}}" class="uk-button uk-button-primary uk-button-small" target="_blank" style="margin-bottom:20px;">CETAK PDF LOKASI ATCS</a>
            <a href="{{url('upspll_inventaris/ms_lokasi/cetak_pdf_lokasinonatcs')}}" class="uk-button uk-button-primary uk-button-small" target="_blank" style="margin-bottom:20px;">CETAK PDF LOKASI NON ATCS</a>
           
                    @include('ms_lokasis.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

