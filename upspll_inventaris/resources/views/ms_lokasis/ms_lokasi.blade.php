<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>UP SPLL DISHUB</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <style>
      table {
  border-collapse: collapse;
}
 th {
  background: #ccc;
}

th, td {
  border: 1px solid #ccc;
  padding: 8px;
}

tr:nth-child(even) {
  background: #efefef;
}

tr:hover {
  background: #d1d1d1;
}
    </style>
</head>

<body class="sidebar-mini">
<div class="table-responsive" style="width:100%;">
<center>
<h2>DAFTAR LOKASI SIMPANG UPSPLL DISHUB DKI JAKARTA</h2>
<table id="table" class="table">
<thead>
            <tr>
                <th>Nomor</th>
        <th>SID</th>
        <th>Nama Lokasi</th>
            </tr>
        </thead>
        <tbody> 
        @foreach($msLokasis as $msLokasi)
            <tr>
                <td>{{ $msLokasi->id }}</td>
            <td>{{ $msLokasi->nomor_lokasi }}</td>
            <td>{{ $msLokasi->nama_lokasi }}</td>
          
            </tr>
        @endforeach
        </tbody>
</table>
</center>
</div>


</body>
</html>
