<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
           table {
  border-collapse: collapse;
}
 th {
  background: #ccc;
}

th, td {
  border: 1px solid #ccc;
  padding: 8px;
}

tr:nth-child(even) {
  background: #efefef;
}

tr:hover {
  background: #d1d1d1;
}
    </style>
</head>

<body>

    <h2>Daftar Lokasi Simpang ATCS</h2>
  
    
    <table>
        <thead>
            <tr>
        <th>SID</th>
        <th>Nama Lokasi</th>
        
            </tr>
        </thead>
        <tbody>
        @foreach($msLokasi as $msLokasis)
            <tr>
                <td>{!! $msLokasis->nomor_lokasi !!}</td>
            <td>{!! $msLokasis->nama_lokasi !!}</td>
            
           </tr>
           
    @endforeach
        </tbody>
    </table>
</body>

</html>