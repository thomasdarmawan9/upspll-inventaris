@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ms Lokasi
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                   @include('ms_lokasis.show_fields')
                    <a href="{!! route('msLokasis.index') !!}" class="uk-button uk-button-default uk-button-small">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
