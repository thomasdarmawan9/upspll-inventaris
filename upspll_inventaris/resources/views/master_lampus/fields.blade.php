<!-- Jenis Lampu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_lampu', 'Jenis Lampu:') !!}
    {!! Form::text('jenis_lampu', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('masterLampus.index') !!}" class="btn btn-default">Cancel</a>
</div>
