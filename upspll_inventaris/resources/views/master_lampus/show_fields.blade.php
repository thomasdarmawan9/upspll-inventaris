<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $masterLampu->id !!}</p>
</div>

<!-- Jenis Lampu Field -->
<div class="form-group">
    {!! Form::label('jenis_lampu', 'Jenis Lampu:') !!}
    <p>{!! $masterLampu->jenis_lampu !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $masterLampu->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $masterLampu->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $masterLampu->deleted_at !!}</p>
</div>

