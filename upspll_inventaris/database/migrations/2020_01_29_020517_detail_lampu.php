<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetailLampu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_lampu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_lokasi')->nullable();
            $table->string('jenis_lampu')->nullable();
            $table->integer('jml_lampu')->nullable();
            $table->string('thn_psng_unit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
