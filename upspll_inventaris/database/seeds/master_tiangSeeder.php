<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class master_tiangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_tiang')->insert([
            [
                'jenis_tiang' => 'tiang lurus 4 meter bulat'
            ],
            [
                'jenis_tiang' => 'tiang lurus 4 meter oktagonal'
            ],
            [
                'jenis_tiang' => 'tiang lurus 6 meter bulat'
            ],
            [
                'jenis_tiang' => 'tiang lurus 6 meter oktagonal'
            ],
            [
                'jenis_tiang' => 'tiang lengkung bulat'
            ],
            [
                'jenis_tiang' => 'tiang lengkung oktagonal'
            ],
            [
                'jenis_tiang' => 'tiang F'
            ]
            ]);
    }
}
