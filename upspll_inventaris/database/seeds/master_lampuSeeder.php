<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class master_lampuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_lampu')->insert( [
            [
                'jenis_lampu' => '2 X 30 cm (MKH)'
            ],
            [
                'jenis_lampu' => '2 X 30 cm (K)'
            ],
            [
                'jenis_lampu' => '2 X 30 cm (MH)'
            ],
            [
                'jenis_lampu' => '3 X 30 cm (MKH)'
            ],
            [
                'jenis_lampu' => '3 X 30 cm (K)'
            ],
            [
                'jenis_lampu' => '3 X 30 cm (MH)'
            ]
            ]);
    }
}