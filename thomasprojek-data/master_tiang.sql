-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29 Jan 2020 pada 05.32
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `up_spll_inventaris`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_tiang`
--

CREATE TABLE `master_tiang` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis_tiang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `master_tiang`
--

INSERT INTO `master_tiang` (`id`, `jenis_tiang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'tiang lurus 4 meter bulat', NULL, NULL, NULL),
(9, 'tiang lurus 4 meter oktagonal', NULL, NULL, NULL),
(10, 'tiang lurus 6 meter bulat', NULL, NULL, NULL),
(11, 'tiang lurus 6 meter oktagonal', NULL, NULL, NULL),
(12, 'tiang lengkung bulat', NULL, NULL, NULL),
(13, 'tiang lengkung oktagonal', NULL, NULL, NULL),
(14, 'tiang F', NULL, NULL, NULL),
(15, 'tiang siku bentangan 7 1&frasl;2', '2020-01-28 21:31:46', '2020-01-28 21:31:46', NULL),
(16, 'tiang siku bentangan 4 1&frasl;2', '2020-01-28 21:32:17', '2020-01-28 21:32:17', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_tiang`
--
ALTER TABLE `master_tiang`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_tiang`
--
ALTER TABLE `master_tiang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
