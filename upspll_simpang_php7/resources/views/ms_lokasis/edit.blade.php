@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ms Lokasi
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($msLokasi, ['route' => ['msLokasis.update', $msLokasi->id], 'method' => 'patch']) !!}

                        @include('ms_lokasis.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection