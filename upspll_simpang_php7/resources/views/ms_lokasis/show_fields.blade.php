<!-- Id Lokasi Field -->
<div class="form-group">
    {!! Form::label('id', 'Id Lokasi:') !!}
    <p>{{ $msLokasi->id }}</p>
</div>

<!-- Nomor Lokasi Field -->
<div class="form-group">
    {!! Form::label('nomor_lokasi', 'Nomor Lokasi:') !!}
    <p>{{ $msLokasi->nomor_lokasi }}</p>
</div>

<!-- Nama Lokasi Field -->
<div class="form-group">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    <p>{{ $msLokasi->nama_lokasi }}</p>
</div>

