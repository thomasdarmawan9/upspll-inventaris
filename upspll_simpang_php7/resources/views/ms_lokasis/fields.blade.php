

<!-- Nomor Lokasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nomor_lokasi', 'Nomor Lokasi:') !!}
    {!! Form::text('nomor_lokasi', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Lokasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_lokasi', 'Nama Lokasi:') !!}
    {!! Form::text('nama_lokasi', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('msLokasis.index') }}" class="btn btn-default">Cancel</a>
</div>
