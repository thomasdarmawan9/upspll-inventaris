<table id="table" class="display">
<thead>
            <tr>
                <th>Id Lokasi</th>
        <th>Nomor Lokasi</th>
        <th>Nama Lokasi</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($msLokasis as $msLokasi)
            <tr>
                <td>{{ $msLokasi->id }}</td>
            <td>{{ $msLokasi->nomor_lokasi }}</td>
            <td>{{ $msLokasi->nama_lokasi }}</td>
                <td>
                    {!! Form::open(['route' => ['msLokasis.destroy', $msLokasi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('msLokasis.show', [$msLokasi->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('msLokasis.edit', [$msLokasi->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
</table>

