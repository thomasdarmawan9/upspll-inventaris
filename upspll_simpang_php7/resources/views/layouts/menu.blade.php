

<li class="{{ Request::is('msLokasis*') ? 'active' : '' }}">
    <a href="{{ route('msLokasis.index') }}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Master Lokasi</span></a>
</li>

<li class="{{ Request::is('detailSimpangs*') ? 'active' : '' }}">
    <a href="{{ route('detailSimpangs.index') }}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Detail Simpang</span></a>
</li>

<li class="{{ Request::is('planSimpangs*') ? 'active' : '' }}">
    <a href="{{ route('planSimpangs.index') }}" class="uk-text-emphasis uk-text-bold"><i class="fa fa-edit"></i><span>Plan Simpang</span></a>
</li>

