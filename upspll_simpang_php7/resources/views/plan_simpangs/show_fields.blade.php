<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $planSimpang->id }}</p>
</div>

<!-- Id Simpang Fk Field -->
<div class="form-group">
    {!! Form::label('id_simpang_fk', 'Id Simpang Fk:') !!}
    <p>{{ $planSimpang->id_simpang_fk }}</p>
</div>

<!-- Nomor Plan Field -->
<div class="form-group">
    {!! Form::label('nomor_plan', 'Nomor Plan:') !!}
    <p>{{ $planSimpang->nomor_plan }}</p>
</div>

<!-- Cycle Time Field -->
<div class="form-group">
    {!! Form::label('cycle_time', 'Cycle Time:') !!}
    <p>{{ $planSimpang->cycle_time }}</p>
</div>

<!-- Offset Field -->
<div class="form-group">
    {!! Form::label('offset', 'Offset:') !!}
    <p>{{ $planSimpang->offset }}</p>
</div>

<!-- A Field -->
<div class="form-group">
    {!! Form::label('a', 'A:') !!}
    <p>{{ $planSimpang->a }}</p>
</div>

<!-- B Field -->
<div class="form-group">
    {!! Form::label('b', 'B:') !!}
    <p>{{ $planSimpang->b }}</p>
</div>

<!-- C Field -->
<div class="form-group">
    {!! Form::label('c', 'C:') !!}
    <p>{{ $planSimpang->c }}</p>
</div>

<!-- D Field -->
<div class="form-group">
    {!! Form::label('d', 'D:') !!}
    <p>{{ $planSimpang->d }}</p>
</div>

<!-- E Field -->
<div class="form-group">
    {!! Form::label('e', 'E:') !!}
    <p>{{ $planSimpang->e }}</p>
</div>

<!-- F Field -->
<div class="form-group">
    {!! Form::label('f', 'F:') !!}
    <p>{{ $planSimpang->f }}</p>
</div>

<!-- R Minus Field -->
<div class="form-group">
    {!! Form::label('r_minus', 'R Minus:') !!}
    <p>{{ $planSimpang->r_minus }}</p>
</div>

<!-- R Plus Field -->
<div class="form-group">
    {!! Form::label('r_plus', 'R Plus:') !!}
    <p>{{ $planSimpang->r_plus }}</p>
</div>

<!-- Y Minus Field -->
<div class="form-group">
    {!! Form::label('y_minus', 'Y Minus:') !!}
    <p>{{ $planSimpang->y_minus }}</p>
</div>

<!-- Y Plus Field -->
<div class="form-group">
    {!! Form::label('y_plus', 'Y Plus:') !!}
    <p>{{ $planSimpang->y_plus }}</p>
</div>

<!-- Z Minus Field -->
<div class="form-group">
    {!! Form::label('z_minus', 'Z Minus:') !!}
    <p>{{ $planSimpang->z_minus }}</p>
</div>

<!-- Z Plus Field -->
<div class="form-group">
    {!! Form::label('z_plus', 'Z Plus:') !!}
    <p>{{ $planSimpang->z_plus }}</p>
</div>

<!-- Q Minus Field -->
<div class="form-group">
    {!! Form::label('q_minus', 'Q Minus:') !!}
    <p>{{ $planSimpang->q_minus }}</p>
</div>

<!-- Q Plus Field -->
<div class="form-group">
    {!! Form::label('q_plus', 'Q Plus:') !!}
    <p>{{ $planSimpang->q_plus }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $planSimpang->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $planSimpang->updated_at }}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{{ $planSimpang->deleted_at }}</p>
</div>

