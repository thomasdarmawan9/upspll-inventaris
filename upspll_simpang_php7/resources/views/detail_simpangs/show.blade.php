@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Detail Simpang
    </h1>
</section>

<div class="content">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row" style="padding-left: 20px">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home"><b>Planning</b></a></li>
                    <li><a data-toggle="tab" href="#menu1"><b>Schedule</b></a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active" style="padding-top:10px;">
                        <h3>PLANNING</h3>
                        <p>
                            <center>
                                <div class="table-responsive" style="width:95%;padding-top:20px;">
                                    <table class="display" id="table">
                                        <thead>
                                            <tr>
                                                <th>Nomor Lokasi</th>
                                                <th>Nomor Plan</th>
                                                <th>Cycle Time</th>
                                                <th>Offset</th>
                                                <th>A</th>
                                                <th>B</th>
                                                <th>C</th>
                                                <th>D</th>
                                                <th>E</th>
                                                <th>F</th>
                                                <th>R Minus</th>
                                                <th>R Plus</th>
                                                <th>Y Minus</th>
                                                <th>Y Plus</th>
                                                <th>Z Minus</th>
                                                <th>Z Plus</th>
                                                <th>Q Minus</th>
                                                <th>Q Plus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($plan as $planSimpang)
                                            <tr>
                                                <td>{{ $planSimpang->nama_lokasi }}</td>
                                                <td>{{ $planSimpang->nomor_plan }}</td>
                                                <td>{{ $planSimpang->cycle_time }}</td>
                                                <td>{{ $planSimpang->offset }}</td>
                                                <td>{{ $planSimpang->a }}</td>
                                                <td>{{ $planSimpang->b }}</td>
                                                <td>{{ $planSimpang->c }}</td>
                                                <td>{{ $planSimpang->d }}</td>
                                                <td>{{ $planSimpang->e }}</td>
                                                <td>{{ $planSimpang->f }}</td>
                                                <td>{{ $planSimpang->r_minus }}</td>
                                                <td>{{ $planSimpang->r_plus }}</td>
                                                <td>{{ $planSimpang->y_minus }}</td>
                                                <td>{{ $planSimpang->y_plus }}</td>
                                                <td>{{ $planSimpang->z_minus }}</td>
                                                <td>{{ $planSimpang->z_plus }}</td>
                                                <td>{{ $planSimpang->q_minus }}</td>
                                                <td>{{ $planSimpang->q_plus }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </center>
                        </p>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <h3>Schedule</h3>
                        <p></p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" style="padding-left: 20px">
                @include('detail_simpangs.show_fields')
                <div class="col-sm-12" style="padding-top:30px">
                <a href="{{ route('detailSimpangs.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
