<!-- gambar_simpang Field -->
<div class="col-sm-12">
    {!! Form::label('gambar_simpang', 'gambar simpang:') !!}
    <p>{{ $detailSimpang->gambar_simpang }}</p>
</div>

<!-- Id Lokasi Fk Field -->
<div class="col-sm-12">
    {!! Form::label('nama_lokasi', 'Nama lokasi:') !!}
    <p>{{ $detailSimpang->nama_lokasi }}</p>
</div>

<!-- Lat Field -->
<div class="col-sm-4">
    {!! Form::label('lat', 'Lat:') !!}
    <p>{{ $detailSimpang->lat }}</p>
</div>

<!-- Long Field -->
<div class="col-sm-4">
    {!! Form::label('long', 'Long:') !!}
    <p>{{ $detailSimpang->long }}</p>
</div>

<!-- Tiang Field -->
<div class="col-sm-4">
    {!! Form::label('tiang', 'Tiang:') !!}
    <p>{{ $detailSimpang->tiang }}</p>
</div>

<!-- Unit Lampu Field -->
<div class="col-sm-4">
    {!! Form::label('unit_lampu', 'Unit Lampu:') !!}
    <p>{{ $detailSimpang->unit_lampu }}</p>
</div>

<!-- Thn Psng Unit Field -->
<div class="col-sm-4">
    {!! Form::label('thn_psng_unit', 'Thn Psng Unit:') !!}
    <p>{{ $detailSimpang->thn_psng_unit }}</p>
</div>

<!-- Controller Field -->
<div class="col-sm-4">
    {!! Form::label('controller', 'Controller:') !!}
    <p>{{ $detailSimpang->controller }}</p>
</div>

<!-- Thn Psng Controller Field -->
<div class="col-sm-4">
    {!! Form::label('thn_psng_controller', 'Thn Psng Controller:') !!}
    <p>{{ $detailSimpang->thn_psng_controller }}</p>
</div>

<!-- Controller Pc Field -->
<div class="col-sm-4">
    {!! Form::label('controller_pc', 'Controller Pc:') !!}
    <p>{{ $detailSimpang->controller_pc }}</p>
</div>

<!-- Fase A Field -->
<div class="col-sm-4">
    {!! Form::label('fase_a', 'Fase A:') !!}
    <p>{{ $detailSimpang->fase_a }}</p>
    <img   src="{{ asset('uploads'.'/'. $detailSimpang->fase_a) }}"  alt="gambar tidak ada" />
    
</div>

<!-- Fase B Field -->
<div class="col-sm-4">
    {!! Form::label('fase_b', 'Fase B:') !!}
    <p>{{ $detailSimpang->fase_b }}</p>
    <img   src="{{ asset('uploads'.'/'. $detailSimpang->fase_b) }}" alt="gambar tidak ada" />
</div>

<!-- Fase C Field -->
<div class="col-sm-4">
    {!! Form::label('fase_c', 'Fase C:') !!}
    <p>{{ $detailSimpang->fase_c }}</p>
    <img   src="{{ asset('uploads'.'/'. $detailSimpang->fase_c) }}" alt="gambar tidak ada" />
</div>

<!-- Fase D Field -->
<div class="col-sm-4">
    {!! Form::label('fase_d', 'Fase D:') !!}
    <p>{{ $detailSimpang->fase_d }}</p>
    <img   src="{{ asset('uploads'.'/'. $detailSimpang->fase_d) }}" alt="gambar tidak ada" />
</div>

<!-- Fase E Field -->
<div class="col-sm-4">
    {!! Form::label('fase_e', 'Fase E:') !!}
    <p>{{ $detailSimpang->fase_e }}</p>
    <img   src="{{ asset('uploads'.'/'. $detailSimpang->fase_e) }}" alt="gambar tidak ada" />
</div>

<!-- Fase F Field -->
<div class="col-sm-4">
    {!! Form::label('fase_f', 'Fase F:') !!}
    <p>{{ $detailSimpang->fase_f }}</p>
    <img   src="{{ asset('uploads'.'/'. $detailSimpang->fase_f) }}" alt="gambar tidak ada" />
</div>
