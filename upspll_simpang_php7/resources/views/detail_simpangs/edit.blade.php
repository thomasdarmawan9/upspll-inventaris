@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Detail Simpang
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($detailSimpang, ['route' => ['detailSimpangs.update', $detailSimpang->id], 'method' => 'patch']) !!}

                        @include('detail_simpangs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection