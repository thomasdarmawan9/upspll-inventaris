
    <table id="table" class="display" >
        <thead>
            <tr>
                <th>Nama Lokasi</th>
        <th>Lat</th>
        <th>Long</th>
        <th>Tiang</th>
        <th>Unit Lampu</th>
        <th>Controller</th>
        <th>Thn Psng Controller</th>
        <th>Thn Psng Unit</th>
        <th>Controller Pc</th>
        
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($detailSimpangs as $detailSimpang)
            <tr>
                <td>{{ $detailSimpang->nama_lokasi }}</td>
            <td>{{ $detailSimpang->lat }}</td>
            <td>{{ $detailSimpang->long }}</td>
            <td>{{ $detailSimpang->tiang }}</td>
            <td>{{ $detailSimpang->unit_lampu }}</td>
            <td>{{ $detailSimpang->controller }}</td>
            <td>{{ $detailSimpang->thn_psng_controller }}</td>
            <td>{{ $detailSimpang->thn_psng_unit }}</td>
            <td>{{ $detailSimpang->controller_pc }}</td>
            
                <td>
                    {!! Form::open(['route' => ['detailSimpangs.destroy', $detailSimpang->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('detailSimpangs.show', [$detailSimpang->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i> Lihat Detail</a>
                        <a href="/plansimpangadd/{{$detailSimpang->id}}" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Plan Simpang</a>
                      <a href="{{ route('detailSimpangs.edit', [$detailSimpang->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i> Edit Info</a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
