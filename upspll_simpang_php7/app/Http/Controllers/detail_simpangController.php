<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createdetail_simpangRequest;
use App\Http\Requests\Updatedetail_simpangRequest;
use App\Repositories\detail_simpangRepository;
use App\Repositories\plan_simpangRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class detail_simpangController extends AppBaseController
{
    /** @var  detail_simpangRepository */
    private $detailSimpangRepository;

    public function __construct(detail_simpangRepository $detailSimpangRepo, plan_simpangRepository $planSimpangRepo)
    {
        $this->detailSimpangRepository = $detailSimpangRepo;
        $this->planSimpangRepository = $planSimpangRepo;
    }

    /**
     * Display a listing of the detail_simpang.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->detailSimpangRepository->pushCriteria(new RequestCriteria($request));
        $detailSimpangs = $this->detailSimpangRepository->all();

        return view('detail_simpangs.index')
            ->with('detailSimpangs', $detailSimpangs);
    }

    /**
     * Show the form for creating a new detail_simpang.
     *
     * @return Response
     */
    public function create()
    {
        $nama_lokasi = DB::table('ms_lokasi')->pluck('nama_lokasi','nama_lokasi');

        return view('detail_simpangs.create', compact('nama_lokasi','nama_lokasi'));
    }

    /**
     * Store a newly created detail_simpang in storage.
     *
     * @param Createdetail_simpangRequest $request
     *
     * @return Response
     */
    public function store(Createdetail_simpangRequest $request)
    {
        $input = $request->all();

        echo "<pre>";
        print_r($input);
        
        $fase_a = $request->file('fase_a');
        if($fase_a != null){
        $input['fase_a'] = md5($request->file('fase_a')->getClientOriginalName().time()).'.'.$request->file('fase_a')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_a')->move($destination, $input['fase_a']);    
        }

        $fase_b = $request->file('fase_b');
        if($fase_b != null){
        $input['fase_b'] = md5($request->file('fase_b')->getClientOriginalName().time()).'.'.$request->file('fase_b')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_b')->move($destination, $input['fase_b']);
        }

        $fase_c = $request->file('fase_c');
        if($fase_c != null){
        $input['fase_c'] = md5($request->file('fase_c')->getClientOriginalName().time()).'.'.$request->file('fase_c')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_c')->move($destination, $input['fase_c']);
        }

        $fase_d = $request->file('fase_d');
        if($fase_d != null){
        $input['fase_d'] = md5($request->file('fase_d')->getClientOriginalName().time()).'.'.$request->file('fase_d')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_d')->move($destination, $input['fase_d']);
        }

        $fase_e = $request->file('fase_e');
        if($fase_e != null){
        $input['fase_e'] = md5($request->file('fase_e')->getClientOriginalName().time()).'.'.$request->file('fase_e')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_e')->move($destination, $input['fase_e']);
        }

        $fase_f = $request->file('fase_f');
        if($fase_f != null){
        $input['fase_f'] = md5($request->file('fase_f')->getClientOriginalName().time()).'.'.$request->file('fase_f')->getClientOriginalExtension();
        $destination = base_path() . '/public/uploads';
        $request->file('fase_f')->move($destination, $input['fase_f']);
        }
        
        $detailSimpang = $this->detailSimpangRepository->create($input);

        Flash::success('Detail Simpang saved successfully.');

        return redirect(route('detailSimpangs.index'));
    }

    /**
     * Display the specified detail_simpang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);
        $plan= \App\Models\plan_simpang::where('nama_lokasi','=',$detailSimpang->nama_lokasi)->get();
        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('detailSimpangs.index'));
        }
        // dd($plan);
        return view('detail_simpangs.show')->with(['detailSimpang'=> $detailSimpang,'plan'=>$plan]);
    }

 

    /**
     * Show the form for editing the specified detail_simpang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);

        $nama_lokasi = DB::table('ms_lokasi')->pluck('nama_lokasi','nama_lokasi');

        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('detailSimpangs.index'));
        }

        return view('detail_simpangs.edit',compact('nama_lokasi','nama_lokasi'))->with('detailSimpang', $detailSimpang);
    }

    /**
     * Update the specified detail_simpang in storage.
     *
     * @param  int              $id
     * @param Updatedetail_simpangRequest $request
     *
     * @return Response
     */
    public function update($id, Updatedetail_simpangRequest $request)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);

        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('detailSimpangs.index'));
        }

        $detailSimpang = $this->detailSimpangRepository->update($request->all(), $id);

        Flash::success('Detail Simpang updated successfully.');

        return redirect(route('detailSimpangs.index'));
    }

    /**
     * Remove the specified detail_simpang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detailSimpang = $this->detailSimpangRepository->findWithoutFail($id);

        if (empty($detailSimpang)) {
            Flash::error('Detail Simpang not found');

            return redirect(route('detailSimpangs.index'));
        }

        $this->detailSimpangRepository->delete($id);

        Flash::success('Detail Simpang deleted successfully.');

        return redirect(route('detailSimpangs.index'));
    }
}
