<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createplan_simpangRequest;
use App\Http\Requests\Updateplan_simpangRequest;
use App\Repositories\plan_simpangRepository;
use App\Repositories\detail_simpangRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class plan_simpangController extends AppBaseController
{
    /** @var  plan_simpangRepository */
    private $planSimpangRepository;

    public function __construct(detail_simpangRepository $detailSimpangRepo, plan_simpangRepository $planSimpangRepo)
    {
        $this->detailSimpangRepository = $detailSimpangRepo;
        $this->planSimpangRepository = $planSimpangRepo;
    }

    /**
     * Display a listing of the plan_simpang.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->planSimpangRepository->pushCriteria(new RequestCriteria($request));
        $planSimpangs = $this->planSimpangRepository->all();

        return view('plan_simpangs.index')
            ->with('planSimpangs', $planSimpangs);
    }

    /**
     * Show the form for creating a new plan_simpang.
     *
     * @return Response
     */
    public function create()
    {
        $nama_lokasi = DB::table('detail_simpang')->where('nama_lokasi','=','id');
        
        return view('plan_simpangs.create')->with('planSimpang', $nama_lokasi);
    }

    public function addsimpang($id)
    {
        $nama_lokasi = DB::table('detail_simpang')->where('id','=',$id)->first();
        return view('plan_simpangs.create')->with('planSimpang', $nama_lokasi);
    }

    /**
     * Store a newly created plan_simpang in storage.
     *
     * @param Createplan_simpangRequest $request
     *
     * @return Response
     */
    public function store(Createplan_simpangRequest $request)
    {
        $this->detailSimpangRepository->pushCriteria(new RequestCriteria($request));
        $detailSimpangs = $this->detailSimpangRepository->all();

        $input = $request->all();
        // dd($input);
        $this->planSimpangRepository->create($input);
        
        if (isset($input['nama_lokasi_arr'])) {
            if(count($input['nama_lokasi_arr'])>0){

                foreach ($input['nama_lokasi_arr'] as $key => $value) {
               
                    $planSimpang = $this->planSimpangRepository->create([
                        'nama_lokasi' => $input['nama_lokasi_arr'][$key],
                        'nomor_plan' => $input['nomor_plan_arr'][$key],
                        'cycle_time' => $input['cycle_time_arr'][$key],
                        'offset' => $input['offset_arr'][$key],
                        'a' => $input['a_arr'][$key],
                        'b' => $input['b_arr'][$key],
                        'c' => $input['c_arr'][$key],
                        'd' => $input['d_arr'][$key],
                        'e' => $input['e_arr'][$key],
                        'f' => $input['f_arr'][$key],
                        'r_minus' => $input['r_minus_arr'][$key],
                        'r_plus' => $input['r_plus_arr'][$key],
                        'y_minus' => $input['y_minus_arr'][$key],
                        'y_plus' => $input['y_plus_arr'][$key],
                        'z_minus' => $input['z_minus_arr'][$key],
                        'z_plus' => $input['z_plus_arr'][$key],
                        'q_minus' => $input['q_minus_arr'][$key],
                        'q_plus' => $input['q_plus_arr'][$key],
                    ]);
                }
        }
        }
            
        
       

        Flash::success('Plan Simpang saved successfully.');

        return view('detail_simpangs.index')
            ->with('detailSimpangs', $detailSimpangs);
    }

    /**
     * Display the specified plan_simpang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $planSimpang = $this->planSimpangRepository->findWithoutFail($id);

        if (empty($planSimpang)) {
            Flash::error('Plan Simpang not found');

            return redirect(route('planSimpangs.index'));
        }

        return view('plan_simpangs.show')->with('planSimpang', $planSimpang);
    }

    /**
     * Show the form for editing the specified plan_simpang.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $nama_lokasi = DB::table('detail_simpang')->where('nama_lokasi','=',$id);
        
        return view('plan_simpangs.edit')->with('planSimpang', $nama_lokasi);
    }

    /**
     * Update the specified plan_simpang in storage.
     *
     * @param  int              $id
     * @param Updateplan_simpangRequest $request
     *
     * @return Response
     */
    public function update($id, Updateplan_simpangRequest $request)
    {
        $planSimpang = $this->planSimpangRepository->findWithoutFail($id);

        if (empty($planSimpang)) {
            Flash::error('Plan Simpang not found');

            return redirect(route('planSimpangs.index'));
        }

        $planSimpang = $this->planSimpangRepository->update($request->all(), $id);

        Flash::success('Plan Simpang updated successfully.');

        return redirect(route('planSimpangs.index'));
    }

    /**
     * Remove the specified plan_simpang from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $planSimpang = $this->planSimpangRepository->findWithoutFail($id);

        if (empty($planSimpang)) {
            Flash::error('Plan Simpang not found');

            return redirect(route('planSimpangs.index'));
        }

        $this->planSimpangRepository->delete($id);

        Flash::success('Plan Simpang deleted successfully.');

        return redirect(route('planSimpangs.index'));
    }
}
