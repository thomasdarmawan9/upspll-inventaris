<?php

namespace App\Repositories;

use App\Models\ms_lokasi;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ms_lokasiRepository
 * @package App\Repositories
 * @version January 10, 2020, 12:57 am UTC
 *
 * @method ms_lokasi findWithoutFail($id, $columns = ['*'])
 * @method ms_lokasi find($id, $columns = ['*'])
 * @method ms_lokasi first($columns = ['*'])
*/
class ms_lokasiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'nomor_lokasi',
        'nama_lokasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ms_lokasi::class;
    }
}
