<?php

namespace App\Repositories;

use App\Models\detail_simpang;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class detail_simpangRepository
 * @package App\Repositories
 * @version January 10, 2020, 2:43 am UTC
 *
 * @method detail_simpang findWithoutFail($id, $columns = ['*'])
 * @method detail_simpang find($id, $columns = ['*'])
 * @method detail_simpang first($columns = ['*'])
*/
class detail_simpangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_lokasi',
        'lat',
        'long',
        'tiang',
        'unit_lampu',
        'controller',
        'thn_psng_controller',
        'thn_psng_unit',
        'controller_pc',
        'fase_a',
        'fase_b',
        'fase_c',
        'fase_d',
        'fase_e',
        'fase_f',
        'gambar_simpang'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return detail_simpang::class;
    }
}
