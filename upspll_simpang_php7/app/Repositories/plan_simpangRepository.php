<?php

namespace App\Repositories;

use App\Models\plan_simpang;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class plan_simpangRepository
 * @package App\Repositories
 * @version January 14, 2020, 1:51 am UTC
 *
 * @method plan_simpang findWithoutFail($id, $columns = ['*'])
 * @method plan_simpang find($id, $columns = ['*'])
 * @method plan_simpang first($columns = ['*'])
*/
class plan_simpangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_lokasi',
        'nomor_plan',
        'cycle_time',
        'offset',
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'r_minus',
        'r_plus',
        'y_minus',
        'y_plus',
        'z_minus',
        'z_plus',
        'q_minus',
        'q_plus'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return plan_simpang::class;
    }
}
