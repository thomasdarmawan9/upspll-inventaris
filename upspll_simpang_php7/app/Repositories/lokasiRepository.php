<?php

namespace App\Repositories;

use App\Models\lokasi;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class lokasiRepository
 * @package App\Repositories
 * @version January 10, 2020, 12:38 am UTC
 *
 * @method lokasi findWithoutFail($id, $columns = ['*'])
 * @method lokasi find($id, $columns = ['*'])
 * @method lokasi first($columns = ['*'])
*/
class lokasiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_lokasi',
        'nomor_lokasi',
        'nama_lokasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return lokasi::class;
    }
}
