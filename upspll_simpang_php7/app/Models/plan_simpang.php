<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class plan_simpang
 * @package App\Models
 * @version January 14, 2020, 1:51 am UTC
 *
 * @property integer id_simpang_fk
 * @property integer nomor_plan
 * @property integer cycle_time
 * @property integer offset
 * @property integer a
 * @property integer b
 * @property integer c
 * @property integer d
 * @property integer e
 * @property integer f
 * @property string r_minus
 * @property string r_plus
 * @property string y_minus
 * @property string y_plus
 * @property string z_minus
 * @property string z_plus
 * @property string q_minus
 * @property string q_plus
 */
class plan_simpang extends Model
{
    use SoftDeletes;

    public $table = 'plan_simpang';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nama_lokasi',
        'nomor_plan',
        'cycle_time',
        'offset',
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'r_minus',
        'r_plus',
        'y_minus',
        'y_plus',
        'z_minus',
        'z_plus',
        'q_minus',
        'q_plus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_lokasi' => 'string',
        'nomor_plan' => 'integer',
        'cycle_time' => 'integer',
        'offset' => 'integer',
        'a' => 'integer',
        'b' => 'integer',
        'c' => 'integer',
        'd' => 'integer',
        'e' => 'integer',
        'f' => 'integer',
        'r_minus' => 'string',
        'r_plus' => 'string',
        'y_minus' => 'string',
        'y_plus' => 'string',
        'z_minus' => 'string',
        'z_plus' => 'string',
        'q_minus' => 'string',
        'q_plus' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'id_simpang_fk' => 'required',
        'nomor_plan' => 'required'
    ];

    public function detail_simpang(){
        return $this->hasOne('\App\Models\detail_simpang','id','nama_lokasi');
    }
    
}
