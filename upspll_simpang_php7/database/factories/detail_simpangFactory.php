<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\detail_simpang;
use Faker\Generator as Faker;

$factory->define(detail_simpang::class, function (Faker $faker) {

    return [
        'nama_lokasi' => $faker->word,
        'lat' => $faker->randomDigitNotNull,
        'long' => $faker->randomDigitNotNull,
        'tiang' => $faker->word,
        'unit_lampu' => $faker->word,
        'controller' => $faker->word,
        'thn_psng_controller' => $faker->word,
        'thn_psng_unit' => $faker->word,
        'controller_pc' => $faker->word,
        'fase_a' => $faker->word,
        'fase_b' => $faker->word,
        'fase_c' => $faker->word,
        'fase_d' => $faker->word,
        'fase_e' => $faker->word,
        'fase_f' => $faker->word,
        'gambar_simpang' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
