<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ms_lokasi;
use Faker\Generator as Faker;

$factory->define(ms_lokasi::class, function (Faker $faker) {

    return [
        'nomor_lokasi' => $faker->word,
        'nama_lokasi' => $faker->word
    ];
});
