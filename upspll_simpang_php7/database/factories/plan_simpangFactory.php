<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\plan_simpang;
use Faker\Generator as Faker;

$factory->define(plan_simpang::class, function (Faker $faker) {

    return [
        'nama_lokasi' => $faker->word,
        'nomor_plan' => $faker->randomDigitNotNull,
        'cycle_time' => $faker->randomDigitNotNull,
        'offset' => $faker->randomDigitNotNull,
        'a' => $faker->randomDigitNotNull,
        'b' => $faker->randomDigitNotNull,
        'c' => $faker->randomDigitNotNull,
        'd' => $faker->randomDigitNotNull,
        'e' => $faker->randomDigitNotNull,
        'f' => $faker->randomDigitNotNull,
        'r_minus' => $faker->word,
        'r_plus' => $faker->word,
        'y_minus' => $faker->word,
        'y_plus' => $faker->word,
        'z_minus' => $faker->word,
        'z_plus' => $faker->word,
        'q_minus' => $faker->word,
        'q_plus' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
